const oracledb = require('oracledb');
const dbConfig = require('../../database');


async function getProfileDetails(email, purchaseType) {
    var query;
        // query = `BEGIN XXSP_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o); END;`
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o); END;`
    var arg1 = email;
    var arg2 = purchaseType;
    return await dbConfig.getData(query, arg1, arg2)
}

async function saveProfileDetails(inFileName) {
    var query = `BEGIN
    XXSP_CMN_ACTION_PKG.profile_managment(:i,:o);
      END;`
    return await dbConfig.saveData(query,inFileName,2)
}

module.exports.getProfileDetails = getProfileDetails;
module.exports.saveProfileDetails=saveProfileDetails;
