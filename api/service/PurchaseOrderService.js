const oracledb = require('oracledb');
const dbConfig = require('../../database');
const { FTP } = require('../config/config');
const DIR = FTP.filePath
var path = require('path');
const http = require('http');
const fs = require('fs');

async function getPendingPurchaseOrder(email, purchaseType, date1, date2) {
  var query;
  if (date1 && date2) {
    query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o,:date1,:date2); END;`
  }
  else {
    query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o); END;`
  }
  var arg1 = email;
  var arg2 = purchaseType;
  var arg3 = date1;
  var arg4 = date2;
  return await dbConfig.getData(query, arg1, arg2, arg3, arg4)
}



async function getDocs(email, purchaseType, docId, date1, date2) {
  var query;
  query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DOC_DETAILS_WRAP(:i,:io,:io1,:o,:date1,:date2); END;`
  var arg1 = email;
  var arg2 = purchaseType;
  var arg3 = docId;
  var arg4 = date1;
  var arg5 = date2;
  return await dbConfig.getDocDetails(query, arg1, arg2, arg3, arg4, arg5)
}

async function getAttachmentEBS(folder, file_url) {
  // ORACLE ATTACHMENT
  var __dirname = path.dirname(folder)
  var originalfilename = path.basename(folder)
  var startPath = path.join(DIR, __dirname)
  // var mkdir = 'mkdir' + startPath;
  if (!fs.existsSync(startPath)) {
    fs.mkdirSync(startPath);
  } else {

    filename = path.basename(folder)
    var uploadedFilename = path.join(startPath, originalfilename)
    var download = (url, dest, cb) => {
      var file = fs.createWriteStream(dest);
      var request = http.get(url, function (response) {
        response.pipe(file);
        file.on('finish', function () {
          ffff = uploadedFilename;
          file.close(cb); // close() is async, call cb after close completes.
        });
      }).on('error', function (err) { // Handle errors
        fs.unlink(dest); // Delete the file async. (But we don't check the result)
        if (cb) cb(err.message);
      });
    };
    var filedownload = await download(file_url, uploadedFilename, (data) => {
      return ffff;
    })

    var query;
    query = `BEGIN XXSP_ADM_ATTACHMENT_PKG.VIEW_ATTACHMENT(:i,:o); END;`
    var arg1 = undefined;
    var arg2 = undefined;
    return await dbConfig.documentDownload(query, arg1, arg2, folder)
  }
}

async function getAttachmentFS(documentId, arg2, folder) {
  // FS ATTACHMENT
  var query;
  query = `BEGIN XXSP_ADM_ATTACHMENT_PKG.VIEW_ATTACHMENT(:i,:o); END;`
  var arg1 = documentId;
  return await dbConfig.documentDownload(query, arg1, arg2, folder)
}


async function getAttachmentEBSwithNewFN(newfilename, NEW_FILE_NAME_PAth) {
  // ORACLE ATTACHMENT
  var __dirname = path.dirname(NEW_FILE_NAME_PAth)
  var originalfilename = path.basename(NEW_FILE_NAME_PAth)
  var startPath = path.join(DIR, __dirname)
  if (!fs.existsSync(startPath)) {
    fs.mkdirSync(startPath);
  } else {
    var fs = require('fs');
    // writeFile function with filename, content and callback function
    fs.writeFile('c:/temp/'+OLD_FILE_NAME, startPath+NEW_FILE_NAME, function (err) {
        if (err) throw err;
          console.log('File is created successfully.');
          return 1
    });
  }
}

async function getDocDetails(printDocumentString) {
  var query;
  // query = `BEGIN XX_SP_PRINT_DOC_PKG.GET_DOC_PRINT_WRAP(:i,:io,:o); END;`
  query = `BEGIN XXSP_CMN_PRINT_DOC_PKG.DOCUMENT_PRINT_ORACLE(:i,:o1,:o2,:o3); END;`
  // var arg1 = purchaseType;
  // var arg2 = headerId;
  var arg1 = printDocumentString;
  var arg2 = undefined;
  var arg3 = undefined;
  return await dbConfig.getDataPrintDocument(query, arg1)
}

async function setAccept(headerId, action) {


  var query;
  query = `BEGIN XXSP_CMN_ACTION_PKG.PO_ACCEPTANCE(:i,:io,:o); END;`

  var arg1 = headerId;
  var arg2 = action;

  return await dbConfig.getDataByString(query, arg1, arg2);

}

async function saveAcceptance(inFileName) {
  var query = `BEGIN
    XXSP_CMN_ACTION_PKG.PO_ACCEPTANCE(:i,:o);
      END;`
  return await dbConfig.saveData(query, inFileName, 2)
}

async function updateAttachmentEBS(inFileName, pdfFile) {
  var query = `BEGIN XXSP_ADM_ATTACHMENT_PKG.ADD_ATTACHMENT_EBS(:i,:io,:o); END;`
  return await dbConfig.updateAttachment(query, inFileName, pdfFile)
}

async function updateAttachmentFS(inFileName, pdfFile) {
  var query = `BEGIN XXSP_ADM_ATTACHMENT_PKG.ADD_ATTACHMENT_FS(:i,:io,:o); END;`
  return await dbConfig.updateAttachment(query, inFileName, pdfFile)
}


async function saveASN(inFileName) {
  var query = `BEGIN XXSP_CMN_ACTION_PKG.create_ASN(:i,:o2,:o); END;`
  return await dbConfig.saveData(query, inFileName, 3)
}

async function dbAPIServiceCall(inFileName) {
  var query = `BEGIN XXSP_CMN_WEB_API_PKG.MAIN(:i,:o); END;`
  return await dbConfig.dbAPICallMain(query, inFileName)
}
async function saveInvoice(inFileName) {
  var query = `BEGIN
    XXSP_CMN_ACTION_PKG.create_invoice(:i,:o2,:o);
      END;`
  return await dbConfig.saveData(query, inFileName, 3)
}

async function createInvoice(inFileName) {
  var query = `BEGIN
    XXSP_CMN_ACTION_PKG. create_invoice_document(:i,:o2,:o);
    END;`
  return await dbConfig.saveData(query, inFileName, 3)
}

async function createGateEntry(inFileName) {
  var query = `BEGIN
    XXSP_CMN_ACTION_PKG.create_gate_entry(:i,:o2,:o);
    END;`
  return await dbConfig.saveData(query, inFileName, 3)
}



async function getPurchaseOrderList(email, purchaseType, date1, date2) {

  query = `BEGIN XXSP_CMN_ACTION_PKG.get_pending_po_for_invoice(:i,:io,:o); END;`
  var arg1 = email;
  var arg2 = purchaseType;
  var arg3 = date1;
  var arg4 = date2;
  return await dbConfig.getData(query, arg1, arg2, arg3, arg4)
}

// async function fileuploadwithDIRandFN(req, res) {
//   const = fileUploadPdf.upload
// }


module.exports.getPendingPurchaseOrder = getPendingPurchaseOrder;
module.exports.getDocs = getDocs;
module.exports.getAttachmentFS = getAttachmentFS;
module.exports.getAttachmentEBS = getAttachmentEBS;
module.exports.getDocDetails = getDocDetails;
module.exports.setAccept = setAccept;
module.exports.saveAcceptance = saveAcceptance;
module.exports.updateAttachmentFS = updateAttachmentFS;
module.exports.updateAttachmentEBS = updateAttachmentEBS;

module.exports.saveASN = saveASN;
module.exports.saveInvoice = saveInvoice;
module.exports.createInvoice = createInvoice;
module.exports.createGateEntry = createGateEntry;
module.exports.getPurchaseOrderList = getPurchaseOrderList;
module.exports.dbAPIServiceCall = dbAPIServiceCall;
module.exports.getAttachmentEBSwithNewFN = getAttachmentEBSwithNewFN;
