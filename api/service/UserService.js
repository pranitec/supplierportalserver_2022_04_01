const oracledb = require('oracledb');
const dbConfig = require('../../database');
const jwt = require('jsonwebtoken');
const logger = require("../lib/logger");


async function getUserEmail(email, password, loginptions) {
  let inFileName = {
    INPUT_HEADER :{ MAIL_ID : email.toUpperCase()},
    FILTER_DETAILS :{ MENU_NAME:"LOGIN_PAGE", DOCUMENT_TYPE: "LOGIN_PAGE", PASSWORD : password, SUPPLIER_ID :undefined }
  }
    // var query = `BEGIN XXSP_ADM_LOGIN_PKG.USER_DETAILS_WRAP(:i,:io,:o,:io1, :io2); END;`
    // var arg1 = email;
    // var arg2 = password;
    // var arg3 = undefined;
    // var arg4 = "loginoptions";
    // return await dbConfig.getUserData(query, arg1, arg2, arg3, arg4)
    var query = `BEGIN XXSP_CMN_WEB_API_PKG.MAIN(:i,:o); END;`
    return await dbConfig.dbAPICallMain(query, inFileName)
}

async function tokenGeneration(req,res,user) {
    user.USER.PASSWORD=undefined;
    let token = jwt.sign(
        { emailId: user.USER.MAIL_ID, userData: user }
        , 'secret', { expiresIn: '2h' });
        logger.info('Token generated Successfullyyy');
    res.status(200).json({
        token: token,
        user: user
    });
}

function verifyToken(req, res, next) {
    var token;
        token = req.headers.authorization;
    if (token) {
        jwt.verify(token, 'secret', function (err, tokendata) {
            if (err) {
                return res.status(400).json({ message: 'Unauthorized Request' })
            }
            if (tokendata) {
                    req.body.emailId = tokendata.emailId;
                    decodedToken = tokendata;
                next();
            }
        });
    } else {
        return res.status(400).json({ message: 'Unauthorized Request' })
    }


}

async function userRegister(inFileName) {
    var query = `BEGIN
    :o := XXSP_ADM_LOGIN_PKG.SUPPLIER_REGISTRATION(:i);
      END;`
    return await dbConfig.saveData(query,inFileName,1)
}

async function updateAttachment(arg1,arg2,arg3,inFileName) {
    var query = `BEGIN
    XXYTYSPORT.XXSP_ADM_LOGIN_PKG.ADD_Attachment(:id,:id2,:id3,:id4);
   END;`
    return await dbConfig.saveAttachment(query,arg1,arg2,arg3,inFileName)
}

// async function getAllSupplier(email) {
//     var query = `BEGIN XXSP_ADM_LOGIN_PKG.USER_SUPPLIER_LIST_WRAP(:i,:o); END;`
//     var arg1 = email;
//     return await dbConfig.getSupplierListData(query, arg1)
// }

async function getDataBySupplierId(email,supplierId) {
    var query = `BEGIN XXSP_ADM_LOGIN_PKG.USER_DETAILS_WRAP(:i,:io,:o,:io1); END;`
    var arg1 = email;
    var arg3 = supplierId;
    var arg2 = undefined;

    return await dbConfig.getUserData(query, arg1,arg2,arg3)
}
// `BEGIN
//         XXYTYSPORT.XX_SP_USER_PKG.ADD_Attachment(:id,:id2(COMPANY/BANK),:id3(VENDOR_NAME/BANK_ACCOUNT_NO),:id4);
//        END;`
module.exports.getUserEmail = getUserEmail;
module.exports.tokenGeneration = tokenGeneration;
module.exports.verifyToken=verifyToken;
module.exports.userRegister=userRegister;
module.exports.updateAttachment=updateAttachment;
// module.exports.getAllSupplier = getAllSupplier;
module.exports.getDataBySupplierId = getDataBySupplierId;
