const oracledb = require('oracledb');
const dbConfig = require('../../database');


async function getFinance(email, financeType, date1, date2) {
    var query;
    if (date1 && date2) {
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o,:date1,:date2); END;`
    } else {
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o); END;`
    }
    var arg1 = email;
    var arg2 = financeType;
    var arg3 = date1;
    var arg4 = date2;
    return await dbConfig.getData(query, arg1, arg2, arg3, arg4)
}

module.exports.getFinance = getFinance;
