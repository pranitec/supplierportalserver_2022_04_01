const oracledb = require('oracledb');
const dbConfig = require('../../database');


async function getShipment(email, shipmentType, date1, date2) {
    var query;
    if (date1 && date2) {
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o,:date1,:date2); END;`
    } else {
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DETAILS_WRAP(:i,:io,:o); END;`
    }
    var arg1 = email;
    var arg2 = shipmentType;
    var arg3 = date1;
    var arg4 = date2;
    return await dbConfig.getData(query, arg1, arg2, arg3, arg4)
}

async function getShipmentReceipt(shipmentType, lineId) {
    var query;
        query = `BEGIN XXSP_CMN_VIEW_PKG.GET_DOC_DETAILS_WRAP(:i,:io,:o); END;`

    var arg1 = shipmentType;
    var arg2 = lineId;
    return await dbConfig.getDocDetails(query, arg1, arg2)
}

module.exports.getShipment = getShipment;
module.exports.getShipmentReceipt=getShipmentReceipt;
