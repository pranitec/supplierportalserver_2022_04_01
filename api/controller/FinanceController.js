
const FinanceService = require('../service/FinanceService');
const logger = require("../lib/logger");
const { Parser, transforms: { unwind } } = require('json2csv');


exports.getFinance = (req, res, next) => {
    FinanceService.getFinance(req.body.email,req.body.financeType,req.body.date1,req.body.date2)
        .then(doc => {
            logger.info('Get FinanceService Success');
            res.status(200).json(doc);
        })
        .catch(next);
}

exports.generateExcel=(req,res,next)=>{

        var fields = [
            {
                label: 'Invoice Type',
                value: 'TYPE'
            }, {
                label: 'Invoice No',
                value: 'INVOICE_NUM'
            }, {
                label: 'Voucher No',
                value: 'VOUCHER_NUMBER'
            },
            {
                label: 'Invoice Date',
                value: 'INVOICE_DATE'
            },
            {
                label: 'Currency',
                value: 'INVOICE_CURRENCY'
            },
            {
                label: 'Base Amount',
                value: 'BASE_AMOUNT'
            },
            {
                label: 'Tax Amount',
                value: 'TAX_AMOUNT'
            },
            {
                label: 'Freight Amount',
                value: 'FREIGHT_AMOUNT'
            },
            {
                label: 'Invoice Amount',
                value: 'INVOICE_AMOUNT'
            },
            {
                label: 'Status',
                value: 'PAYMENT_STATUS'
            }
           ];
        const json2csvParser = new Parser({ fields });
        logger.info('Get Finance Excel');
        const csv = json2csvParser.parse(req.body.POLINES);
        res.attachment('filename.csv');
        res.status(200).send(csv);

}

exports.generatePaymentExcel=(req,res,next)=>{

    var fields = [
           {
            label: 'Invoice No',
            value: 'INVOICE_NUM'
        }, {
            label: 'Voucher No',
            value: 'VOUCHER_NUMBER'
        },
        {
            label: 'Invoice Date',
            value: 'INVOICE_DATE'
        },
        {
            label: 'Currency',
            value: 'INVOICE_CURRENCY'
        },
        {
            label: 'Line Amount',
            value: 'LINE_AMOUNT'
        },
        {
            label: 'Tax Amount',
            value: 'TAX_AMOUNT'
        },
        {
            label: 'Freight Amount',
            value: 'FREIGHT_AMOUNT'
        },
        {
            label: 'Invoice Amount',
            value: 'INVOICE_AMOUNT'
        },
        {
            label: 'Status',
            value: 'PAYMENT_STATUS'
        }
       ];
    const json2csvParser = new Parser({ fields });
    logger.info('Get Finance Excel');
    const csv = json2csvParser.parse(req.body.POLINES);
    res.attachment('filename.csv');
    res.status(200).send(csv);

}

exports.generateAdvancePaymentExcel=(req,res,next)=>{

    var fields = [
         {
            label: 'Invoice No',
            value: 'INVOICE_NUM'
        }, {
            label: 'Voucher No',
            value: 'VOUCHER_NUMBER'
        },
        {
            label: 'Invoice Date',
            value: 'INVOICE_DATE'
        },
        {
            label: 'Currency',
            value: 'INVOICE_CURRENCY'
        },
        {
            label: 'Line Amount',
            value: 'LINE_AMOUNT'
        },
        {
            label: 'Tax Amount',
            value: 'TAX_AMOUNT'
        },
        {
            label: 'Freight Amount',
            value: 'FREIGHT_AMOUNT'
        },
        {
            label: 'Invoice Amount',
            value: 'INVOICE_AMOUNT'
        },
          {
            label: 'Purchase Order',
            value: 'PURCHASE_ORDER'
        },
        {
            label: 'Status',
            value: 'PAYMENT_STATUS'
        }
       ];
    const json2csvParser = new Parser({ fields });
    logger.info('Get Finance Excel');
    const csv = json2csvParser.parse(req.body.POLINES);
    res.attachment('filename.csv');
    res.status(200).send(csv);

}



exports.generateLedgerExcel=(req,res,next)=>{
    var fields = [
        {
            label: 'Document Type',
            value: 'TRANSACTION_TYPE'
        }, {
            label: 'Document No',
            value: 'TRANSACTION_NUM'
        }, {
            label: 'Document Date',
            value: 'TRANSACTION_DATE'
        },
        {
            label: 'Currency',
            value: 'CURRENCY'
        },
        {
            label: 'Debit',
            value: 'DEBIT'
        },
        {
            label: 'Credit',
            value: 'CREDIT'
        }
       ];
    const json2csvParser = new Parser({ fields });
    logger.info('Get Finance Excel');
    const csv = json2csvParser.parse(req.body.POLINES);
    res.attachment('filename.csv');
    res.status(200).send(csv);

}
