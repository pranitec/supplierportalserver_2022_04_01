
const ProfileManagementService = require('../service/ProfileManagementService');
const logger = require("../lib/logger");
const { Parser, transforms: { unwind } } = require('json2csv');
var js2xmlparser = require("js2xmlparser");
const clobInFileName = 'pdfs/demofile.txt';
const fs = require('fs');


exports.getProfileDetails = (req, res, next) => {
    ProfileManagementService.getProfileDetails(req.body.email,req.body.purchaseType)
        .then(doc => {
            logger.info('Get Profile Details Success');
            res.status(200).json(doc);
        })
        .catch(next); 
}



exports.saveProfileDetails = (req, res, next) => {
    let file = js2xmlparser.parse(req.body.type, req.body.details);
    fs.writeFile(clobInFileName, file, function (err) {
        if (err) throw err;
    });
    ProfileManagementService.saveProfileDetails(clobInFileName)
    .then(doc=>{
        res.status(200).json(doc);       
    })
    .catch(next);
}