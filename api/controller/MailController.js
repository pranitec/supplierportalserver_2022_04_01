

const mailService = require('../config/mailConfig');


exports.sendMail = (req, res, next) => {
  // console.log("send sms  & mail: ", req.body)
  try {
    // mailService.mailsend(req, res, req.body.to, req.body.subject, req.body.content);
    mailService.mailsend(req, res, req.body.to, req.body.subject, req.body.content);
    if(req.body.mobileNo)
    {
      mailService.smsSend(req, res);
    }
  } catch (error) {
    console.log("error", error)
    res.status(200).json('ERROR');
  }
}

exports.sendMailWithAttachments = (req, res, next) => {

  try {
    mailService.mailsendWithAttachments(req, res,req.body.to, req.body.subject, req.body.content, req.body.filename, req.body.base64);
    // mailService.smsSend(req, res);
    if(req.body.mobileNo)
    {
      mailService.smsSend(req, res);
    }
  }
  catch (error) {
    res.status(200).json('ERROR');
  }
}

exports.sendSMS = (req,res,next) =>{
  try{
      // mailService.smsSend(req,res);
      if(req.body.mobileNo)
      {
        mailService.smsSend(req, res);
      }
  }
  catch(error){
      res.status(200).json('ERROR');
  }
}

