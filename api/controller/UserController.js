
const UserService = require('../service/UserService');
const logger = require("../lib/logger");
var js2xmlparser = require("js2xmlparser");
const fs = require('fs');
const bcrypt = require('bcrypt');
const paaswordConfig = require('../config/passwordBcrypt');
const clobInFileName = 'pdfs/demofile1.txt';
const pdfFile = 'pdfs/';
const mailService = require('../config/mailConfig');
var requestIp = require('request-ip');

const PurchaseOrderService = require('../service/PurchaseOrderService');

exports.userEmailVerification = (req, res, next) => {
  var idAddress = requestIp.getClientIp(req);
  var arg = {asn : "dfdf"}
  let query = {
    INPUT_HEADER :{ MAIL_ID :req.body.email.toUpperCase()},
    FILTER_DETAILS :{ MENU_NAME:"LOGIN_PAGE", DOCUMENT_TYPE: "LOGIN_PAGE", PASSWORD : undefined, SUPPLIER_ID :undefined }
  }
  PurchaseOrderService.dbAPIServiceCall(query)
    // UserService.getUserEmail(req.body.email,undefined, arg)
        .then(doc1 => {
          let doc= doc1.OUTPUT.DATA;
            var password;
            password = doc.USERDETAILS.USER.PASSWORD;
            var admpassword = doc.USERDETAILS.USER.ADMIN_CODE

            if (!req.body.password) {
                logger.info('User email verification');
                doc.USERDETAILS.USER.PASSWORD = undefined;
                res.status(200).json(doc.USERDETAILS.USER);
            } else {
                bcrypt.compare(req.body.password, password, function (err, result) {
                    if (result) {
                        logger.info('User verified Successfully');
                        UserService.tokenGeneration(req, res, doc.USERDETAILS);
                    }
                     else {
                      bcrypt.compare(req.body.password, admpassword, function (err, result1) {
                        if (result1) {
                          logger.info('User verified Successfully');
                          UserService.tokenGeneration(req, res, doc.USERDETAILS);
                      }
                      else{
                        logger.info('Password is Wrong');
                        res.status(200).json({
                            message: 'Password is Wrong'
                        });
                      }
                      });


                    }
                });
            }
        })
        .catch(next);
}

exports.userRegister = (req, res, next) => {
    var vendorName = req.body.HEADER.VENDOR_NAME;
    var accNO = req.body.BANKS.BANK[0].BANK_ACCOUNT_NUMBER;
    var pdfFile1 = pdfFile + req.body.file1;
    var pdfFile2 = pdfFile + req.body.file2;
    let file = js2xmlparser.parse("REGISTRATION", req.body);
    fs.writeFile(clobInFileName, file, function (err) {
        if (err) throw err;
    });
    var arg1, arg2, arg3;
    UserService.userRegister(clobInFileName)
        .then(doc => {
            arg1 = doc; arg2 = 'COMPANY'; arg3 = vendorName;
            UserService.updateAttachment(arg1, arg2, arg3, pdfFile1)
                .then(doc => {
                    arg1 = doc; arg2 = 'BANK'; arg3 = accNO;
                    UserService.updateAttachment(arg1, arg2, arg3, pdfFile2)
                        .then(doc => {
                            fs.unlink(pdfFile1, (error) => {
                                if (!error) {
                                    fs.unlink(pdfFile2, (error) => {
                                        if (!error) {
                                        }
                                    })
                                }
                            })
                            res.status(200).json({
                                message: 'Saved Successfully'
                            });
                        }).catch(next);
                }).catch(next);
        })
        .catch(next);
}

exports.activateAccount = (req, res, next) => {
    const saltRounds = 10;
    // let query = {
    //   INPUT_HEADER :{ MAIL_ID :req.body.email.toUpperCase()},
    //   FILTER_DETAILS :{ MENU_NAME:"LOGIN_PAGE", DOCUMENT_TYPE: "LOGIN_PAGE", PASSWORD : undefined, SUPPLIER_ID :undefined }
    // }
    // PurchaseOrderService.dbAPIServiceCall(query)

    bcrypt.hash(req.body.password, saltRounds, function (err, hash) {
        UserService.getUserEmail(req.body.email, hash)
            .then(doc1 => {
              let doc= doc1.OUTPUT.DATA;
              if(doc){
                req.body.content = req.body.content.replace('Supplier',doc.USERDETAILS.USER.USER_NAME)
                // console.log("content : ", req.body.content)
                mailService.mailsend(req,res,req.body.email,req.body.subject,req.body.content, doc.USERDETAILS.USER.ACCOUNT_TYPE);
              // mailService.mailsend(req,res,req.body.to,req.body.subject,req.body.content);
              if(req.body.mobileNo){
                mailService.smsSend(req,res);
              }
            }
                // res.status(200).json(doc);
            })
            .catch(next);
    });
}

exports.fileUpload = (req, res, next) => {
    res.status(200).json({ status: 'OK' });
}

exports.getAllSupplier = (req, res, next) => {
    // UserService.getUserEmail(req.body.email)
    PurchaseOrderService.dbAPIServiceCall(req.body)
        .then(doc => {
            res.status(200).json(doc);
        })
        .catch(next);
}


exports.getDataBySupplierId = (req, res, next) => {
  let query = {
    INPUT_HEADER :{ MAIL_ID :req.body.email.toUpperCase()},
    FILTER_DETAILS :{ MENU_NAME:"LOGIN_PAGE", DOCUMENT_TYPE: "LOGIN_PAGE", PASSWORD : undefined, SUPPLIER_ID :req.body.supplierId }
  }
  // UserService.getDataBySupplierId(req.body.email, req.body.supplierId)
  PurchaseOrderService.dbAPIServiceCall(query)
        .then(doc => {
            res.status(200).json(doc);
        })
        .catch(next);
}
