
const ShipmentService = require('../service/ShipmentService');
const logger = require("../lib/logger");
const { Parser, transforms: { unwind } } = require('json2csv');


exports.getShipment = (req, res, next) => {
    ShipmentService.getShipment(req.body.email,req.body.shipmentType,req.body.date1,req.body.date2)
        .then(doc => {
            logger.info('Get Shipment Success');
            res.status(200).json(doc);
        })
        .catch(next); 
} 


exports.getShipmentReceipt = (req, res, next) => {
    ShipmentService.getShipmentReceipt(req.body.shipmentType,req.body.lineId)
        .then(doc => {
            logger.info('Get Shipment Success');
            res.status(200).json(doc);
        })
        .catch(next); 
} 