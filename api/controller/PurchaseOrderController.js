const PurchaseOrderService = require("../service/PurchaseOrderService");
const logger = require("../lib/logger");
const {
  Parser,
  transforms: { unwind },
} = require("json2csv");
const fs = require("fs");
var js2xmlparser = require("js2xmlparser");
const clobInFileName = "pdfs/demofile.txt";
const mailConfig = require("../config/mailConfig");
var path = require("path");
// const pdfFile = 'pdfs/';
const { FTP } = require("../config/config");
const pdfFile = FTP.filePath;
const DIR = FTP.filePath;
const TEMPDIR = FTP.filePath1;
const fileUploadPdf = require("../config/filePdf");
var path = require('path');

exports.getPendingPurchaseOrder = (req, res, next) => {
  // let file = { INPUT: req.body };
  let file =req.body
  PurchaseOrderService.dbAPIServiceCall(file)
    .then((doc) => {
      logger.info("Get Pending Purchase Order Success");
      res.status(200).json(doc.OUTPUT.DATA);
    })
    .catch(next);
};

exports.generateExcel = (req, res, next) => {
  // console.log("req.body : ", req.body.POLINES)
  var fields = [
    {
      label: "Unit",
      value: "BUYER_UNIT",
    },
    // {

    //   label: "Supplier Name",
    //   value: "DOCUMENT_NO",
    // },
    // {
    //   label: "Supplier Site",
    //   value: "DOCUMENT_NO",
    // },
    {
      label: "Document No",
      value: "DOCUMENT_NO",
    },
    {
      label: "Document Date",
      value: "DOCUMENT_DATE",
    },
    {
      label: "Currency",
      value: "PO_CURRENCY",
    },
    {
      label: "Line No",
      value: "PO_LINE_NUMBER",
    },
    {
      label: "Shipment No",
      value: "PO_SHIPMENT_NUMBER",
    },
    {
      label: "Item",
      value: "PO_LINE_ITEM",
    },
    {
      label: "Item Description",
      value: "ITEM_DESCRIPTION",
    },
    {
      label: "UOM",
      value: "UOM",
    },
    {
      label: "Quantity Ordered",
      value: "ORDERED_QTY",
    },
    {
      label: "Price",
      value: "PRICE",
    },
    {
      label: "Quantity Deliverded",
      value: "RECEIVED_QTY",
    },
    {
      label: "Balance Quantity",
      value: "BALANCE_QTY",
    },
    {
      label: "Promised Date",
      value: "PROMISED_DATE",
    },
    {
      label: "Division",
      value: "DIVISION",
    },
    {
      label: "Division Code",
      value: "DIVISION_CODE",
    },
  ];
  const json2csvParser = new Parser({ fields });
  logger.info("Get Purchase Excel");
  const csv = json2csvParser.parse(req.body.POLINES);
  res.attachment("filename.csv");
  res.status(200).send(csv);
};

exports.getDocs = (req, res, next) => {
  PurchaseOrderService.getDocs(
    req.body.email,
    req.body.purchaseType,
    req.body.attachmentString
  )
    .then((doc) => {
      logger.info("Get Pending Purchase Order Success");
      res.status(200).json(doc);
    })
    .catch(next);
};

exports.getAttachment = (req, res, next) => {
  PurchaseOrderService.getAttachmentFS(
    req.body.documentId,
    req.body.fileName,
    req.body.folder
  )
    .then((doc) => {
      if (doc) {
        logger.info("Get Pending Purchase Order Success");
        // res.download('pdfs/' + req.body.fileName);
        res.download(doc);
      } else {
        res.status(404).json();
      }
    })
    .catch(next);
};

exports.getDocDetails = (req, res, next) => {
  PurchaseOrderService.getDocDetails(req.body.printDocumentString)
    .then((doc) => {
      var __dirname = path.dirname(doc.outBinds.o2);
      var originalfilename = path.basename(doc.outBinds.o2);
      if (doc.outBinds.o1 === "FINAL") {
        PurchaseOrderService.getAttachmentFS(
          __dirname,
          originalfilename,
          doc.outBinds.o2
        )
          .then((doc1) => {
            if (doc1) {
              let filename=path.basename(doc1);
              // console.log("doc1 : ", path.basename(doc1));
              logger.info("Get Printing document Success");
              // res.writeHead(200, {'fileName': doc1});
              // res.download({doc: doc1, filename: filename });
              res.download(doc1)
              // var dolwnloadedFile = download(doc1);
              // res.status.json({doc: doc1, filename: dolwnloadedFile });
            } else {
              PurchaseOrderService.getAttachmentEBS(doc.outBinds.o2, doc.outBinds.o3)
              .then((doc2) => {
                let fileSize=fs.stat(doc2, (err, stats) => {
                  if (err) {
                    res.status(404).json({message : "File doesnt exist"})
                  } else {
                      // console.log(stats, stats.size);
                      if(stats.size<=4500){
                        res.status(404).json();
                      }else{
                        logger.info("Get Printing document Success");
                        // res.download('pdfs/' + req.body.fileName);
                        // var dolwnloadedFile = download(doc2);
                        // res.status.json({doc: doc2, filename: dolwnloadedFile });
                        res.download(doc2)
                      }
                  }
              });

              });
            }
          })
          .catch(next);
      } else {
        res.status(404).json();
      }
    })
    .catch(next);
};

exports.setAcceptance = (req, res, next) => {
  PurchaseOrderService.setAccept(req.body.headerId, req.body.action)
    .then((doc) => {
      logger.info("Set Accept Order Success");
      res.status(200).json(doc);
    })
    .catch(next);
};

exports.saveAcceptance = (req, res, next) => {
  let file = js2xmlparser.parse("PO_ACCEPTANCE", req.body);
  fs.writeFile(clobInFileName, file, function (err) {
    if (err) throw err;
  });
  PurchaseOrderService.saveAcceptance(clobInFileName)
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch(next);
};

exports.updateAttachment = (req, res, next) => {
  var pdfFile1 = pdfFile + req.body.FILE_NAME;
  var filename;
  var dir;
  var dir1;
  let file = { ATTACHMENT: req.body };
  PurchaseOrderService.updateAttachmentFS(pdfFile1, file)
    .then((doc) => {
      if (doc.o === "S") {
        var temp = doc.io;
        // dir1 = temp.split(/[\\/]/).pop();
        filename = path.basename(temp);
        dir = path.dirname(temp);
      }
      // PurchaseOrderService.fileuploadwithDIRandFN ()
      // .then(doc2 =>{
      // fileUploadPdf.upload
      // .then(doc1 =>{
      // console.log('doc2 : ', doc2)
      doc2 = { filename: filename, path: dir, o: "S" };
      logger.info("Set Accept Order Success");
      res.status(200).json(doc2);
      // }).catch(next);
    })
    .catch(next);
};

exports.saveASN = (req, res, next) => {
  // var pdfFile1 = pdfFile + req.body.FILE_NAME;
  // let file = js2xmlparser.parse("ASN", req.body);
  // let file = { ASN: req.body };
  var filename;
  var dir;
  // fs.writeFile(clobInFileName, file, function (err) {
  //     if (err) {
  //       console.log("eooror : ", err)
  //       throw err;}
  // });
  PurchaseOrderService.saveASN(req.body)
    .then((doc) => {
      let data;
      data = {
        DOCUMENT_ID: doc,
        FILE_NAME: req.body.ASN.FILE_NAME,
        TYPE: "ASN_DETAILS",
        DESCRIPTION: "Test file loading",
      };
      // let file1 = js2xmlparser.parse("ATTACHMENT", data);
      let file1 = { ATTACHMENT: data };
      // fs.writeFile(clobInFileName, file1, function (err) {
      //     if (err) throw err;
      // });
      var pdfFile1 = pdfFile + req.body.ASN.FILE_NAME;
      PurchaseOrderService.updateAttachmentFS(pdfFile1, file1)
        .then((doc1) => {
          if (doc1.o === "S") {
            var temp = doc1.io;
            filename = path.basename(temp);
            dir = path.dirname(temp);
          }
          doc2 = { filename: filename, path: dir, o: "S" };
          logger.info("Save ASN Success");
          res.status(200).json(doc2);
        })
        .catch(next);
    })
    .catch(next);
};

// exports.saveMenu = (req, res, next) => {
//    let file = { ASN: req.body };
//   PurchaseOrderService.saveAcceptance(file)
//     .then((doc) => {
//       res.status(200).json(doc);
//     })
//     .catch(next);
// };

exports.dbAPICall = (req, res, next) => {
  // var pdfFile1 = pdfFile + req.body.FILE_NME;
  // let file = js2xmlparser.parse("ASN", req.body);
  // let file = { ASN: req.body };
  var filename;
  var dir;
  var type = 0;
  console.log("dbAPICall : ", req.body);
  // fs.writeFile(clobInFileName, file, function (err) {
  //     if (err) {
  //       console.log("eooror : ", err)
  //       throw err;}
  // });
  PurchaseOrderService.dbAPIServiceCall(req.body)
    .then((doc) => {
      console.log(" save ASN doc : ", doc);
      if(doc.OUTPUT.ATTACHMENTS){
        var attachments = doc.OUTPUT.ATTACHMENTS.map(x=>x.ATTACHMENT);
        attachments.forEach(x => {
          var oldfile = path.join(TEMPDIR, x.OLD_FILE_NAME)
          var newfile =  path.join(DIR,x.NEW_FILE_NAME)
          if ( x.ACTION=== "MOVE") {
            try{
              fs.rename(oldfile, newfile, function(err) {
                if ( err ) {
                //   // throw err;
                  console.log("file not found")
                  return true
                  // res.status(200).json('ERROR');
                }
              });
          }
          catch(error){
              res.status(200).json('ERROR');
          }
        }
        });
        }
        if(doc.OUTPUT.COMMON_INFO.STATUS ==='S'){
          res.status(200).json(doc.OUTPUT.COMMON_INFO.DOCUMENT_ID)
        }else{
          res.status(200).json(doc.OUTPUT.COMMON_INFO.STATUS)
        }

        // }




      // else {
      //   res.status(404).json();
      // }
    })
    .catch(next);
};

exports.saveInvoice = (req, res, next) => {
  let file = js2xmlparser.parse("INVOICE", req.body);
  fs.writeFile(clobInFileName, file, function (err) {
    if (err) throw err;
  });
  PurchaseOrderService.saveInvoice(clobInFileName)
    .then((doc) => {
      let data;
      data = {
        DOCUMENT_ID: doc,
        FILE_NAME: req.body.INVOICE.FILE_NAME,
        TYPE: "INVOICE_DETAILS",
        DESCRIPTION: req.body.INVOICE.INVOICE_DESCRIPTION,
      };
      let file1 = js2xmlparser.parse("ATTACHMENT", data);
      fs.writeFile(clobInFileName, file1, function (err) {
        if (err) throw err;
      });
      var pdfFile1 = pdfFile + req.body.INVOICE.FILE_NAME;
      PurchaseOrderService.updateAttachment(file1, pdfFile1)
        .then((doc1) => {
          logger.info("Set Update Attachment For Inv Success");
          res.status(200).json(doc1);
        })
        .catch(next);
    })
    .catch(next);
};

exports.getPurchaseOrderList = (req, res, next) => {
  PurchaseOrderService.getPurchaseOrderList(
    req.body.email,
    req.body.purchaseType,
    req.body.date1,
    req.body.date2
  )
    .then((doc) => {
      logger.info("Get Pending Purchase Order Success");
      res.status(200).json(doc);
    })
    .catch(next);
};

exports.createInvoice = (req, res, next) => {
  //   var pdfFile1 = pdfFile + req.body.FILE_NAME;
  let file = js2xmlparser.parse("INVOICE", req.body);
  fs.writeFile(clobInFileName, file, function (err) {
    if (err) throw err;
  });
  PurchaseOrderService.createInvoice(clobInFileName)
    .then((doc) => {
      let data;
      data = {
        DOCUMENT_ID: doc,
        FILE_NAME: req.body.INVOICE.FILE_NAME,
        TYPE: "INVOICE_DETAILS",
        DESCRIPTION: "Test file loading",
      };
      let file1 = js2xmlparser.parse("ATTACHMENT", data);
      fs.writeFile(clobInFileName, file1, function (err) {
        if (err) throw err;
      });
      var pdfFile1 = pdfFile + req.body.INVOICE.FILE_NAME;
      PurchaseOrderService.updateAttachment(file1, pdfFile1)
        .then((doc1) => {
          logger.info("Set Accept Order Success");
          res.status(200).json(doc);
        })
        .catch(next);
    })
    .catch(next);
};

exports.createGateEntry = (req, res, next) => {
  let file = js2xmlparser.parse("GATE_ENTRY", req.body);
  fs.writeFile(clobInFileName, file, function (err) {
    if (err) throw err;
  });
  PurchaseOrderService.createGateEntry(clobInFileName)
    .then((doc) => {
      logger.info("createGateEntry Success");
      res.status(200).json(doc);
    })
    .catch(next);
};
