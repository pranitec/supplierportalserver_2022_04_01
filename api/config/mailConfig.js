const nodemailer = require("nodemailer");
const profileService = require('../service/ProfileManagementService')

const { ENV , EMAIL, SMS, TEMPLATES} = require('./config');
const http = require('http');
var request = require('request');
var axios = require('axios');

function mailsend(req,res,to,subject,body, accountType) {
  // console.log("mailsend : ", to,subject,body, accountType)
      const transporter = nodemailer.createTransport({
        host: EMAIL.EMAIL_HOST,
        secure: EMAIL.SSL,
        port: EMAIL.EMAIL_PORT ,
        auth: {
          user: EMAIL.EMAIL_USER,
          pass: EMAIL.EMAIL_PASS
        },
         proxy: EMAIL.PROXY_SERVER + EMAIL.PROXY_PORT,
         tls: {
          // do not fail on invalid certs
          rejectUnauthorized: false
      },

      });

      // const options;
      const options = {
        from: EMAIL.EMAIL_USER,
        to: to,
        // bcc : EMAIL.EMAIL_BCCUSER,
        subject: subject,
        html: body,
      }

    if(accountType === 'SUPPLIER'){
      const options = {
    from: EMAIL.EMAIL_USER,
    to: to,
    bcc : EMAIL.EMAIL_BCCUSER,
    subject: subject,
    html: body,
  }
}
      transporter.sendMail(options, (error, info) => {
        if (error) {
          console.log("error : ", error)
          res.status(200).json(error);
        } else {
          res.status(200).json('OK')
        }
      });
    }

  function mailsendWithAttachments(req,res,to,subject,body,filename,base64) {
    // profileService.getProfileDetails(ENV.mailId,'MAIL_SERVER').then(doc=>{

    //   if(doc.MAIL_SERVERS){

        const transporter = nodemailer.createTransport({
          // host:doc.MAIL_SERVERS.MAIL_SERVER.IP_ADDRESS,
          // secure: true,
          // port: Number( doc.MAIL_SERVERS.MAIL_SERVER.PORT_NUMBER) ,
          // auth: {
          //   user: (Buffer.from(doc.MAIL_SERVERS.MAIL_SERVER.USER_NAME,'base64')).toString('ascii'),
          //   pass: (Buffer.from(doc.MAIL_SERVERS.MAIL_SERVER.PASSWORD,'base64')).toString('ascii')
          // },
          // proxy: doc.MAIL_SERVERS.MAIL_SERVER.PROXY_SERVER + doc.MAIL_SERVERS.MAIL_SERVER.PROXY_PORT,
          host: EMAIL.EMAIL_HOST,
          secure: EMAIL.SSL,
          port: EMAIL.EMAIL_PORT ,
          auth: {
            user: EMAIL.EMAIL_USER,
            pass: EMAIL.EMAIL_PASS
          },
           proxy: EMAIL.PROXY_SERVER + EMAIL.PROXY_PORT,
           tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        },
        });

        const options = {
          from: EMAIL.EMAIL_USER,
          to: to,
          subject: subject,
          html: body,
          attachments: [
            {   // encoded string as an attachment
              filename: filename,
              // content: base64.split("base64,")[1],
              content:  base64.includes("base64,")?base64.split("base64,")[1] : base64,
              encoding:'base64'
            }
          ]
        };

        transporter.sendMail(options, (error, info) => {
          if (error) {
            res.status(200).json(error);
          } else {
            console.log("Email Error with attachment", error);
            res.status(200).json('OK')
          }
        });
    }

function smsSend(req, res) {
  var templateid;
  var message;
  const mapObj = {
    XXXX: req.body.field1,
    YYYY: req.body.field2,
    ZZZZ: req.body.field3
  };

  console.log("sms send", req.body.SMSType,req.body.mobileNo, req.body.field1, req.body.field2, req.body.field3);
  TEMPLATES.TEMPLATE.forEach(x=>{
    if(req.body.SMSType === x.SMSTYPE){
      templateid = x.TEMPLATEID
      message = x.MESSAGE.replace(/\b(?:XXXX|YYYY|ZZZZ)\b/gi, matched => mapObj[matched]);
    }
  })

  const request = require("request-promise");
  var url = "http://digimate.airtel.in:15080/BULK_API/InstantJsonPush";
  const options = {
    method: "POST",
    url: "http://digimate.airtel.in:15080/BULK_API/InstantJsonPush",
    body: {
      keyword:"DEMO",
      timeStamp:"121120171830",
      port :80,
      dataSet:    [
      {
        UNIQUE_ID: "1111111222",
        MESSAGE : message,
        OA: "CRAFTS",
        MSISDN :req.body.mobileNo,
        CHANNEL: "SMS",
        CAMPAIGN_NAME: "craftsman_tu",
        CIRCLE_NAME: "DLT_SERVICE_EXPLICT",
        USER_NAME: "craftsman_tu",
        DLT_TM_ID: "1001096933494158",
        DLT_CT_ID: templateid,
        DLT_PE_ID: "1001410221845124361"
      }
      ]
      },
    json: true,
    headers: {
      "Content-Type": "application/json",
    },
  };

  request(options)
    .then((parsedBody) => {
      // POST succeeded...
      console.log("SUCCESS! Posted");
      return null;
    })
    .catch((err) => {
      console.log("SMS ERROR", err);
      return null;
    });
}

  //   export const sendMessage = async (mobileNo, message, msgCode) => {
  //     var result;
  //     mobileNo = '+91' + mobileNo;
  //     message = message;
  //     let smsCode = Config.SERVER.SMS_CODE;
  //     if (msgCode === 1 && Number(smsCode) === 1) {
  //         await msg91.send(mobileNo, message, function (err, response) {
  //             if (err) {
  //                 result = err;
  //             }
  //             if (response) {
  //                 result = response;
  //             }
  //         });
  //     }
  // }

  module.exports.mailsend=mailsend;
  module.exports.mailsendWithAttachments = mailsendWithAttachments;
  module.exports.smsSend=smsSend;

