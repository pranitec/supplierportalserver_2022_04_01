const Production ={
    user          : "XXSPORT",
    password      : "calxxsp7",
    connectString : "192.168.32.19:1551/DEV",
    mailId:"info@pranitec.com",
    "port":'80',
    "origin":'https://supplier.craftsmanautomation.com/'
};

// const development ={
//     user          : "xxsport",
//     password      : "calsp7",
//     connectString : "172.16.8.37:1591/DEV",
//     mailId:"info@pranitec.com",
//     "port":'3000',
//     "origin":'http://localhost:4200'
// };

const development ={
  user          : "xxsport",
  password      : "calxxsp7",
  connectString : "172.16.8.37:1591/TEST",
  mailId:"info@pranitec.com",
  "port":'3000',
  "origin":'http://localhost:4200'
};


// const SERVER ={
// EMAIL_HOST : "pranitec.com",
// EMAIL_PORT : 465,
// EMAIL_USER : "info@pranitec.com",
// EMAIL_PASS : "pipl@123",
// EMAIL_CODE : 1,
// PROXY_PORT:'',
// PROXY_SERVER:'',
// SSL:true
// }

const FTP ={
      host: '10.100.100.98',
      port: '8080',
      username: ' CAL-SPORTAL\Administrator',
      password: 'Cal@sp22',
      filePath: "c:\\xxsp_fs",
      filePath1 : 'c:\\temp'
    }


const SERVER ={
  EMAIL_HOST : "mail.pranitec.com",
  EMAIL_PORT : 587,
  EMAIL_USER : "info@pranitec.com",
  EMAIL_BCCUSER : "info@pranitec.com",
  EMAIL_PASS : "pipl@123",
  EMAIL_CODE : 1,
  PROXY_PORT:'',
  PROXY_SERVER:'',
  SSL:false
  }

//   const FTP ={
//     host: '10.100.100.98',
//     port: '8080',
//     username: ' CAL-SPORTAL\Administrator',
//     password: 'Cal@sp22',
//     filePath: "c:\\xxsp_fs",
//     filePath1 : 'c:\\temp'
//   }

// const SMS ={
//   keyword:"DEMO",
//   timeStamp:"'||l_TIME_STAMP||'",
//   port :80,
//   dataSet:    [
//   {
//     UNIQUE_ID: "1111111222",
//     MESSAGE : "Congratulations! Your Supplier Portal account has been successfully created. CRAFTSMAN",
//     OA: "CRAFTS",
//     MSISDN :"8939833725",
//     CHANNEL: "SMS",
//     CAMPAIGN_NAME: "craftsman_tu",
//     CIRCLE_NAME: "DLT_SERVICE_EXPLICT",
//     USER_NAME: "craftsman_tu",
//     DLT_TM_ID: "1001096933494158",
//     DLT_CT_ID: "1007872414481865616",
//     DLT_PE_ID: "1001410221845124361"
//   }
//   ]
//   }

  const TEMPLATES = {

      TEMPLATE :
      [{
      SMSTYPE :"Supplier Portal Account Activation",
      TEMPLATEID :"1007872414481865616",
      MESSAGE : "Congratulations! Your Supplier Portal account has been successfully created. CRAFTSMAN"
      },
      {
        SMSTYPE : "PO Acceptance",
        TEMPLATEID :"1007714234087609994",
        MESSAGE :"Supplier XXXX acknowledged the receipt of your purchase order number YYYY CRAFTSMAN"
      },
      {
        SMSTYPE : "PO Rejected",
        TEMPLATEID :"1007483018617649360",
        MESSAGE : "Supplier XXXX rejected your purchase order number YYYY with reason - Not able to Deliver CRAFTSMAN"
      },
      {
        SMSTYPE : "ASN Creation",
        TEMPLATEID :"1007584135397132217",
        MESSAGE :"ASN XXXX has been generated successfully by Supplier YYYY CRAFTSMAN"
      },
      {
        SMSTYPE : "ASN Edit",
        TEMPLATEID :"1007500320894905805",
        MESSAGE :"ASN XXXX has been generated successfully with modified ASN ZZZZ by Supplier YYYY CRAFTSMAN"
      },
      {
        SMSTYPE : "ASN Cancel",
        TEMPLATEID :"1007739406787007645",
        MESSAGE :"ASN XXXX has been cancelled by Supplier YYYY CRAFTSMAN"
      }
    ]
  }


module.exports.ENV = development;
module.exports.EMAIL = SERVER;
module.exports.FTP = FTP;
// module.exports.SMS = SMS;
module.exports.TEMPLATES = TEMPLATES;
