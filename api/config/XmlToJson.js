var parseString = require('xml2js').parseString;

async function xmlToJson(clob) {
    const doStream = new Promise((resolve, reject) => {
        let myclob = "";
        let xmlResult;
        clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
        clob.on('error', (err) => {
            reject(err);
        });

        clob.on('data', (chunk) => {
            myclob += chunk;
        });

        clob.on('end', () => {
            clob.destroy();
            parseString(myclob, { explicitArray: false }, function (err, result) {
                xmlResult=result;
            });
            clob.on('close', () => {
                resolve(xmlResult);
            });
        });        
    });
   return await doStream;
}

module.exports.xmlToJson=xmlToJson;