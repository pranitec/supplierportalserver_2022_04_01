

const express=require('express');
const routes=express.Router();
const ShipmentController = require('../controller/ShipmentController');

routes.post('/',ShipmentController.getShipment);
routes.put('/',ShipmentController.getShipmentReceipt);

module.exports=routes;