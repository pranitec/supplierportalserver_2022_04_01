// CretedOn:20-08-2020

const express=require('express');
const routes=express.Router();
const PurchaseOrderController = require('../controller/PurchaseOrderController');
const login = require('../service/UserService')
const fileUploadPdf = require('../config/filePdf');

routes.post('/',PurchaseOrderController.getPendingPurchaseOrder);
routes.post('/list',PurchaseOrderController.getPurchaseOrderList);

//routes.post('/',PurchaseOrderController.getPastPurchaseOrder);
routes.post('/getExcel',login.verifyToken,PurchaseOrderController.generateExcel);
routes.post('/docs',PurchaseOrderController.getDocs);
routes.post('/print',PurchaseOrderController.getDocDetails);
routes.post('/attachment',login.verifyToken, PurchaseOrderController.getAttachment);
//routes.post('/accept',PurchaseOrderController.setAcceptance);
routes.post('/accept',PurchaseOrderController.saveAcceptance);
routes.post('/updateAttachment',PurchaseOrderController.updateAttachment);
// routes.post('/saveASN',PurchaseOrderController.saveASN);
routes.post('/saveASN',login.verifyToken,PurchaseOrderController.dbAPICall);
routes.post('/saveInvoice',PurchaseOrderController.saveInvoice);
routes.post('/createInvoice',PurchaseOrderController.createInvoice);
routes.post('/createGateEntry',PurchaseOrderController.createGateEntry);

module.exports=routes;
