// CretedOn:20-08-2020

const express=require('express');
const routes=express.Router();
const FinanceController = require('../controller/FinanceController');
const login = require('../service/UserService');

//routes.post('/',FinanceController.getPastPurchaseOrder);
routes.post('/getExcel',login.verifyToken,FinanceController.generateExcel);
routes.post('/getLedgerExcel',login.verifyToken,FinanceController.generateLedgerExcel);
routes.post('/getPaymentExcel',login.verifyToken,FinanceController.generatePaymentExcel);
routes.post('/getAdvancePaymentExcel',login.verifyToken,FinanceController.generateAdvancePaymentExcel);
routes.post('/',FinanceController.getFinance);

module.exports=routes;