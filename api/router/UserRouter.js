

const express=require('express');
const routes=express.Router();
const UserController = require('../controller/UserController');
const fileUploadPdf = require('../config/filePdf');
routes.post('/',UserController.userEmailVerification);
routes.post('/userRegister',UserController.userRegister);
routes.post('/account',UserController.activateAccount);
routes.post('/fileupload', fileUploadPdf.upload,UserController.fileUpload);
routes.post('/supplierList', fileUploadPdf.upload,UserController.getAllSupplier);
routes.post('/bySupplierId', fileUploadPdf.upload,UserController.getDataBySupplierId);
module.exports=routes;
