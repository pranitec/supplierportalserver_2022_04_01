

const express=require('express');
const routes=express.Router();

const mailController = require('../controller/MailController');

routes.post('/',mailController.sendMail);
routes.post('/attachments',mailController.sendMailWithAttachments);
routes.post('/sms',mailController.sendSMS);

module.exports=routes;
