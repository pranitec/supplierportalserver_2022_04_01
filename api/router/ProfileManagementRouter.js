// CretedOn:20-08-2020

const express=require('express');
const routes=express.Router();
const ProfileManagementController = require('../controller/ProfileManagementController');
const login = require('../service/UserService')

routes.post('/',ProfileManagementController.getProfileDetails);
routes.put('/',ProfileManagementController.saveProfileDetails); 
 

module.exports=routes;