const http=require('http');
 const { ENV } = require('./api/config/config');
const app=require('./index');
const mongoose=require('./api/service/UserService');
const port=process.env.PORT || ENV.port;
const server=http.createServer(app);
server.listen(port);
console.log('Connected Successfully', port);
