const { ENV } = require('./api/config/config');
const oracledb = require('oracledb');
var parseString = require('xml2js').parseString;
const fs = require('fs');
const { json } = require('body-parser');
const { Console } = require('console');
const { FTP} = require('./api/config/config');
var path = require('path');
var directoryfind= require('findit');
// const { blob } = require('node:stream/consumers');
const DIR = FTP.filePath
const { Readable } = require('stream');

dbConfig = {
    user: ENV.user,

    // Get the password from the environment variable
    // NODE_ORACLEDB_PASSWORD.  The password could also be a hard coded
    // string (not recommended), or it could be prompted for.
    // Alternatively use External Authentication so that no password is
    // needed.
    password: ENV.password,

    // For information on connection strings see:
    // https://oracle.github.io/node-oracledb/doc/api.html#connectionstrings
    connectString: ENV.connectString,

    // Setting externalAuth is optional.  It defaults to false.  See:
    // https://oracle.github.io/node-oracledb/doc/api.html#extauth
    externalAuth: process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
};

async function getDataByString(query, arg1, arg2) {
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        var result;
        result = await connection.execute(query,
            {
                i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                io: { val: arg2, dir: oracledb.BIND_INOUT },
                o: { type: oracledb.STRING, dir: oracledb.BIND_OUT },
            }
        )
        return result.outBinds.o;
    }
    catch (err) {
    }
}

async function getSupplierListData(query, arg1) {
  let connection;
  try {
      connection = await oracledb.getConnection(dbConfig);
      var result;
            result = await connection.execute(query,
              {
                  i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                  o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT }
              }
          );
      console.log("resulr ss : ", result)
      const clob = result.outBinds.o;
      const doStream = new Promise((resolve, reject) => {
          let myclob = "";
          let xmlResult, xmlresult1;
          clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
          clob.on('error', (err) => {
              reject(err);
          });

          clob.on('data', (chunk) => {
              myclob += chunk;
          });
          clob.on('end', () => {
              clob.destroy();
              // parseString(myclob, { explicitArray: false }, function (err, result) {
              //     xmlResult1 = result;
              // });
              xmlResult = JSON.parse(myclob);
              console.log("xmlResult : ", xmlResult);
              clob.on('close', () => {
                  resolve(xmlResult);
              });
          });
      });
      return await doStream;

  } catch (err) {
      console.error('sss', err);
  } finally {
      if (connection) {
          try {
              await connection.close();
          } catch (err) {
              console.error(err);
          }
      }
  }

}

async function getUserData(query, arg1, arg2, arg3, arg4) {
  let connection;
  console.log("arg : ", arg1, arg2, arg3, arg4)
  try {
      connection = await oracledb.getConnection(dbConfig);
      var result;
      if (arg3) {
              result = await connection.execute(query,
                  {
                      i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                      io: { val: arg2, dir: oracledb.BIND_INOUT },
                      o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT },
                      io1: { val: arg3, dir: oracledb.BIND_INOUT }
                  }
              );
          }else{
            console.log("else")
            result = await connection.execute(query,
              {
                  i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                  io: { val: arg2, dir: oracledb.BIND_INOUT },
                  o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT },
                  io1: { val: arg3, dir: oracledb.BIND_INOUT },
                  io2: { val: arg4, dir: oracledb.BIND_INOUT }
              }
          );
          }


      console.log("resulr ss : ", result)
      const clob = result.outBinds.o;
      const doStream = new Promise((resolve, reject) => {
          let myclob = "";
          let xmlResult, xmlresult1;
          clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
          clob.on('error', (err) => {
              reject(err);
          });

          clob.on('data', (chunk) => {
              myclob += chunk;
          });
          clob.on('end', () => {
              clob.destroy();
              // parseString(myclob, { explicitArray: false }, function (err, result) {
              //     xmlResult1 = result;
              // });
              xmlResult = JSON.parse(myclob);
              // console.log("xmlResult : ", xmlResult);
              clob.on('close', () => {
                  resolve(xmlResult);
              });
          });
      });
      return await doStream;

  } catch (err) {
      console.error('sss', err);
  } finally {
      if (connection) {
          try {
              await connection.close();
          } catch (err) {
              console.error(err);
          }
      }
  }

}

async function getDocDetails(query, arg1, arg2, arg3, arg4, arg5) {
  let connection;
  try {
      connection = await oracledb.getConnection(dbConfig);
      var result;
              result = await connection.execute(query,
                  {
                      i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                      io: { val: arg2, dir: oracledb.BIND_INOUT },
                      io1: { val: arg3, dir: oracledb.BIND_INOUT },
                      o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT },
                      date1: { val: arg4, dir: oracledb.BIND_INOUT },
                      date2: { val: arg5, dir: oracledb.BIND_INOUT }
                  }
              );

      console.log("resulr ss : ", result)
      const clob = result.outBinds.o;
      const doStream = new Promise((resolve, reject) => {
          let myclob = "";
          let xmlResult, xmlresult1;
          clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
          clob.on('error', (err) => {
              reject(err);
          });

          clob.on('data', (chunk) => {
              myclob += chunk;
          });
          clob.on('end', () => {
              clob.destroy();
              // parseString(myclob, { explicitArray: false }, function (err, result) {
              //     xmlResult1 = result;
              // });
              xmlResult = JSON.parse(myclob);
              // console.log("xmlResult : ", xmlResult);
              clob.on('close', () => {
                  resolve(xmlResult);
              });
          });
      });
      return await doStream;

  } catch (err) {
      console.error('sss', err);
  } finally {
      if (connection) {
          try {
              await connection.close();
          } catch (err) {
              console.error(err);
          }
      }
  }

}


async function getData(query, arg1, arg2, arg3, arg4) {
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        var result;
        if (arg3 && arg4) {
                result = await connection.execute(query,
                    {
                        i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                        io: { val: arg2, dir: oracledb.BIND_INOUT },
                        o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT },
                        date1: { val: arg3, dir: oracledb.BIND_INOUT },
                        date2: { val: arg4, dir: oracledb.BIND_INOUT }
                    }
                );
                  }
            else {
              // GET_DOC_DETAILS_WRAP('senthilkumar.r@pranitec.com','AP_INVOICE_ATTACHMENTS',p_invoice_id,l_OUTPUT);           console.log("inn, else")
                result = await connection.execute(query,
                    {
                        i: { val: arg1, dir: oracledb.BIND_INOUT },  // Bind type is determined from the data.  Default direction is BIND_IN
                        io: { val: arg2, dir: oracledb.BIND_INOUT },
                        // io1: { val: arg3, dir: oracledb.BIND_INOUT },
                        o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT },
                    }
                );
            }

        // else if(arg2){
        //   console.log("arg 2");
        //     if(arg2==='pass'){
        //         arg2=undefined;
        //     }
        //     result = await connection.execute(query,
        //         {
        //             i: { val: arg1, dir: oracledb.BIND_INOUT },
        //             io: { val: arg2, dir: oracledb.BIND_INOUT }, // Bind type is determined from the data.  Default direction is BIND_IN
        //             o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT }
        //         }
        //     );
        // }
        // else{
        //   console.log("arg1");
        //     result = await connection.execute(query,
        //         {
        //             i: { val: arg1, dir: oracledb.BIND_INOUT },
        //             o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT }

        //         }
        //     );
        // }
        console.log("resulr ss : ", result)
        const clob = result.outBinds.o;
        const doStream = new Promise((resolve, reject) => {
            let myclob = "";
            let xmlResult, xmlresult1;
            clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
            clob.on('error', (err) => {
                reject(err);
            });

            clob.on('data', (chunk) => {
                myclob += chunk;
            });
            clob.on('end', () => {
                clob.destroy();
                // parseString(myclob, { explicitArray: false }, function (err, result) {
                //     xmlResult1 = result;
                // });
                xmlResult = JSON.parse(myclob);
                // console.log("xmlResult : ", xmlResult);
                clob.on('close', () => {
                    resolve(xmlResult);
                });
            });
        });
        return await doStream;

    } catch (err) {
        console.error('sss', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

async function getDataPrintDocument(query, arg1) {
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        var result;
            result = await connection.execute(query,
                {
                    i: { val: arg1, dir: oracledb.BIND_INOUT },
                    o1: { type: oracledb.STRING, dir: oracledb.BIND_OUT },
                    o2: { type: oracledb.STRING, dir: oracledb.BIND_OUT },
                    o3: { type: oracledb.STRING, dir: oracledb.BIND_OUT },
                }
            );
        console.log("resulr ss : ", result)
        return result
        // var ffff;
        // var fileurl=result.outBinds.o3

        // var __dirname = path.dirname('\\PURCHASE_ORDER_PRINT\\2005982_.pdf')
        // var originalfilename = path.basename('\\PURCHASE_ORDER_PRINT\\2005982_.pdf')
                // var Ofile = filename[i].split("_")

        // var startPath = path.join(DIR,__dirname)
        // console.log("startPath 2: ", startPath)
        // if(result.outBinds.o1 === 'FINAL'){
        //   var filename= fs.readdirSync(startPath, (err, files) => {
        //     if (err) {
        //       console.log("erre" , err);
        //       return;
        //     }
        //      })
        //     for(var i=0;i<filename.length;i++){
        //       if (filename[i] === originalfilename) {
        //         var ffff= path.join(startPath,filename[i]);
        //         console.log("found", ffff)
        //       }
        //     }
        //       if(ffff){
        //         return ffff;
        //       }
        //       else{
        //         console.log("no file found block")
        //         // filename=path.basename(result.outBinds.o2)
        //         filename=path.basename('\\PURCHASE_ORDER_PRINT\\2005982_.pdf')
        //         var uploadedFilename = path.join(startPath, originalfilename)
        //         console.log("uploadedPath : " , uploadedFilename)
        //         var download =  (url, dest, cb) =>{
        //           var file = fs.createWriteStream(dest);
        //           var request = http.get(url, function(response) {
        //             response.pipe(file);
        //             file.on('finish', function() {
        //               console.log("file downloaded")
        //               ffff=uploadedFilename;
        //               file.close(cb); // close() is async, call cb after close completes.
        //             });
        //           }).on('error', function(err) { // Handle errors
        //             fs.unlink(dest); // Delete the file async. (But we don't check the result)
        //             if (cb) cb(err.message);
        //           });
        //         };
        //         var filedownload = await download(fileurl, uploadedFilename, (data) => {
        //           return ffff;
        //           console.log("datataa : ", data)
        //         })
        //       }

        // }else{
        //   console.log("else ")
        //   return null;
        // }


    } catch (err) {
        console.error('sss', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

async function saveData(query, inFileName, outputType) {
  console.log("inFileName : ",inFileName)
    let connection;
    try {
      console.log("indise : ")
      var inStream;
        connection = await oracledb.getConnection(dbConfig);
        const tempLob = await connection.createLob(oracledb.CLOB);
        // const doStream1 = new Promise((resolve, reject) => {
        //     tempLob.on('error', (err) => {
        //       console.log("reject")
        //         reject(err);
        //     });

        //     tempLob.on('finish', () => {
        //       console.log("resolve")
        //         resolve();
        //     });
            // const s = JSON.stringify(inFileName);  // change JavaScript value to a JSON string
        const b = Buffer.from(JSON.stringify(inFileName), 'utf8');
        console.log("dddd : ", b)
            // inStream =Readable.from(b.toString());
            // inStream.on('error', (err) => {
            //   console.log("errordddd : ",err);
            //     tempLob.destroy(err);
            // });
            // inStream.pipe(tempLob);
        // });
//
        // await doStream1;
        var result;
        if (outputType === 1) {
            result = await connection.execute(query,
                {
                    i: { val: tempLob, dir: oracledb.BIND_INOUT, type: oracledb.CLOB },
                    o: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER, maxSize: 40 } // Bind type is determined from the data.  Default direction is BIND_IN
                }
            );
        } else if (outputType === 2) {
            result = await connection.execute(query,
                {
                    i: { val: tempLob, dir: oracledb.BIND_INOUT, type: oracledb.CLOB },
                    o: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 40 },
                }
            );
        } else if (outputType === 3) {
          console.log(" 3 inn")
            result = await connection.execute(query,
                {
                    i: { val: b, dir: oracledb.BIND_INOUT, type: oracledb.BLOB },
                    o2: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 40 },
                    o: { dir: oracledb.BIND_OUT, type: oracledb.NUMBER, maxSize: 40 }  // Bind type is determined from the data.  Default direction is BIND_IN
                }
            );
        }
        console.log("result : ", result)
        const clob = result.outBinds.o;
        tempLob.destroy();
        await new Promise((resolve, reject) => {
            tempLob.on('error', reject);
            tempLob.on('close', resolve);
        });

        return clob;
    } catch (err) {
        console.error('sss', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

async function dbAPICallMain(query, inFile) {
  console.log("dbAPICallMain : ",inFile)
  let inFileName = { INPUT: inFile };
    let connection;
    try {
      var inStream;

        connection = await oracledb.getConnection(dbConfig);
        const tempLob = await connection.createLob(oracledb.CLOB);
        const b = Buffer.from(JSON.stringify(inFileName), 'utf8');
        // console.log("dddd : ", b)
          // inStream =Readable.from(b.toString());
          //   inStream.on('error', (err) => {
          //     console.log("errordddd : ",err);
          //       tempLob.destroy(err);
          //   });
          //   inStream.pipe(tempLob);

        var result;
            result = await connection.execute(query,
                {
                    i: { val: b, dir: oracledb.BIND_INOUT, type: oracledb.DB_TYPE_BLOB },
                    // o: { dir: oracledb.BIND_OUT, type: oracledb.CLOB } // Bind type is determined from the data.  Default direction is BIND_IN
                    o: { type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_OUT }
                  }
            );

        console.log("result : ", result)
        const clob = result.outBinds.o;
        // tempLob.destroy();
        // await new Promise((resolve, reject) => {
        //     tempLob.on('error', reject);
        //     tempLob.on('close', resolve);
        // });

        // return clob;
        const doStream = new Promise((resolve, reject) => {
          let myclob = "";
          let xmlResult, xmlresult1;
          clob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
          clob.on('error', (err) => {
              reject(err);
          });

          clob.on('data', (chunk) => {
              myclob += chunk;
          });
          clob.on('end', () => {
              clob.destroy();
              // parseString(myclob, { explicitArray: false }, function (err, result) {
              //     xmlResult1 = result;
              // });
              xmlResult = JSON.parse(myclob);
              console.log("xmlResult : ", xmlResult);
              clob.on('close', () => {
                  resolve(xmlResult);
              });
          });
      });
      return await doStream;
    } catch (err) {
        console.error('sss xx', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}



async function saveAttachment(query, arg1, arg2, arg3, inFileName) {
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        const tempLob = await connection.createLob(oracledb.BLOB);
        const doStream1 = new Promise((resolve, reject) => {

            tempLob.on('error', (err) => {
                reject(err);
            });
            tempLob.on('finish', () => {
                // The data was loaded into the temporary LOB
                resolve();
            });
            const inStream = fs.createReadStream(inFileName);
            inStream.on('error', (err) => {
                tempLob.destroy(err);
            });
            inStream.pipe(tempLob);  // copies the text to the temporary LOB

        });
        await doStream1;
        var result;
        console.log('Calling PL/SQL to insert the temporary LOB into the database');
        result = await connection.execute(query,
            {

                id: { val: arg1, type: oracledb.DB_TYPE_NUMBER, dir: oracledb.BIND_IN },
                id2: { val: arg2, type: oracledb.DB_TYPE_VARCHAR, dir: oracledb.BIND_IN },
                id3: { val: arg3, type: oracledb.DB_TYPE_VARCHAR, dir: oracledb.BIND_IN },
                id4: { val: tempLob, type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_INOUT }, // type and direction are optional for IN binds
            }, // Bind type is determined from the data.  Default direction is BIND_IN

        );
        tempLob.destroy();

        // Wait for destroy() to emit the the close event.  This means the lob will
        // be cleanly closed before the app closes the connection, otherwise a race
        // will occur.
        await new Promise((resolve, reject) => {
            tempLob.on('error', reject);
            tempLob.on('close', resolve);
        });
        const clob = result.outBinds.id4;
        return clob;
    } catch (err) {
        console.error('sss', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}

async function updateAttachment(query, inFileName, pdfFile) {
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        // const tempLob = await connection.createLob(oracledb.BLOB);
        // const doStream1 = new Promise((resolve, reject) => {

        //     tempLob.on('error', (err) => {
        //         reject(err);
        //     });
        //     tempLob.on('finish', () => {
        //         // The data was loaded into the temporary LOB
        //         resolve();
        //     });

        //     const inStream = fs.createReadStream(pdfFile);
        //     inStream.on('error', (err) => {
        //         tempLob.destroy(err);
        //     });
        //     inStream.pipe(tempLob);  // copies the text to the temporary LOB
        // });
        // await doStream1;
        const s = JSON.stringify(pdfFile);  // change JavaScript value to a JSON string
        const b = Buffer.from(s, 'utf8');
        var result;
        console.log('Calling PL/SQL to insert the temporary LOB into the database');
        result = await connection.execute(query,
            {
                i: { val: s, type: oracledb.STRING, dir: oracledb.BIND_IN },
                // io: { val:inFileName , type: oracledb.DB_TYPE_BLOB, dir: oracledb.BIND_IN },
                io: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 1000 },
                o: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 500 } // type and direction are optional for IN binds
            }, // Bind type is determined from the data.  Default direction is BIND_IN
        );
        // tempLob.destroy();
        // await new Promise((resolve, reject) => {
        //     tempLob.on('error', reject);
        //     tempLob.on('close', resolve);
        // });
        console.log("result : ", result)
        const clob = result.outBinds;
        return clob;
    } catch (err) {
        console.error('sss fff', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}


async function documentDownload(query, arg1, arg2, folder) {
  // console.log("getAttachmentFS : ",arg1, arg2, folder )
  var __dirname = path.dirname(folder)
  var originalfilename=path.basename(folder)
  console.log("open file folder : ",originalfilename )
    let connection;
    try {
        connection = await oracledb.getConnection(dbConfig);
        var result;
        var startPath = path.join(DIR,__dirname)
        var filename= fs.readdirSync(startPath, (err, files) => {
          if (err) {
            console.log("erre" , err);
            return;
          }
           })
          for(var i=0;i<filename.length;i++){
            if (filename[i] === originalfilename) {
              // var Ofile = filename[i].split("_")
              var ffff= path.join(startPath,filename[i]);
              console.log("found", ffff)
                 return ffff;
            }
        }
        //    const outStream = fs.createWriteStream(originalfilename);
        //    console.log("outStream : ", outStream)
        //     outStream.on('error', (err) => {
        //         lob.destroy(err);
        //     });
        //     lob.pipe(outStream);
        //     return outStream;
          // result= fs.readdir("outFileName", (err, files) => { console.log(files) })
        // });
        // await doStreamHelper;
    } catch (err) {
        console.error('sss', err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}



module.exports.getData = getData;
module.exports.saveData = saveData;
module.exports.saveAttachment = saveAttachment;
module.exports.documentDownload = documentDownload;
module.exports.getDataByString = getDataByString;
module.exports.updateAttachment = updateAttachment;
module.exports.getDataPrintDocument = getDataPrintDocument;
module.exports.getDocDetails = getDocDetails;
module.exports.getUserData = getUserData;
module.exports.getSupplierListData = getSupplierListData;
module.exports.dbAPICallMain = dbAPICallMain;





