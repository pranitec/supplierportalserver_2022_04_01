const express = require('express');
const bodyparser = require('body-parser');
const path = require('path');
const morgan = require('morgan');
const cors = require('cors');
 const logger = require("./api/lib/logger");
 const { ENV } = require('./api/config/config');


 const user=require('./api/router/UserRouter');
 const purchase=require('./api/router/PurchaseOrderRouter');
 const finance=require('./api/router/FinanceRouter');
 const shipment=require('./api/router/ShipmentRouter');
 const profile=require('./api/router/ProfileManagementRouter');
 const mail = require('./api/router/MailRouter');

 //const mail = require('./api/config/mailConfig');
 //  mail.mailsend(['ganesan.marimuthu@shinelogics.com','kalidas.l@pranitec.com'],"Testing",'Testing');
const app = express();
 app.use(cors());
app.use(bodyparser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'dist/supplier')))
app.use(bodyparser.json({ limit: '5mb' }));
app.use(morgan('dev'));
app.use(bodyparser.json());

app.use('/api/users',user);
app.use('/api/purchase',purchase);
app.use('/api/finance',finance);
app.use('/api/shipment',shipment);
app.use('/api/profile',profile);
app.use('/api/mail',mail);

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/supplier/index.html'))
});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Orgin", "http://localhost:4200");
    res.header("Access-Control-Allow-Header", "Orgin,X-Request-With, Content-Type, Accept");
    res.header("Cache-Control", "no-cache, no-store, must-revalidate");
    res.header("Pragma", "no-cache");
    res.header("Expires", 0);
    next();
});



app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    logger.error('error',error.message);
    if (error.name === 'ValidationError') {
        logger.error(error.name);
        res.status(error.status || 500).json({
            error: {
                message: error.message
            }
        });
    } else {
        logger.error(error.message);
        res.status(error.status || 500).json({
            error: {
                message: error
            }
        });
    }
});

module.exports = app;