function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["components-finance-finance-module"], {
  /***/
  "./src/app/components/auth/service/email.service.ts":
  /*!**********************************************************!*\
    !*** ./src/app/components/auth/service/email.service.ts ***!
    \**********************************************************/

  /*! exports provided: EmailService */

  /***/
  function srcAppComponentsAuthServiceEmailServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EmailService", function () {
      return EmailService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");

    var EmailService =
    /*#__PURE__*/
    function () {
      function EmailService(http) {
        _classCallCheck(this, EmailService);

        this.http = http;
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiBaseUrl + '/mail';
      }

      _createClass(EmailService, [{
        key: "sendMail",
        value: function sendMail(to, subject, body, smsType, mobile, field1, field2, field3) {
          // console.log("rrrr : ", smsType, mobile, field1, field2)
          // return Email.send({
          // //  SecureToken: '9e359c94-801c-44b3-98f9-f47aafb4031e',
          //  // SecureToken: 'b880bdf7-bb84-4078-9990-499e7bf85453',
          //  SecureToken: '796cffa6-0fd5-40e9-844e-75f3f3aed6b8',
          //   To: `${to}`,
          //   From: `info.yty@ytygroup.com.my`,
          //   Subject: `${subject}`,
          //   Body: `${body}`
          // });
          return this.http.post("".concat(this.baseUrl, "/"), {
            to: to,
            subject: subject,
            content: body,
            SMSType: smsType,
            mobileNo: mobile,
            field1: field1,
            field2: field2,
            field3: field3
          });
        }
      }, {
        key: "sendMailWithAttachments",
        value: function sendMailWithAttachments(to, subject, body, fileName, base64, smsType, mobile, field1, field2, field3) {
          // console.log("rrrr : ", to,subject,body,fileName,base64, smsType, mobile, field1, field2, field3)
          // return Email.send({
          //   SecureToken: '796cffa6-0fd5-40e9-844e-75f3f3aed6b8',
          //   To: `${to}`,
          //   From: `info.yty@ytygroup.com.my`,
          //   Subject': `${subject}`,
          //   Body: `${body}`,
          //   Attachments : [
          //     {
          //       name:fileName,
          //         data:base64
          //     }
          //   ]
          // });
          return this.http.post("".concat(this.baseUrl, "/attachments"), {
            to: to,
            subject: subject,
            content: body,
            filename: fileName,
            base64: base64,
            SMSType: smsType,
            mobileNo: mobile,
            field1: field1,
            field2: field2,
            field3: field3
          });
        }
      }]);

      return EmailService;
    }();

    EmailService.ɵfac = function EmailService_Factory(t) {
      return new (t || EmailService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    EmailService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: EmailService,
      factory: EmailService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](EmailService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/auth/service/login.service.ts":
  /*!**********************************************************!*\
    !*** ./src/app/components/auth/service/login.service.ts ***!
    \**********************************************************/

  /*! exports provided: LoginService */

  /***/
  function srcAppComponentsAuthServiceLoginServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginService", function () {
      return LoginService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");

    var LoginService =
    /*#__PURE__*/
    function () {
      function LoginService(http, rsaService, breadcrumbService) {
        _classCallCheck(this, LoginService);

        this.http = http;
        this.rsaService = rsaService;
        this.breadcrumbService = breadcrumbService;
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiBaseUrl + '/users';
      }

      _createClass(LoginService, [{
        key: "getEmail",
        value: function getEmail(email) {
          var _this2 = this;

          return this.http.post("".concat(this.baseUrl), email).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (x) {
            if (x) {
              _this2.setToken(x);
            } else {
              setTimeout(function () {
                _this2.setToken(x);
              }, 3600);
            }
          }));
        }
      }, {
        key: "getUserDetails",
        value: function getUserDetails(data) {
          return this.http.post("".concat(this.baseUrl, "/account"), data);
        }
      }, {
        key: "setToken",
        value: function setToken(data) {
          // console.log("setToken data : ", data, data.user.USERACCESS)
          if (data.token) {
            // console.log("USERACCESS : ", data.user.USERACCESS)
            var token = data.token;
            this.user = data.user; // this.getMenu();

            localStorage.clear();
            localStorage.setItem('1', token);
            localStorage.setItem('2', this.rsaService.encrypt(JSON.stringify(this.user))); // var useraccess = (data.user.USERACCESSES.map(x=>x.USERACCESS))
            // useraccess.sort((a, b) =>a.ERP_ORGANIZATION_NAME < b.ERP_ORGANIZATION_NAME ? -1 : a.ERP_ORGANIZATION_NAME > b.ERP_ORGANIZATION_NAME ? 1 : 0);
            // localStorage.setItem('USERACCESS', this.rsaService.encrypt(JSON.stringify(useraccess)));

            localStorage.setItem('3', this.rsaService.encrypt(data.user.USER.MAIL_ID));
            localStorage.setItem('4', this.rsaService.encrypt(data.user.USER.USER_NAME));
            localStorage.setItem('5', data.user.USER.NO_OF_PARTIES);
            localStorage.setItem('6', this.rsaService.encrypt(data.user.USER.ACCOUNT_TYPE)); // this.breadcrumbService.setSelectValues(data.user.USERACCESSES.map(x=>x.USERACCESS),data.user.USER.NO_OF_PARTIES ||0);
          }
        }
      }, {
        key: "logout",
        value: function logout() {
          localStorage.clear();
        }
      }, {
        key: "tokenVerfiy",
        value: function tokenVerfiy() {
          return this.http.get("".concat(this.baseUrl, "/tokenVerfiy"));
        }
      }, {
        key: "registerUser",
        value: function registerUser(data) {
          return this.http.post("".concat(this.baseUrl, "/userRegister"), data);
        }
      }, {
        key: "FileUpload",
        value: function FileUpload(files) {
          var httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
              type: this.type,
              filetype: this.fileType
            })
          };
          return this.http.post("".concat(this.baseUrl, "/fileUpload"), files, httpOptions);
        }
      }]);

      return LoginService;
    }();

    LoginService.ɵfac = function LoginService_Factory(t) {
      return new (t || LoginService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__["BreadcrumbService"]));
    };

    LoginService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: LoginService,
      factory: LoginService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](LoginService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__["RsaService"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_5__["BreadcrumbService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/advance-payment/advance-payment.component.ts":
  /*!*********************************************************************************!*\
    !*** ./src/app/components/finance/advance-payment/advance-payment.component.ts ***!
    \*********************************************************************************/

  /*! exports provided: AdvancePaymentComponent */

  /***/
  function srcAppComponentsFinanceAdvancePaymentAdvancePaymentComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AdvancePaymentComponent", function () {
      return AdvancePaymentComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _service_finance_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../service/finance.service */
    "./src/app/components/finance/service/finance.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ../file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");
    /* harmony import */


    var _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../shared/service/number-formatting.pipe */
    "./src/app/shared/service/number-formatting.pipe.ts");

    function AdvancePaymentComponent_div_88_div_4_tr_88_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](17, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r572 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r572.INVOICE_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element1_r572.VOUCHER_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r572.INVOICE_DATE, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r572.INVOICE_CURRENCY);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 12, element1_r572.LINE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 14, element1_r572.TAX_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](17, 16, element1_r572.INVOICE_AMOUNT), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r572.PURCHASE_ORDER, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](element1_r572.PAYMENT_STATUS);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r572.PAYMENT_STATUS, "");
      }
    }

    function AdvancePaymentComponent_div_88_div_4_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, " Organization");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Payment Reference No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " Unit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Payment Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Payment Method");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Amount Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](44, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47, "Bank");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Account Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "508298635678 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "UTR Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](62, "app-file-view", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "table", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "thead", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "Invoice No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](72, "Voucher No ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "th", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Line Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "th", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "th", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Invoice Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Purchase Order");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Payment Status");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](88, AdvancePaymentComponent_div_88_div_4_tr_88_Template, 23, 18, "tr", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r569 = ctx.$implicit;
        var i_r570 = ctx.index;

        var ctx_r568 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "heading", i_r570, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.ORGANIZATION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.PAYMENT_REF_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.UNIT);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.PAYMENT_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.PAYMENT_METHOD);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r569.CURRENCY, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](44, 17, element_r569.AMOUNT_PAID));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.Bank);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r569.UTR_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("headerId", element_r569.INVOICE_ID)("type", "AP_INVOICE_ATTACHMENTS")("communicationString", element_r569.COMMUNICATION_STRING);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "collapse ", i_r570, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate1"]("aria-labelledby", "heading", i_r570, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "test", i_r570, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r568.payment[i_r570].filteredList);
      }
    }

    var _c0 = function _c0(a0, a1) {
      return {
        itemsPerPage: a0,
        currentPage: a1
      };
    };

    function AdvancePaymentComponent_div_88_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, AdvancePaymentComponent_div_88_div_4_Template, 89, 19, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "paginate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r565 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](5, 1, ctx_r565.payment, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](4, _c0, ctx_r565.viewCount, ctx_r565.page)));
      }
    }

    function AdvancePaymentComponent_div_89_tr_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](17, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r574 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r574.INVOICE_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element1_r574.VOUCHER_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r574.INVOICE_DATE, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r574.INVOICE_CURRENCY);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 12, element1_r574.LINE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 14, element1_r574.TAX_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](17, 16, element1_r574.INVOICE_AMOUNT), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r574.PURCHASE_ORDER, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](element1_r574.PAYMENT_STATUS);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r574.PAYMENT_STATUS, "");
      }
    }

    function AdvancePaymentComponent_div_89_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 77);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 78);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 79);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "button", 80);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "GO Back");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 81);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "table", 82);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "thead", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Invoice No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Voucher No ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Line Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Invoice Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "Purchase Order");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Payment Status");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, AdvancePaymentComponent_div_89_tr_29_Template, 23, 18, "tr", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r566 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r566.paymentLine);
      }
    }

    function AdvancePaymentComponent_div_90_Template(rf, ctx) {
      if (rf & 1) {
        var _r576 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "select", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AdvancePaymentComponent_div_90_Template_select_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r576);

          var ctx_r575 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r575.viewCount = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "option", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "10");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "option", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "20");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "30");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "40");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "50");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, " \xA0\xA0\xA0 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "span", 91);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "pagination-controls", 92);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pageChange", function AdvancePaymentComponent_div_90_Template_pagination_controls_pageChange_17_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r576);

          var ctx_r577 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r577.onPageChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r567 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r567.viewCount);
      }
    }

    var AdvancePaymentComponent =
    /*#__PURE__*/
    function () {
      function AdvancePaymentComponent(financeService, chRef, spinner, breadCrumbServices, router, rsaService, purchaseService, notifierService) {
        _classCallCheck(this, AdvancePaymentComponent);

        this.financeService = financeService;
        this.chRef = chRef;
        this.spinner = spinner;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.rsaService = rsaService;
        this.purchaseService = purchaseService;
        this.notifierService = notifierService;
        this.viewCount = 10;
        this.searchText = '';
        this.payment = [];
        this.paymentLine = [];
        this.paymentAll = [];
        this.paymentLineAll = [];
        this.summary = [];
        this.noOfinvoicePaid = 0;
        this.noOfpaymentApproved = 0;
        this.noOfpaymentCreated = 0;
        this.noOfpaymentVoid = 0;
        this.selectedmenuFilter = [];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.today = new Date(); // public oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
        // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' +
        //   this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
        // public toDate = ('0' + (new Date()).getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();

        this.oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
        this.fromDate = ('0' + this.oneMonthBefore.getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear(); // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();

        this.toDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
        this.notifier = notifierService;
      }

      _createClass(AdvancePaymentComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this3 = this;

          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          localStorage.setItem('ARDF', this.fromDate);
          localStorage.setItem('ARDT', this.toDate);
          this.view = true;
          this.selectedmenu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
          this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(function (_ref) {
            var MENUACCESS = _ref.MENUACCESS;
            return MENUACCESS.MENU_CODE === 'VIEW_ADVANCE_PAYMENTS';
          });
          this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START);
          this.noOfMonths = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 31);
          this.breadCrumbServices.selectValuesChanged.subscribe(function () {
            if (_this3.router.url === '/craftsmanautomation/finance/advancePayment') {
              _this3.setFilteredData();
            }
          });
          this.getPaymentRegister();

          (function ($, _this) {
            $(document).ready(function () {
              // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              // const today = new Date();
              // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
              // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
              //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: _this.fromDate,
                minDate: _this.oneyearstartingDate,
                change: function change(e) {
                  localStorage.setItem('ARDF', e.target.value);
                  _this.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: _this.toDate,
                maxDate: new Date(),
                change: function change(e) {
                  localStorage.setItem('ARDT', e.target.value);
                  _this.toDate = e.target.value;
                }
              });
            });
          })(jQuery, this);
        }
      }, {
        key: "getPaymentRegister",
        value: function getPaymentRegister() {
          var _this4 = this;

          this.spinner.show(); // this.fromDate = localStorage.getItem('ARDF');
          // this.toDate = localStorage.getItem('ARDT');
          // const details = { email: this.email, financeType: 'ADVANCE_PAYMENT', date1: this.fromDate, date2: this.toDate };
          // this.financeService.getFinance(details).subscribe((data: any) => {

          var time_difference = Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
          var months_difference = Math.floor(time_difference / (1000 * 60 * 60 * 24) / 31);

          if (-months_difference > this.noOfMonths) {
            // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
            this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
            this.spinner.hide();
          } else {
            var query = this.purchaseService.inputJSON('ADVANCE_PAYMENT', this.fromDate, this.toDate);
            this.purchaseService.getPurchaseOrder(query).subscribe(function (data) {
              // console.log("getPaymentRegister", data);
              _this4.spinner.hide(); // this.paymentLineAll = Array.isArray(data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE) ? data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE :
              //   (data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE) ? [data.ADVPAYMENT.PAYMENTLINES.PAYMENTLINE] : [];
              // this.paymentAll = Array.isArray(data.ADVPAYMENT.PAYMENTS.PAYMENT) ?
              //   data.ADVPAYMENT.PAYMENTS.PAYMENT : (data.ADVPAYMENT.PAYMENTS.PAYMENT) ? [(data.ADVPAYMENT.PAYMENTS.PAYMENT)] : [];


              _this4.paymentLineAll = data.ADVPAYMENT.PAYMENTLINES ? data.ADVPAYMENT.PAYMENTLINES.map(function (x) {
                return x.PAYMENTLINE;
              }) : [];
              _this4.paymentAll = data.ADVPAYMENT.PAYMENTS ? data.ADVPAYMENT.PAYMENTS.map(function (x) {
                return x.PAYMENT;
              }) : []; // this.paymentAll = data.ADVPAYMENT ? data.ADVPAYMENT.PAYMENTS.map(x=>(x.PAYMENT)) :[]

              _this4.summary = data.PAYMENT_SUMMARIES ? data.REGPAYMENT.PAYMENT_SUMMARIES.map(function (x) {
                return x.PAYMENT_SUMMARY;
              }) : [];
              _this4.payment = _this4.paymentAll;
              _this4.paymentLine = _this4.paymentLineAll;

              _this4.payment.forEach(function (f, i) {
                _this4.payment[i].filteredList = _this4.paymentLineAll.filter(function (x) {
                  return x.CHECK_ID == f.CHECK_ID;
                });
              });
            }, function (err) {
              console.log(err);
            });
          }
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this5 = this;

          this.payment = this.paymentAll.filter(function (f) {
            var a = true;

            if (_this5.breadCrumbServices.select1 && _this5.breadCrumbServices.select1 !== 'All') {
              a = a && _this5.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this5.breadCrumbServices.select2 && _this5.breadCrumbServices.select2 !== 'All') {
              a = a && _this5.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this5.breadCrumbServices.select3 && _this5.breadCrumbServices.select3 !== 'All') {
              a = a && _this5.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            }

            if (_this5.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this5.searchText.toLowerCase());
            }

            return a;
          });
          this.setSummaryData(); // this.poHeader.forEach((f, i) => {
          //   this.poHeader[i].list = this.poLineAll.filter(x => x.PO_HEADER_ID == f.PO_HEADER_ID);
          // });

          this.payment.forEach(function (f, i) {
            _this5.payment[i].list = _this5.paymentLineAll.filter(function (x) {
              return x.CHECK_ID == f.CHECK_ID;
            });
          });
          this.setAsDataTable();
        }
      }, {
        key: "setSummaryData",
        value: function setSummaryData() {
          var invoicePaid = 0,
              paymentApproved = 0,
              paymentCreated = 0,
              paymentVoid = 0;
          this.summary.forEach(function (f) {
            invoicePaid = invoicePaid + Number(f.NO_OF_INV_PAID);
            paymentApproved = paymentApproved + Number(f.NO_OF_PAY_APPROVED);
            paymentCreated = paymentCreated + Number(f.NO_OF_PAY_CREATED);
            paymentVoid = paymentVoid + Number(f.NO_OF_PAY_VOID);
          }); // this.poCounts.forEach(f=>{
          //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
          // })

          this.noOfinvoicePaid = invoicePaid;
          this.noOfpaymentApproved = paymentApproved;
          this.noOfpaymentCreated = paymentCreated;
          this.noOfpaymentVoid = paymentVoid;
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();

          (function ($) {
            $("#listtest").DataTable({
              responsive: true,
              bLengthChange: false,
              retrieve: true,
              pageLength: 20
            });
          })(jQuery);
        }
      }, {
        key: "exportExcel",
        value: function exportExcel() {
          var details = {
            POLINES: this.paymentLineAll,
            title: 'Advance Payment Report'
          };
          this.financeService.getAdvancePaymentExcel(details);
        }
      }, {
        key: "getTableDetails",
        value: function getTableDetails(paymentId) {
          this.view = false;
          this.paymentLine = this.paymentLineAll.filter(function (x) {
            return x.CHECK_ID == paymentId;
          });
          this.setAsDataTable();
        }
      }, {
        key: "getBack",
        value: function getBack() {
          this.view = true;
        }
      }, {
        key: "onPageChanged",
        value: function onPageChanged(event) {
          this.page = event;
        }
      }]);

      return AdvancePaymentComponent;
    }();

    AdvancePaymentComponent.ɵfac = function AdvancePaymentComponent_Factory(t) {
      return new (t || AdvancePaymentComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_finance_service__WEBPACK_IMPORTED_MODULE_1__["FinanceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]));
    };

    AdvancePaymentComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: AdvancePaymentComponent,
      selectors: [["app-advance-payment"]],
      decls: 95,
      vars: 24,
      consts: [["id", "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["id", "container-wrapper", 1, "container-fluid"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12", "col-xs-12"], [1, "row", "float", "pt-2", "pb-2", "d-flex", "align-items-center"], [1, "col-md-2"], [1, "form-group"], [1, "control-label"], ["id", "datetimepicker1", 1, "input-group", "date"], ["id", "datepicker", "width", "276", "ngDefaultControl", "", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-2"], ["id", "datetimepicker2", 1, "input-group", "date"], ["id", "datepicker1", "width", "276", "ngDefaultControl", "", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-1"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "mt-3", 3, "click"], [1, "col-sm-4", 2, "padding-top", "13px"], [1, "searchbox-section"], ["type", "text", "placeholder", "Enter Your Search Text", "aria-describedby", "basic-addon2", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "col-sm-1", 2, "padding-top", "13px"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "btn-sm"], [1, "fas", "fa-search", 3, "click"], ["href", "javascript:void(0)", 1, "", 3, "click"], ["xmlns", "http://www.w3.org/2000/svg", "version", "1.1", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", 0, "xmlns", "svgjs", "http://svgjs.com/svgjs", "width", "28", "height", "28", "x", "0", "y", "0", "viewBox", "0 0 512 512", 0, "xml", "space", "preserve", 1, "", 2, "enable-background", "new 0 0 512 512"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5v338c0 18.2-14.7 32.9-32.9 32.9h-304.2c-18.2 0-32.9-14.7-32.9-32.9v-445c0-18.2 14.7-32.9 32.9-32.9h197.2z", "fill", "#23a566", "data-original", "#23a566", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m336.9 254.9h-161.8c-5.8 0-10.5 4.7-10.5 10.5v140.9c0 5.8 4.7 10.5 10.5 10.5h161.9c5.8 0 10.5-4.7 10.5-10.5v-140.8c-.1-5.8-4.8-10.6-10.6-10.6zm-151.3 67.6h59.9v26.7h-59.9zm80.9 0h59.9v26.7h-59.9zm59.9-21h-59.9v-25.5h59.9zm-80.9-25.5v25.5h-59.9v-25.5zm-59.9 94.3h59.9v25.5h-59.9zm80.9 25.5v-25.5h59.9v25.5z", "fill", "#ffffff", "data-original", "#ffffff", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m319.9 135.4 121.1 98.1v-92.1l-68.7-39.9z", "opacity", ".19", "fill", "#000000", "data-original", "#000000"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5h-107c-18.2 0-32.9-14.7-32.9-32.9v-107z", "fill", "#8ed1b1", "data-original", "#8ed1b1"], [1, "row", "mb-3", "pl-3", "pr-3"], [2, "font-size", "10px", "margin-top", "-30px"], [1, "row", "mb-2", 2, "overflow", "hidden"], [1, "col-md-3"], [1, "design-card-2", "dashboard1"], [1, "icons"], [1, "fas", "fa-shopping-cart", "fa-2x"], [1, "data-section"], [1, "design-card", "dashboard1"], [1, "fas", "fa-receipt", "fa-2x"], [1, "fas", "fa-long-arrow-alt-right", "readmore"], [1, "design-card-3", "dashboard1"], [1, "fas", "fa-shipping-fast", "fa-2x"], [1, "design-card-1", "dashboard1"], [1, "fas", "fa-shopping-bag", "fa-2x"], ["class", "row mb-3 pl-3 pr-3", "id", "grid-sections", 4, "ngIf"], ["class", "row mb-3 pl-3 pr-3", "id", "list-sections", 4, "ngIf"], ["class", "row mb-3 pl-3 pr-3", "id", "filter", 4, "ngIf"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12"], [1, "row"], ["id", "accordion", 1, "myaccordion", "w-100"], ["class", "card card-2 ", 4, "ngFor", "ngForOf"], [1, "card", "card-2"], [1, "card-header", 3, "id"], [1, "mb-0"], [1, "col-sm-3"], [1, "form-group", "c1"], [1, "ctrl-label"], [1, "ctrl-value"], [1, "form-group", "c4"], [1, "text-primary"], [1, "form-group", "c3"], [1, "form-group", "c5"], [1, "form-group", "c2"], [1, "form-group", "c7"], [1, "form-group", "c6"], [1, "menubutton"], [3, "headerId", "type", "communicationString"], ["data-parent", "#accordion", 1, "bodysection", "border-top", 3, "id"], [1, "card-body"], [1, "table-responsive", "p-3"], [1, "table", "align-items-center", "table-flush", 3, "id"], [1, "thead-light"], [1, "text-right"], [4, "ngFor", "ngForOf"], ["id", "list-sections", 1, "row", "mb-3", "pl-3", "pr-3"], ["id", "goback", 1, "col-sm-12", "p-3"], [1, "float-right"], [1, "btn", "btn-primary", "btn-sm"], ["id", "list-table", 1, "card", "w-100"], ["id", "listtest", 1, "table", "align-items-center", "table-flush"], [1, "col-sm-12", 2, "padding-bottom", "10px"], [1, "align-items-center", "justify-content-center", "d-flex"], [1, "form-control", 3, "ngModel", "ngModelChange"], ["value", "10"], ["value", "20"], ["value", "30"], ["value", "40"], ["value", "50"], [2, "margin-top", "10px"], ["maxSize", "5", 1, "product-pagination", 3, "pageChange"]],
      template: function AdvancePaymentComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Payment Date From");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AdvancePaymentComponent_Template_input_ngModelChange_12_listener($event) {
            return ctx.fromDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Payment Date To");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "input", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AdvancePaymentComponent_Template_input_ngModelChange_18_listener($event) {
            return ctx.toDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AdvancePaymentComponent_Template_a_click_20_listener() {
            return ctx.getPaymentRegister();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "View");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "span", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function AdvancePaymentComponent_Template_input_ngModelChange_24_listener($event) {
            return ctx.searchText = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "i", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AdvancePaymentComponent_Template_i_click_27_listener() {
            return ctx.setFilteredData();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AdvancePaymentComponent_Template_a_click_29_listener() {
            return ctx.exportExcel();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "svg", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "g");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "path", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "path", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "path", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](40, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "i", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](49, "number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "No.Of Adv. Invoice Paid");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "i", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](59, "number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "No.Of Adv.Payment Approved");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](63, "i", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](67, "i", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](71, "number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "No.Of Adv.Payment Created");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "i", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "i", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](83, "number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "No.Of Adv.Payment Void");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "small");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](87, "i", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](88, AdvancePaymentComponent_div_88_Template, 6, 7, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](89, AdvancePaymentComponent_div_89_Template, 30, 1, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](90, AdvancePaymentComponent_div_90_Template, 18, 1, "div", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "ngx-spinner", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "p", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](94, "notifier-container");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.fromDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.toDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Data available from: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](40, 13, ctx.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START, "dd-MM-yyyy"), " and Maximum range: ", ctx.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 30, " months");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](49, 16, ctx.noOfinvoicePaid));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](59, 18, ctx.noOfpaymentApproved), "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](71, 20, ctx.noOfpaymentCreated));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](83, 22, ctx.noOfpaymentVoid));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.view);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.view);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.view);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerComponent"], angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierContainerComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_10__["FileViewComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_x"], ngx_pagination__WEBPACK_IMPORTED_MODULE_11__["PaginationControlsComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["DatePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["DecimalPipe"], ngx_pagination__WEBPACK_IMPORTED_MODULE_11__["PaginatePipe"], _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_12__["NumberFormattingPipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9hZHZhbmNlLXBheW1lbnQvYWR2YW5jZS1wYXltZW50LmNvbXBvbmVudC5jc3MifQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AdvancePaymentComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-advance-payment',
          templateUrl: './advance-payment.component.html',
          styleUrls: ['./advance-payment.component.css']
        }]
      }], function () {
        return [{
          type: _service_finance_service__WEBPACK_IMPORTED_MODULE_1__["FinanceService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]
        }, {
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__["PurchaseService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/common-payment-view/common-payment-view.component.ts":
  /*!*****************************************************************************************!*\
    !*** ./src/app/components/finance/common-payment-view/common-payment-view.component.ts ***!
    \*****************************************************************************************/

  /*! exports provided: CommonPaymentViewComponent */

  /***/
  function srcAppComponentsFinanceCommonPaymentViewCommonPaymentViewComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CommonPaymentViewComponent", function () {
      return CommonPaymentViewComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _shipment_service_shipment_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../shipment/service/shipment.service */
    "./src/app/components/shipment/service/shipment.service.ts");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");

    function CommonPaymentViewComponent_div_0_div_29_tr_85_span_21_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r665 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, element1_r665.RECEIVED_QTY), " ");
      }
    }

    function CommonPaymentViewComponent_div_0_div_29_tr_85_a_22_Template(rf, ctx) {
      if (rf & 1) {
        var _r671 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommonPaymentViewComponent_div_0_div_29_tr_85_a_22_Template_a_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r671);

          var element1_r665 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r669 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r669.clickedLineId(element1_r665.LINE_LOCATION_ID);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r665 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, element1_r665.RECEIVED_QTY));
      }
    }

    function CommonPaymentViewComponent_div_0_div_29_tr_85_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](15, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](21, CommonPaymentViewComponent_div_0_div_29_tr_85_span_21_Template, 3, 3, "span", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](22, CommonPaymentViewComponent_div_0_div_29_tr_85_a_22_Template, 3, 3, "a", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](25, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "td", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r665 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r665.PO_LINE_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element1_r665.PO_SHIPMENT_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("title", element1_r665.ITEM_DESCRIPTION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r665.PO_LINE_ITEM, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r665.UOM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 13, element1_r665.ORDERED_QTY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](15, 15, element1_r665.PRICE));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r665.NEED_BY_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r665.PROMISED_DATE, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element1_r665.RECEIVED_QTY === "0");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element1_r665.RECEIVED_QTY !== "0");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](25, 17, element1_r665.BALANCE_QTY), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r665.DELAYED_DAYS, "");
      }
    }

    function CommonPaymentViewComponent_div_0_div_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Buyer Organization");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "PO Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Buyer Unit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "PO Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Buyer");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Revision No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45, "Bill to");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "PO Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](55, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "table", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "thead", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Line No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Shipment No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Item");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "UOM");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Quantity Ordered");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Price");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Need By Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Promised Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Quantity Delivered/th> ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Balance Quantity");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Delay In Days");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](85, CommonPaymentViewComponent_div_0_div_29_tr_85_Template, 28, 19, "tr", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r662 = ctx.$implicit;
        var i_r663 = ctx.index;

        var ctx_r660 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "heading", i_r663, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.BUYER_ORGANIZATION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.PO_NO);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.BUYER_UNIT);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.PO_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.BUYER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r662.PO_REVISION, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r662.PO_CURRENCY, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r662.BILL_TO_LOCATION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](55, 17, element_r662.PO_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "collapse", i_r663, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate1"]("aria-labelledby", "heading", i_r663, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMapInterpolate1"]("card-body", i_r663, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "testvi", i_r663, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r660.poHeadersList[i_r663].list);
      }
    }

    function CommonPaymentViewComponent_div_0_div_31_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "h5", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "No Purchase Order Details");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var _c0 = function _c0(a0, a1) {
      return {
        itemsPerPage: a0,
        currentPage: a1
      };
    };

    function CommonPaymentViewComponent_div_0_Template(rf, ctx) {
      if (rf & 1) {
        var _r674 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "b");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "h5");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "Purchase Order Details");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "button", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CommonPaymentViewComponent_div_0_Template_button_click_7_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r674);

          var ctx_r673 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r673.goBackFromShip();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "GO Back");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](11, "div", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "select", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CommonPaymentViewComponent_div_0_Template_select_ngModelChange_13_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r674);

          var ctx_r675 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r675.viewCount = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "option", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "10");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "option", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "20");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "option", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "30");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "option", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "40");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "option", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "50");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "pagination-controls", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pageChange", function CommonPaymentViewComponent_div_0_Template_pagination_controls_pageChange_25_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r674);

          var ctx_r676 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r676.onPageChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, CommonPaymentViewComponent_div_0_div_29_Template, 86, 19, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](30, "paginate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, CommonPaymentViewComponent_div_0_div_31_Template, 3, 0, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r659 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r659.viewCount);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](30, 3, ctx_r659.poHeadersList, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](6, _c0, ctx_r659.viewCount, ctx_r659.page)));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r659.poHeadersList.length === 0);
      }
    }

    var CommonPaymentViewComponent =
    /*#__PURE__*/
    function () {
      // public fromDate;
      // public toDate;
      function CommonPaymentViewComponent(shipmentService, chRef, breadCrumbServices, router, rsaService, spinner) {
        _classCallCheck(this, CommonPaymentViewComponent);

        this.shipmentService = shipmentService;
        this.chRef = chRef;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.rsaService = rsaService;
        this.spinner = spinner;
        this.goBackShip = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.poHeadersList = [];
        this.fullpoHeaderList = [];
        this.poLineList = [];
        this.fullpolines = [];
        this.fullSummary = [];
        this.viewShipmenList = [];
        this.searchText = '';
        this.viewCount = 10;
        this.viewGrid = true;
        this.uom = '';
      }

      _createClass(CommonPaymentViewComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          this.breadCrumbServices.selectValuesChanged.subscribe(function () {});
          this.getViewReceipts();
        }
      }, {
        key: "getViewReceipts",
        value: function getViewReceipts() {
          var _this6 = this;

          this.spinner.show();
          var details = {
            email: this.email,
            shipmentType: 'INVOICE_PO_DETAILS',
            lineId: this.shipmentId
          };
          this.shipmentService.getShipmentsById(details).subscribe(function (data) {
            _this6.spinner.hide();

            if (data) {
              _this6.fullpoHeaderList = Array.isArray(data.PODETAILS.POHEADERS.POHEADER) ? data.PODETAILS.POHEADERS.POHEADER : data.PODETAILS.POHEADERS.POHEADER ? [data.PODETAILS.POHEADERS.POHEADER] : [];
              _this6.fullpolines = Array.isArray(data.PODETAILS.POLINES.POLINE) ? data.PODETAILS.POLINES.POLINE : data.PODETAILS.POLINES.POLINE ? [data.PODETAILS.POLINES.POLINE] : []; //this.fullSummary = Array.isArray(data.PODETAILS.RECEIPTLOTS.RECEIPTLOT) ? data.PODETAILS.RECEIPTLOTS.RECEIPTLOT : (data.PODETAILS.RECEIPTLOTS.RECEIPTLOT) ? [data.PODETAILS.RECEIPTLOTS.RECEIPTLOT] : [];

              _this6.setFilteredData(); // this.setSummaryData();

            } else {
              console.error('NULL VALUE');
            }
          }, function (error) {
            _this6.spinner.hide();

            console.log(error);
          });
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this7 = this;

          this.poHeadersList = this.fullpoHeaderList.filter(function (f) {
            var a = true;

            if (_this7.breadCrumbServices.select1 && _this7.breadCrumbServices.select1 !== 'All') {
              a = a && _this7.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this7.breadCrumbServices.select2 && _this7.breadCrumbServices.select2 !== 'All') {
              a = a && _this7.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this7.breadCrumbServices.select3 && _this7.breadCrumbServices.select3 !== 'All') {
              a = a && _this7.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            } // if (this.fromDate) {
            //   a = a && this.fromDate < f.PO_DATE;
            // }
            // if (this.toDate) {
            //   a = a && this.toDate > f.PO_DATE;
            // }


            if (_this7.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this7.searchText.toLowerCase());
            }

            return a;
          }); //   this.poHeadersList = this.poHeadersList.slice(0, 20);

          this.poHeadersList.forEach(function (f, i) {
            _this7.poHeadersList[i].list = _this7.fullpolines.filter(function (x) {
              return x.SHIPMENT_HEADER_ID == f.SHIPMENT_HEADER_ID;
            });
          });
          this.setAsDataTable();
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();
          this.poHeadersList.forEach(function (f, i) {
            (function ($) {
              $('#testvi' + i).DataTable({
                responsive: true,
                bLengthChange: false,
                retrieve: true,
                pageLength: 5
              });
            })(jQuery);
          });
        }
      }, {
        key: "clickedLineId",
        value: function clickedLineId(id) {} // setAsDataTableList() {
        //   this.chRef.detectChanges();
        //   (function ($) {
        //     $('#Past-table').DataTable({
        //       responsive: true,
        //       iDisplayLength: 5,
        //       bLengthChange: false,
        //       retrieve: true,
        //       pageLength: 5,
        //     });
        //     $('#Past-table').draw();
        //   })(jQuery);
        // }

      }, {
        key: "onPageChanged",
        value: function onPageChanged(event) {
          this.page = event;
        }
      }, {
        key: "onViewGrid",
        value: function onViewGrid() {
          this.viewGrid = true;
          this.searchText = ''; //this.setAsDataTable();
        }
      }, {
        key: "onViewList",
        value: function onViewList() {
          this.viewGrid = false;
          this.searchText = ''; //this.setAsDataTableList();
        }
      }, {
        key: "exportExcel",
        value: function exportExcel() {
          var poLines = [];
          this.poHeadersList.forEach(function (f) {
            poLines.push.apply(poLines, _toConsumableArray(f.list));
          });
          var details = {
            POLINES: poLines
          };
          this.shipmentService.getExcel(details);
        }
      }, {
        key: "changeView",
        value: function changeView(shipment_id) {
          this.viewShipmenList = this.fullSummary.filter(function (f) {
            return f.SHIPMENT_LINE_ID === shipment_id;
          });
          this.viewGrid = false;
        }
      }, {
        key: "goBack",
        value: function goBack() {
          this.viewGrid = true;
        }
      }, {
        key: "goBackFromShip",
        value: function goBackFromShip() {
          this.shipmentView = false;
          this.goBackShip.emit();
        }
      }]);

      return CommonPaymentViewComponent;
    }();

    CommonPaymentViewComponent.ɵfac = function CommonPaymentViewComponent_Factory(t) {
      return new (t || CommonPaymentViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_shipment_service_shipment_service__WEBPACK_IMPORTED_MODULE_1__["ShipmentService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_2__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]));
    };

    CommonPaymentViewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: CommonPaymentViewComponent,
      selectors: [["app-common-payment-view"]],
      inputs: {
        shipmentId: "shipmentId",
        shipmentView: "shipmentView"
      },
      outputs: {
        goBackShip: "goBackShip"
      },
      decls: 1,
      vars: 1,
      consts: [["class", "row mb-3 pl-3 pr-3", "id", "grid-sections", 4, "ngIf"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], ["id", "goback", 1, "col-sm-12", "p-3"], [1, "d-inline-flex", "p-2", "text-center", 2, "margin-left", "40%"], [1, "float-right"], [1, "btn", "btn-primary", "btn-sm", 3, "click"], [1, "col-sm-12"], [1, "row"], [1, "col-sm-7"], [1, "col-sm-1", 2, "padding-top", "25px"], [1, "form-control", 3, "ngModel", "ngModelChange"], ["value", "10"], ["value", "20"], ["value", "30"], ["value", "40"], ["value", "50"], [1, "col-sm-4", 2, "padding-top", "30px"], ["maxSize", "5", 1, "product-pagination", 3, "pageChange"], ["id", "accordion", 1, "myaccordion", "w-100"], ["class", "card c1", 4, "ngFor", "ngForOf"], ["style", "align-self: center;", 4, "ngIf"], [1, "card", "c1"], [1, "card-header", 3, "id"], [1, "mb-0"], [1, "col-sm-3"], [1, "form-group", "c1"], [1, "ctrl-label"], [1, "ctrl-value"], [1, "form-group", "c4"], [1, "col-sm-2"], [1, "form-group", "c3"], [1, "form-group", "c5"], [1, "form-group", "c2"], [1, "form-group", "c8"], [1, "form-group", "c7"], [1, "form-group", "c6"], [1, "col-sm-8"], ["data-parent", "#accordion", 1, "bodysection", "border-top", 3, "id"], [1, "table-responsive", "p-3"], [1, "table", "align-items-center", "table-flush", 3, "id"], [1, "thead-light"], [4, "ngFor", "ngForOf"], ["data-toggle", "tooltip", 3, "title"], [2, "text-align", "right"], [4, "ngIf"], ["href", "javascript:void(0)", 3, "click", 4, "ngIf"], [1, "text-danger"], ["href", "javascript:void(0)", 3, "click"], [2, "align-self", "center"], [1, "text-gray", "text-center"]],
      template: function CommonPaymentViewComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, CommonPaymentViewComponent_div_0_Template, 32, 9, "div", 0);
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.viewGrid);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_6__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵangular_packages_forms_forms_x"], ngx_pagination__WEBPACK_IMPORTED_MODULE_8__["PaginationControlsComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["NgForOf"]],
      pipes: [ngx_pagination__WEBPACK_IMPORTED_MODULE_8__["PaginatePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_6__["DecimalPipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9jb21tb24tcGF5bWVudC12aWV3L2NvbW1vbi1wYXltZW50LXZpZXcuY29tcG9uZW50LmNzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CommonPaymentViewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-common-payment-view',
          templateUrl: './common-payment-view.component.html',
          styleUrls: ['./common-payment-view.component.css']
        }]
      }], function () {
        return [{
          type: _shipment_service_shipment_service__WEBPACK_IMPORTED_MODULE_1__["ShipmentService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_2__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_4__["RsaService"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]
        }];
      }, {
        shipmentId: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        shipmentView: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        goBackShip: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"],
          args: ["goBackShip"]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/create-invoice/create-invoice.component.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/components/finance/create-invoice/create-invoice.component.ts ***!
    \*******************************************************************************/

  /*! exports provided: CreateInvoiceComponent */

  /***/
  function srcAppComponentsFinanceCreateInvoiceCreateInvoiceComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreateInvoiceComponent", function () {
      return CreateInvoiceComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_config_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/app/config/config */
    "./src/app/config/config.ts");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../auth/service/login.service */
    "./src/app/components/auth/service/login.service.ts");
    /* harmony import */


    var _auth_service_email_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../../auth/service/email.service */
    "./src/app/components/auth/service/email.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

    var _c0 = ["myFileInput"];

    function CreateInvoiceComponent_th_53_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "DO Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_54_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_55_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_56_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivered Quantity");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_tr_64_td_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r622 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.DO_NUMBER);
      }
    }

    function CreateInvoiceComponent_tr_64_td_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r622 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.DELIVERY_ORDER_NO);
      }
    }

    function CreateInvoiceComponent_tr_64_td_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r622 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.DELIVERY_ORDER_DATE);
      }
    }

    function CreateInvoiceComponent_tr_64_td_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r622 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, item_r622.DELIVERED_QTY));
      }
    }

    function CreateInvoiceComponent_tr_64_Template(rf, ctx) {
      if (rf & 1) {
        var _r632 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, CreateInvoiceComponent_tr_64_td_15_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, CreateInvoiceComponent_tr_64_td_16_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, CreateInvoiceComponent_tr_64_td_17_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, CreateInvoiceComponent_tr_64_td_18_Template, 3, 3, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](21, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](24, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 80);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_tr_64_Template_a_click_26_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r632);

          var item_r622 = ctx.$implicit;

          var ctx_r631 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r631.editLine(item_r622);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "i", 81);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " \xA0 \xA0 \xA0");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 82);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_tr_64_Template_a_click_29_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r632);

          var item_r622 = ctx.$implicit;

          var ctx_r633 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r633.deleteLine(item_r622);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "i", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r622 = ctx.$implicit;

        var ctx_r608 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.DOCUMENT_NO);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", item_r622.PO_LINE_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.PO_SHIPMENT_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r622.PO_LINE_ITEM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 12, item_r622.PRICE));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 14, item_r622.ORDERED_QTY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r608.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r608.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r608.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r608.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](21, 16, item_r622.BILLED_QUANTITY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](24, 18, item_r622.QUANTITY));
      }
    }

    function CreateInvoiceComponent_th_165_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "DO Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_166_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_167_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_168_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivered Quantity");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_tr_174_td_15_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r634 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.DO_NUMBER);
      }
    }

    function CreateInvoiceComponent_tr_174_td_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r634 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.DELIVERY_ORDER_NO);
      }
    }

    function CreateInvoiceComponent_tr_174_td_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r634 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.DELIVERY_ORDER_DATE);
      }
    }

    function CreateInvoiceComponent_tr_174_td_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r634 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, item_r634.DELIVERED_QTY));
      }
    }

    function CreateInvoiceComponent_tr_174_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, CreateInvoiceComponent_tr_174_td_15_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, CreateInvoiceComponent_tr_174_td_16_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, CreateInvoiceComponent_tr_174_td_17_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, CreateInvoiceComponent_tr_174_td_18_Template, 3, 3, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](21, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](24, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r634 = ctx.$implicit;

        var ctx_r615 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.DOCUMENT_NO);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", item_r634.PO_LINE_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.PO_SHIPMENT_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r634.PO_LINE_ITEM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 12, item_r634.PRICE));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 14, item_r634.ORDERED_QTY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r615.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r615.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r615.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r615.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](21, 16, item_r634.BILLED_QUANTITY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](24, 18, item_r634.QUANTITY));
      }
    }

    function CreateInvoiceComponent_option_203_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r643 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", item_r643);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r643);
      }
    }

    function CreateInvoiceComponent_th_221_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "DO Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_222_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_223_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivery Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_th_224_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Delivered Quantity");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function CreateInvoiceComponent_tr_230_td_16_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r644 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r644.DO_NUMBER);
      }
    }

    function CreateInvoiceComponent_tr_230_td_17_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r644 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r644.DELIVERY_ORDER_NO);
      }
    }

    function CreateInvoiceComponent_tr_230_td_18_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r644 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r644.DELIVERY_ORDER_DATE);
      }
    }

    function CreateInvoiceComponent_tr_230_td_19_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](2, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r644 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](2, 1, item_r644.DELIVERED_QTY));
      }
    }

    function CreateInvoiceComponent_tr_230_Template(rf, ctx) {
      if (rf & 1) {
        var _r655 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "input", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_tr_230_Template_input_ngModelChange_3_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r655);

          var item_r644 = ctx.$implicit;
          return item_r644.selected = $event;
        })("change", function CreateInvoiceComponent_tr_230_Template_input_change_3_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r655);

          var item_r644 = ctx.$implicit;
          var i_r645 = ctx.index;

          var ctx_r656 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r656.checkValue(item_r644.selected, i_r645);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](15, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](16, CreateInvoiceComponent_tr_230_td_16_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, CreateInvoiceComponent_tr_230_td_17_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](18, CreateInvoiceComponent_tr_230_td_18_Template, 2, 1, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, CreateInvoiceComponent_tr_230_td_19_Template, 3, 3, "td", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](22, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_tr_230_Template_input_ngModelChange_24_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r655);

          var item_r644 = ctx.$implicit;
          return item_r644.QUANTITY = $event;
        })("ngModelChange", function CreateInvoiceComponent_tr_230_Template_input_ngModelChange_24_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r655);

          var i_r645 = ctx.index;

          var ctx_r658 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r658.checkQuantityAndUpdate(i_r645);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r644 = ctx.$implicit;

        var ctx_r621 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", item_r644.selected);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", item_r644.PO_LINE_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", item_r644.PO_SHIPMENT_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", item_r644.PO_LINE_ITEM, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 13, item_r644.PRICE), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](15, 15, item_r644.ORDERED_QTY));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r621.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r621.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r621.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r621.invoiceMode === "PO-GR-INV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](22, 17, item_r644.BILLED_QUANTITY), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !item_r644.selected)("ngModel", item_r644.QUANTITY);
      }
    }

    var CreateInvoiceComponent =
    /*#__PURE__*/
    function () {
      function CreateInvoiceComponent(purchaseService, chRef, spinner, breadCrumbServices, router, rsaService, notifierService, loginService, emailService) {
        _classCallCheck(this, CreateInvoiceComponent);

        this.purchaseService = purchaseService;
        this.chRef = chRef;
        this.spinner = spinner;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.rsaService = rsaService;
        this.notifierService = notifierService;
        this.loginService = loginService;
        this.emailService = emailService;
        this.purchasedList = [];
        this.email = '';
        this.poHeaderAll = []; //public poLineAll = [];

        this.poHeader = [];
        this.fileData = null;
        this.selectedPO = '';
        this.currPOlines = [];
        this.invoiceLines = [];
        this.beforeUpdateInvLine = [];
        this.uniquePOs = [];
        this.invoiceMode = '';
        this.edit = false;
        this.notifier = notifierService;
      }

      _createClass(CreateInvoiceComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.createInv = new Invoice(); // this.createInv.CURRENCY = "MYR";

          (function ($, thi) {
            // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            // const today = new Date();
            // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
            // $('#datepicker1').datepicker({
            //   uiLibrary: 'bootstrap4', format: 'dd-mmm-yyyy',
            //   value: cur.NEED_BY_DATE,
            //   change(e) {
            //     localStorage.setItem('ADDO', e.target.value);
            //     cur.NEED_BY_DATE = e.target.value;
            //   }
            // });
            $('.nav-tabs > li a[title]').tooltip(); //Wizard

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
              var target = $(e.target);

              if (target.parent().hasClass('disabled')) {
                return false;
              }
            });
            $(".next-step").click(function (e) {
              var active = $('.wizard .nav-tabs li.active');
              active.next().removeClass('disabled');
              nextTab(active);
            });
            $(".prev-step").click(function (e) {
              var active = $('.wizard .nav-tabs li.active');
              prevTab(active);
            });

            function nextTab(elem) {
              $('#datepicker2').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
                value: thi.createInv.INVOICE_DATE,
                change: function change(e) {
                  thi.createInv.INVOICE_DATE = e.target.value;
                }
              });
              $(elem).next().find('a[data-toggle="tab"]').click();
            }

            function prevTab(elem) {
              $(elem).prev().find('a[data-toggle="tab"]').click();
            }

            $('.nav-tabs').on('click', 'li', function () {
              $('.nav-tabs li.active').removeClass('active');
              $(this).addClass('active');
            });
            $('#datepicker2').datepicker({
              uiLibrary: 'bootstrap4',
              format: 'dd-mmm-yyyy',
              // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
              value: thi.createInv.INVOICE_DATE,
              change: function change(e) {
                thi.createInv.INVOICE_DATE = e.target.value;
              }
            });
          })(jQuery, this); // (function ($, thi) {
          // })(jQuery, this);


          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          this.getPendingPurchase();
        }
      }, {
        key: "AddPOClicked",
        value: function AddPOClicked() {}
      }, {
        key: "onPOModelClose",
        value: function onPOModelClose() {}
      }, {
        key: "setPo",
        value: function setPo(value) {
          var a = this.uniquePOs.filter(function (f) {
            return f === value;
          });

          if (a.length === 1) {
            this.selectedPO = value;
            this.setLinesList();
          }
        }
      }, {
        key: "getPendingPurchase",
        value: function getPendingPurchase() {
          var _this8 = this;

          this.spinner.show(); // const details = { email: this.email, purchaseType: 'PENDING_PO_LINE_FOR_INVOICE' };

          var details = this.purchaseService.inputJSON('PENDING_PO_LINE_FOR_INVOICE', undefined, undefined);
          this.purchaseService.getPurchaseOrder(details).subscribe(function (data) {
            // console.log('ppp', data);
            _this8.spinner.hide(); ///     this.getAllFreightTerms();


            _this8.poHeaderAll = Array.isArray(data.PENDING_INV_LINES.PENDING_INV_LINE) ? data.PENDING_INV_LINES.PENDING_INV_LINE : data.PENDING_INV_LINES.PENDING_INV_LINE ? [data.PENDING_INV_LINES.PENDING_INV_LINE] : []; // this.poLineAll = Array.isArray(data.PENDINGPO.POLINES.POLINE) ? data.PENDINGPO.POLINES.POLINE : (data.PENDINGPO.POLINES.POLINE) ? [(data.PENDINGPO.POLINES.POLINE)] : [];

            _this8.poHeader = _this8.poHeaderAll; // this.poLine = this.poLineAll;
            // this.poHeader.forEach(
            //   (x, i) => {
            //     this.poHeader[i].filteredList = this.poLine.filter(y => y.PO_HEADER_ID === x.PO_HEADER_ID);
            //     this.poHeader[i].filteredList = this.poHeader[i].filteredList.map(y => {
            //       y.BUYER_EMAIL =   this.poHeader[i].BUYER_EMAIL
            //     return y;
            //     });
            //   }
            // );

            _this8.uniquePOs = Array.from(new Set(_this8.poHeader.map(function (m) {
              return m.PO_NUMBER;
            }))); //console.log(this.uniquePOs.length);
          }, function (err) {
            _this8.spinner.hide();

            console.log(err);
          });
        }
      }, {
        key: "editLine",
        value: function editLine(item) {
          this.selectedPO = item.DOCUMENT_NO;
          this.edit = true;
          this.beforeUpdateInvLine = JSON.parse(JSON.stringify(this.invoiceLines));
          this.setLinesList();
        }
      }, {
        key: "setLinesList",
        value: function setLinesList() {
          var _this9 = this;

          var line = this.poHeaderAll.filter(function (f) {
            return f.PO_NUMBER === _this9.selectedPO;
          });
          this.currPOlines = line ? line : [];

          if (line.length > 0) {
            this.invoiceMode = line[0].INVOICE_MODE;
          }
        }
      }, {
        key: "deleteLine",
        value: function deleteLine(item) {
          // console.log('Delete Called',item)
          this.invoiceLines = this.invoiceLines.filter(function (f) {
            return f.LINE_LOCATION_ID !== item.LINE_LOCATION_ID;
          });
        }
      }, {
        key: "checkValue",
        value: function checkValue(value, index) {
          //console.log(value,index);
          if (!value) {
            this.currPOlines[index].QUANTITY = undefined;
          } else {
            this.currPOlines[index].QUANTITY = Number(this.currPOlines[index].DELIVERED_QTY) - Number(this.currPOlines[index].BILLED_QUANTITY);
          }
        }
      }, {
        key: "checkQuantityAndUpdate",
        value: function checkQuantityAndUpdate(i) {
          if (Number(this.currPOlines[i].QUANTITY) > Number(this.currPOlines[i].DELIVERED_QTY) - Number(this.currPOlines[i].BILLED_QUANTITY)) {
            this.notifier.notify('error', 'Invoice Qty Exceeds Delivered Qty');
            this.currPOlines[i].QUANTITY = 0;
          }
        }
      }, {
        key: "AddButtonClicked",
        value: function AddButtonClicked() {
          if (!this.edit) {
            var _this$invoiceLines;

            var linesSelected = [];
            var selectedLineIds = [];
            linesSelected = this.currPOlines.filter(function (f) {
              return f.selected && Number(f.QUANTITY) > 0;
            });
            selectedLineIds = linesSelected.map(function (m) {
              return m.LINE_LOCATION_ID;
            }); //console.log('LineSelected Length',linesSelected.length);

            (_this$invoiceLines = this.invoiceLines).push.apply(_this$invoiceLines, _toConsumableArray(linesSelected));

            if (this.invoiceLines, length > 0) {
              this.createInv.CURRENCY = this.invoiceLines[0].PO_CURRENCY;
              this.invoiceMode = this.invoiceLines[0].INVOICE_MODE;
            } //console.log('LEN',this.asnLines.length,'LEN!',this.asnLots.length);

          } else {
            // this.edit= false;
            var _selectedLineIds = [];
            this.invoiceLines = this.invoiceLines.filter(function (f) {
              return f.selected && Number(f.QUANTITY) > 0;
            });
            _selectedLineIds = this.invoiceLines.map(function (m) {
              return m.LINE_LOCATION_ID;
            });

            if (this.invoiceLines, length > 0) {
              this.createInv.CURRENCY = this.invoiceLines[0].PO_CURRENCY;
              this.invoiceMode = this.invoiceLines[0].INVOICE_MODE;
            } // console.log('LEN',this.asnLines.length,'LEN!',this.asnLots.length);

          }
        }
      }, {
        key: "onModelClose",
        value: function onModelClose() {
          //  console.log('Called');
          this.invoiceLines = this.beforeUpdateInvLine;
        }
      }, {
        key: "AddLineCalled",
        value: function AddLineCalled() {
          var _this10 = this;

          this.selectedPO = '';
          this.currPOlines = [];
          this.edit = false;
          this.beforeUpdateInvLine = JSON.parse(JSON.stringify(this.invoiceLines));
          var invLines = [];

          if (this.invoiceLines.length > 0) {
            invLines = this.poHeaderAll.filter(function (f) {
              return f.INVOICE_MODE === _this10.invoiceLines[0].INVOICE_MODE && f.BUYER_UNIT_ID === _this10.invoiceLines[0].BUYER_UNIT_ID && f.VENDOR_SITE_ID === _this10.invoiceLines[0].VENDOR_SITE_ID && f.PO_CURRENCY === _this10.invoiceLines[0].PO_CURRENCY;
            });
            this.uniquePOs = Array.from(new Set(invLines.map(function (m) {
              return m.PO_NUMBER;
            }))); //console.log(this.uniquePOs.length);
          }
        }
      }, {
        key: "onFilesAdded",
        value: function onFilesAdded(fileInput) {
          var _this11 = this;

          // this.spinner.show();
          this.fileData = fileInput.target.files; //const length = fileInput.target.files.length;
          // this.length = fileInput.target.files.length;

          var file = new FormData();

          for (var i = 0; i < 1; i++) {
            file.append('file', this.fileData[0], this.fileData[0].name);
          }

          var reader = new FileReader();
          reader.readAsDataURL(this.fileData[0]);

          reader.onload = function () {
            _this11.file64 = reader.result;
          }; // this.createInv.FILE_NAME = this.fileData[0].name;


          this.loginService.FileUpload(file).subscribe(function (data) {
            _this11.spinner.hide();

            var i;
            _this11.createInv.FILE_NAME = _this11.fileData[0].name;
          }, function (err) {
            _this11.spinner.hide();

            _this11.resetFile();

            if (err.error.message) {} else {}
          });
        }
      }, {
        key: "resetFile",
        value: function resetFile() {
          this.myInputVariable.nativeElement.value = '';
        }
      }, {
        key: "cancel",
        value: function cancel() {
          this.resetFile();
        }
      }, {
        key: "saveInvoice",
        value: function saveInvoice() {
          var _this12 = this;

          //console.log(this.asnHeaders,this.asnLines,this.asnLots,this.email);
          var emailList = [];
          var uniqueEmailList = []; // emailList = this.asnLines.map(m=>m.BUYER_EMAIL);
          // uniqueEmailList =Array.from(new Set(emailList));
          //console.log(uniqueEmailList);

          var query = {
            INVOICE: {
              HEADER: Object.assign(Object.assign({}, this.createInv), {
                USER_NAME: this.email
              }),
              LINES: {
                LINE: _toConsumableArray(this.invoiceLines.map(function (m, i) {
                  return {
                    LINE_NUMBER: '' + (i + 1),
                    PO_HEADER_ID: m.PO_HEADER_ID,
                    PO_LINE_ID: m.PO_LINE_ID,
                    LINE_LOCATION_ID: m.LINE_LOCATION_ID,
                    RCV_TRANSACTION_ID: m.RCV_TRANSACTION_ID,
                    ACTUAL_DELIVERY_DATE: m.ACTUAL_DELIVERY_DATE,
                    QUANTITY: m.QUANTITY,
                    LINE_PRICE: m.PRICE
                  };
                }))
              },
              FILE_NAME: this.createInv.FILE_NAME
            }
          };
          this.spinner.show();
          this.purchaseService.createInvoice(query).subscribe(function (data) {
            // console.log(data);
            _this12.spinner.hide();

            _this12.reset();

            _this12.notifier.notify('success', 'Submitted Sucessfully');

            setTimeout(function () {
              _this12.redirectTo(_this12.router.url);
            }, 2000); // this.asn_id = data;
            //console.log('ASN_ID',this.asn_id);
            //       this.makePdf(emailList,`ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}`,`
            //       Dear Team,
            // ASN ${this.asn_id} has been generated successfully generated by Supplier ${this.supplierName}.
            // Please find attached ASN document for further details.
            // Regards,
            // ${this.supplierName}
            //       `,data);
          }, function (error) {
            console.log(error);
          });
          console.log(query);
        }
      }, {
        key: "reset",
        value: function reset() {
          this.selectedPO = '';
          this.poHeaderAll = [];
          this.poHeader = [];
          this.uniquePOs = [];
          this.createInv = new Invoice();
          this.invoiceLines = [];

          (function ($, thi) {
            $('#datepicker2').datepicker({
              uiLibrary: 'bootstrap4',
              format: 'dd-mmm-yyyy',
              // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
              value: thi.createInv.INVOICE_DATE,
              change: function change(e) {
                thi.createInv.INVOICE_DATE = e.target.value;
              }
            });
          })(jQuery, this);

          this.getPendingPurchase();
        }
      }, {
        key: "redirectTo",
        value: function redirectTo(uri) {
          var _this13 = this;

          this.router.navigateByUrl('/', {
            skipLocationChange: true
          }).then(function () {
            return _this13.router.navigateByUrl(uri, {
              skipLocationChange: src_app_config_config__WEBPACK_IMPORTED_MODULE_1__["profileData"].hideURL
            });
          });
        }
      }]);

      return CreateInvoiceComponent;
    }();

    CreateInvoiceComponent.ɵfac = function CreateInvoiceComponent_Factory(t) {
      return new (t || CreateInvoiceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service_email_service__WEBPACK_IMPORTED_MODULE_9__["EmailService"]));
    };

    CreateInvoiceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: CreateInvoiceComponent,
      selectors: [["app-create-invoice"]],
      viewQuery: function CreateInvoiceComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.myInputVariable = _t.first);
        }
      },
      decls: 241,
      vars: 33,
      consts: [["id", "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["id", "container-wrapper", 1, "container-fluid"], [1, "row", "d-flex", "justify-content-center"], [1, "col-md-12"], [1, "wizard", "card"], [1, "wizard-inner", "card-body"], [1, "connecting-line"], ["role", "tablist", 1, "nav", "nav-tabs"], ["role", "presentation", 1, "active"], ["href", "#step1", "data-toggle", "tab", "aria-controls", "step1", "role", "tab", "aria-expanded", "true"], [1, "round-tab"], ["role", "presentation", 1, "disabled"], ["href", "#step2", "data-toggle", "tab", "aria-controls", "step2", "role", "tab", "aria-expanded", "false"], ["href", "#step3", "data-toggle", "tab", "aria-controls", "step3", "role", "tab"], ["role", "form", "action", "index.html", 1, "login-box"], ["id", "main_form", 1, "tab-content"], ["role", "tabpanel", "id", "step1", 1, "tab-pane", "active"], [1, "row", "mt-3"], [1, "pl-3", "pr-3", "w-100"], [1, "text-primary"], ["data-toggle", "modal", "data-target", "#myModal1", "data-backdrop", "static", "type", "submit", "value", "Add Location", 1, "float-right", "btn", "btn-primary", "btn-sm", 3, "click"], [1, "table-responsive", "p-3"], ["id", "listtest", 1, "table", "table-bordered"], [1, "thead-light"], [4, "ngIf"], [4, "ngFor", "ngForOf"], [1, "form-group", "row"], [1, "list-inline", "pull-right"], ["type", "button", 1, "default-btn", "next-step", 3, "disabled"], ["role", "tabpanel", "id", "step2", 1, "tab-pane"], [1, "card", 2, "padding", "10px"], ["invoiceForm", "ngForm"], [1, "row", "m-2"], [1, "col-sm-4"], [1, "form-group"], ["type", "text", "name", "invno", "id", "exampleInputFirstName", "placeholder", "Invoice Number ", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["id", "datetimepicker1", 1, "input-group", "date"], ["id", "datepicker2", "name", "date", "width", "276", "required", "", 3, "ngModel", "ngModelChange"], ["type", "text", "name", "currency", "id", "exampleInputFirstName", "placeholder", "Currency ", "readonly", "", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "number", "name", "amount", "id", "exampleInputFirstName", "placeholder", "Enter Amount ", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "exampleInputFirstName", "placeholder", " ", "name", "description", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "exampleInputFirstName", "placeholder", " ", "name", "ADDITIONAL_NOTES", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "file", "name", "file", "accept", "image/*,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document", 1, "files", 3, "change"], ["myFileInput", ""], ["type", "button", 1, "default-btn", "prev-step"], ["role", "tabpanel", "id", "step3", 1, "tab-pane"], [1, "row"], [1, "card-headers", "m-2"], [1, "invoice-status"], [1, "invoice-Date", "mb-2"], [1, "font-1", "color-1", "min-w-50", "mb-2"], ["id", "Sp_CustPoRefNo", 1, "font-2", "color-2", "capitalize"], [1, "font-1", "color-1", "min-w-50"], ["id", "Sp_OrderDt", 1, "font-2", "color-2", "font-weight"], [1, "invoice-status", "min-w-50"], ["id", "Sp_ShipTo", 1, "font-2", "color-2"], ["id", "Sp_BillTo", 1, "font-2", "color-2"], [1, "col-sm-12"], [1, "checkbox"], ["type", "button", "name", "submmit", 1, "default-btn", "next-step", "submitBut", 3, "disabled", "click"], [1, "clearfix"], ["id", "myModal1", 1, "modal", "fade"], [1, "modal-dialog", "modal-xl"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title", "text-primary"], ["type", "button", "data-dismiss", "modal", 1, "close", 3, "click"], [1, "modal-body", "reg-form"], ["list", "selectPO", "id", "documentNo", 1, "form-control", "js-example-basic-single", 3, "value", "disabled", "keyup"], ["id", "selectPO"], [3, "value", 4, "ngFor", "ngForOf"], [1, "table-responsive"], [1, "table", "table-bordered"], [1, "modal-footer"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-secondary", 3, "click"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-primary", 3, "click"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["href", "javascript:void(0)", "data-toggle", "modal", "data-target", "#myModal1", "data-backdrop", "static", 3, "click"], [1, "fas", "fa-pen", "text-grey"], ["href", "javascript:void(0)", 3, "click"], [1, "fas", "fa-trash", "text-red"], [3, "value"], [1, ""], ["type", "checkbox", "id", "ibaccount", 1, "form-controls", 3, "ngModel", "ngModelChange", "change"], ["type", "text", 1, "form-control", 3, "disabled", "ngModel", "ngModelChange"]],
      template: function CreateInvoiceComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "ul", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "li", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "a", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "span", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "1 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Select Purchase Orders");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "li", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "span", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "2");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Enter Invoice Details");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "li", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "span", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25, "3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "i");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](27, "Review Invoice Details");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "form", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h5", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Added Purchase Lines ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_Template_button_click_35_listener() {
            return ctx.AddLineCalled();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36, "Add Purchase Order");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "table", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "thead", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Purchase Order No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Line No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Shipment No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "Item");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50, "Price");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](52, "PO Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](53, CreateInvoiceComponent_th_53_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](54, CreateInvoiceComponent_th_54_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](55, CreateInvoiceComponent_th_55_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](56, CreateInvoiceComponent_th_56_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Quantity Billed");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Invoice Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Action");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](64, CreateInvoiceComponent_tr_64_Template, 31, 20, "tr", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "ul", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "button", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Continue ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "form", null, 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Invoice Number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "input", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_79_listener($event) {
            return ctx.createInv.INVOICE_NUMBER = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Invoice Date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "input", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_85_listener($event) {
            return ctx.createInv.INVOICE_DATE = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "Currency");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "input", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_90_listener($event) {
            return ctx.createInv.CURRENCY = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](91, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](94, "Invoice Amount");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "input", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_95_listener($event) {
            return ctx.createInv.INVOICE_AMOUNT = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](99, "Description");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "input", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_100_listener($event) {
            return ctx.createInv.DESCRIPTION = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Additional Notes");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "input", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function CreateInvoiceComponent_Template_input_ngModelChange_105_listener($event) {
            return ctx.createInv.ADDITIONAL_NOTES = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](106, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](108, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](109, "File");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "input", 44, 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function CreateInvoiceComponent_Template_input_change_110_listener($event) {
            return ctx.onFilesAdded($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](112, "ul", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](114, "button", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](115, "Back");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](116, " \xA0 \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](117, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](118, "button", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](119, "Continue");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](120, "div", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](121, "div", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](122, "div", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](123, "div", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](124, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](125, "span", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](126, "Invoice No :");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](127, "span", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](128);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](129, "span", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](130, "Invoice Date :");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](131, "span", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](132);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](133, "div", 56);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](134, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](135, "span", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](136, "Invoice Amount :");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](137, "span", 57);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](138);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](139, "span", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](140, "Currency :");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](141, "span", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](142);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](143, "div", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](144, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](145, "span", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](146, "Description :");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](147, "span", 58);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](148);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](149, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](150, "table", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](151, "thead", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](152, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](153, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](154, "Purchase Order No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](155, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](156, "Line No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](157, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](158, "Shipment No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](159, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](160, "Item");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](161, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](162, "Price");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](163, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](164, "PO Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](165, CreateInvoiceComponent_th_165_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](166, CreateInvoiceComponent_th_166_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](167, CreateInvoiceComponent_th_167_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](168, CreateInvoiceComponent_th_168_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](169, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](170, "Quantity Billed");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](171, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](172, "Invoice Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](173, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](174, CreateInvoiceComponent_tr_174_Template, 25, 20, "tr", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](175, "div", 59);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](176, "div", 60);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](177, "ul", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](178, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](179, "button", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](180, "Back");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](181, " \xA0 \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](182, "li");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](183, "button", 61);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_Template_button_click_183_listener() {
            return ctx.saveInvoice();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](184, "Submit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](185, "div", 62);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](186, "div", 63);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](187, "div", 64);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](188, "div", 65);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](189, "div", 66);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](190, "h4", 67);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](191, "Invoice Line Details");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](192, "button", 68);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_Template_button_click_192_listener() {
            return ctx.onModelClose();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](193, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](194, "div", 69);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](195, "div", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](196, "div", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](197, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](198, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](199, "Purchase Order No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](200, "input", 70);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function CreateInvoiceComponent_Template_input_keyup_200_listener($event) {
            return ctx.setPo($event.target.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](201, "datalist", 71);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](202, "select");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](203, CreateInvoiceComponent_option_203_Template, 2, 2, "option", 72);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](204, "div", 59);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](205, "div", 73);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](206, "table", 74);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](207, "thead", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](208, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](209, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](210, "Select");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](211, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](212, "Line No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](213, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](214, "Shipment No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](215, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](216, "Item");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](217, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](218, "Price");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](219, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](220, "PO Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](221, CreateInvoiceComponent_th_221_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](222, CreateInvoiceComponent_th_222_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](223, CreateInvoiceComponent_th_223_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](224, CreateInvoiceComponent_th_224_Template, 2, 0, "th", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](225, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](226, "Quantity Billled");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](227, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](228, "Invoice Quantity");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](229, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](230, CreateInvoiceComponent_tr_230_Template, 25, 19, "tr", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](231, "div", 75);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](232, "button", 76);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_Template_button_click_232_listener() {
            return ctx.onModelClose();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](233, "Close");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](234, " \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](235, "button", 77);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function CreateInvoiceComponent_Template_button_click_235_listener() {
            return ctx.AddButtonClicked();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](236, "Add");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](237, "ngx-spinner", 78);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](238, "p", 79);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](239, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](240, "notifier-container");
        }

        if (rf & 2) {
          var _r609 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](73);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.invoiceLines);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !(ctx.invoiceLines.length > 0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.INVOICE_NUMBER);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.INVOICE_DATE);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.CURRENCY);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.INVOICE_AMOUNT);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.DESCRIPTION);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.createInv.ADDITIONAL_NOTES);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !_r609.valid || !(ctx.createInv.FILE_NAME.length > 0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.createInv.INVOICE_NUMBER);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.createInv.INVOICE_DATE);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx.createInv.INVOICE_AMOUNT, " ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.createInv.CURRENCY);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.createInv.DESCRIPTION);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.invoiceLines);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !(ctx.invoiceLines.length > 0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.selectedPO)("disabled", ctx.edit);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.uniquePOs);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.invoiceMode === "PO-GR-INV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.currPOlines);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgForm"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_11__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NumberValueAccessor"], ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerComponent"], angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierContainerComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["CheckboxControlValueAccessor"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_11__["DecimalPipe"]],
      styles: [".wizard.card[_ngcontent-%COMP%] {\r\n    padding: 30px;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]    > div.wizard-inner[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    text-align: center;\r\n}\r\n\r\n.card[_ngcontent-%COMP%]   .card-body[_ngcontent-%COMP%] {\r\n    padding: 1.875rem 1.875rem;\r\n}\r\n\r\n.card-body[_ngcontent-%COMP%] {\r\n    flex: 1 1 auto;\r\n    min-height: 1px;\r\n}\r\n\r\n.tab-content[_ngcontent-%COMP%] {\r\n    border: 1px solid #f1f6f8;\r\n    border-top: 0;\r\n    padding: 2rem 1rem;\r\n    text-align: justify;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .tab-pane[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    padding-top: 20px;\r\n}\r\n\r\n.tab-content[_ngcontent-%COMP%]    > .active[_ngcontent-%COMP%] {\r\n    display: block;\r\n}\r\n\r\n.tab-content[_ngcontent-%COMP%]    > .tab-pane[_ngcontent-%COMP%] {\r\n    display: none;\r\n}\r\n\r\n.form-group[_ngcontent-%COMP%] {\r\n    margin-bottom: 1.5rem;\r\n}\r\n\r\n.form-group[_ngcontent-%COMP%] {\r\n    margin-bottom: 1rem;\r\n}\r\n\r\n.row[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    margin-right: -15px;\r\n    margin-left: -15px;\r\n}\r\n\r\n.connecting-line[_ngcontent-%COMP%] {\r\n    height: 2px;\r\n    background: #e0e0e0;\r\n    position: absolute;\r\n    width: 63%;\r\n    margin: 0 auto;\r\n    left: 25px;\r\n    right: 0;\r\n    top: 45px;\r\n    z-index: 1;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    margin-bottom: 0;\r\n    border-bottom-color: transparent;\r\n}\r\n\r\n.nav-tabs[_ngcontent-%COMP%] {\r\n    border-bottom: 1px solid #ebedf2;\r\n}\r\n\r\n.nav[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    padding-left: 0;\r\n    margin-bottom: 0;\r\n    list-style: none;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%] {\r\n    width: 33%;\r\n}\r\n\r\n.pull-right[_ngcontent-%COMP%] {\r\n    float: right;\r\n}\r\n\r\n.list-inline[_ngcontent-%COMP%] {\r\n    padding-left: 0;\r\n    list-style: none;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li.active[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%], .wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li.active[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:hover, .wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li.active[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]:focus {\r\n    color: #555555;\r\n    cursor: default;\r\n    border: 0;\r\n    border-bottom-color: transparent;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]:hover {\r\n    background: transparent;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\r\n    width: 30px;\r\n    height: 30px;\r\n    margin: 20px auto;\r\n    border-radius: 100%;\r\n    padding: 0;\r\n    background-color: transparent;\r\n    position: relative;\r\n    top: 0;\r\n}\r\n\r\na[_ngcontent-%COMP%]:hover {\r\n    text-decoration: none;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   li.active[_ngcontent-%COMP%]   span.round-tab[_ngcontent-%COMP%] {\r\n    background: #0db02b;\r\n    color: #fff;\r\n    border-color: #0db02b;\r\n}\r\n\r\nspan.round-tab[_ngcontent-%COMP%] {\r\n    width: 30px;\r\n    height: 30px;\r\n    line-height: 30px;\r\n    display: inline-block;\r\n    border-radius: 50%;\r\n    background: #fff;\r\n    z-index: 2;\r\n    position: absolute;\r\n    left: 0;\r\n    text-align: center;\r\n    font-size: 16px;\r\n    color: #0e214b;\r\n    font-weight: 500;\r\n    border: 1px solid #ddd;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li.active[_ngcontent-%COMP%]    > a[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\r\n    color: #0db02b;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .nav-tabs[_ngcontent-%COMP%]    > li[_ngcontent-%COMP%]   a[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\r\n    position: absolute;\r\n    top: -15px;\r\n    font-style: normal;\r\n    font-weight: 400;\r\n    white-space: nowrap;\r\n    left: 50%;\r\n    transform: translate(-50%, -50%);\r\n    font-size: 12px;\r\n    font-weight: 700;\r\n    color: #000;\r\n}\r\n\r\n.form-group[_ngcontent-%COMP%]   label[_ngcontent-%COMP%] {\r\n    font-size: 1rem;\r\n    line-height: 1.4rem;\r\n    vertical-align: top;\r\n    margin-bottom: .5rem;\r\n}\r\n\r\nlabel[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n}\r\n\r\nform[_ngcontent-%COMP%] {\r\n    display: block;\r\n    margin-top: 0em;\r\n}\r\n\r\n.wizard[_ngcontent-%COMP%]   .tab-pane[_ngcontent-%COMP%] {\r\n    position: relative;\r\n    padding-top: 20px;\r\n}\r\n\r\n.tab-content[_ngcontent-%COMP%]    > .active[_ngcontent-%COMP%] {\r\n    display: block;\r\n}\r\n\r\nbutton[_ngcontent-%COMP%]:not(:disabled), [type=\"button\"][_ngcontent-%COMP%]:not(:disabled), [type=\"reset\"][_ngcontent-%COMP%]:not(:disabled), [type=\"submit\"][_ngcontent-%COMP%]:not(:disabled) {\r\n    cursor: pointer;\r\n}\r\n\r\n.next-step[_ngcontent-%COMP%] {\r\n    background-color: #005baa;\r\n    color: #ffff;\r\n}\r\n\r\n.prev-step[_ngcontent-%COMP%], .next-step[_ngcontent-%COMP%] {\r\n    font-size: 13px;\r\n    padding: 8px 24px;\r\n    border: none;\r\n    border-radius: 4px;\r\n    margin-top: 30px;\r\n}\r\n\r\n.card-headers[_ngcontent-%COMP%] {\r\n    display: flex;\r\n    align-items: center;\r\n    justify-content: space-between;\r\n    width: 100%;\r\n    background: #eaecf4;\r\n}\r\n\r\n.pull-right[_ngcontent-%COMP%] {\r\n    float: right;\r\n}\r\n\r\n.list-inline[_ngcontent-%COMP%] {\r\n    padding-left: 0;\r\n    list-style: none;\r\n}\r\n\r\n*[_ngcontent-%COMP%], *[_ngcontent-%COMP%]::before, *[_ngcontent-%COMP%]::after {\r\n    box-sizing: border-box;\r\n}\r\n\r\n.list-inline[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\r\n    display: inline-block;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9maW5hbmNlL2NyZWF0ZS1pbnZvaWNlL2NyZWF0ZS1pbnZvaWNlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0lBQ0ksYUFBYTtBQUNqQjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixrQkFBa0I7QUFDdEI7O0FBR0E7SUFDSSwwQkFBMEI7QUFDOUI7O0FBRUE7SUFDSSxjQUFjO0lBQ2QsZUFBZTtBQUNuQjs7QUFDQTtJQUNJLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixpQkFBaUI7QUFDckI7O0FBQ0E7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksYUFBYTtBQUNqQjs7QUFHQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFDQTtJQUNJLG1CQUFtQjtBQUN2Qjs7QUFDQTtJQUNJLGFBQWE7SUFDYixlQUFlO0lBQ2YsbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxtQkFBbUI7SUFDbkIsa0JBQWtCO0lBQ2xCLFVBQVU7SUFDVixjQUFjO0lBQ2QsVUFBVTtJQUNWLFFBQVE7SUFDUixTQUFTO0lBQ1QsVUFBVTtBQUNkOztBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixnQ0FBZ0M7QUFDcEM7O0FBQ0E7SUFDSSxnQ0FBZ0M7QUFDcEM7O0FBQ0E7SUFDSSxhQUFhO0lBQ2IsZUFBZTtJQUNmLGVBQWU7SUFDZixnQkFBZ0I7SUFDaEIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksVUFBVTtBQUNkOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFDQTtJQUNJLGVBQWU7SUFDZixnQkFBZ0I7QUFDcEI7O0FBR0E7SUFDSSxjQUFjO0lBQ2QsZUFBZTtJQUNmLFNBQVM7SUFDVCxnQ0FBZ0M7QUFDcEM7O0FBQ0E7SUFDSSx1QkFBdUI7QUFDM0I7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixtQkFBbUI7SUFDbkIsVUFBVTtJQUNWLDZCQUE2QjtJQUM3QixrQkFBa0I7SUFDbEIsTUFBTTtBQUNWOztBQUNBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksbUJBQW1CO0lBQ25CLFdBQVc7SUFDWCxxQkFBcUI7QUFDekI7O0FBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixxQkFBcUI7SUFDckIsa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLE9BQU87SUFDUCxrQkFBa0I7SUFDbEIsZUFBZTtJQUNmLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIsc0JBQXNCO0FBQzFCOztBQUNBO0lBQ0ksY0FBYztBQUNsQjs7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsU0FBUztJQUNULGdDQUFnQztJQUNoQyxlQUFlO0lBQ2YsZ0JBQWdCO0lBQ2hCLFdBQVc7QUFDZjs7QUFHQTtJQUNJLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLG9CQUFvQjtBQUN4Qjs7QUFDQTtJQUNJLHFCQUFxQjtBQUN6Qjs7QUFDQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0FBQ25COztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLGNBQWM7QUFDbEI7O0FBR0E7SUFDSSxlQUFlO0FBQ25COztBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixrQkFBa0I7SUFDbEIsZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksYUFBYTtJQUNiLG1CQUFtQjtJQUNuQiw4QkFBOEI7SUFDOUIsV0FBVztJQUNYLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxlQUFlO0lBQ2YsZ0JBQWdCO0FBQ3BCOztBQUNBO0lBQ0ksc0JBQXNCO0FBQzFCOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9maW5hbmNlL2NyZWF0ZS1pbnZvaWNlL2NyZWF0ZS1pbnZvaWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLndpemFyZC5jYXJkIHtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbn1cclxuXHJcbi53aXphcmQgPiBkaXYud2l6YXJkLWlubmVyIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuXHJcbi5jYXJkIC5jYXJkLWJvZHkge1xyXG4gICAgcGFkZGluZzogMS44NzVyZW0gMS44NzVyZW07XHJcbn1cclxuXHJcbi5jYXJkLWJvZHkge1xyXG4gICAgZmxleDogMSAxIGF1dG87XHJcbiAgICBtaW4taGVpZ2h0OiAxcHg7XHJcbn1cclxuLnRhYi1jb250ZW50IHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICNmMWY2Zjg7XHJcbiAgICBib3JkZXItdG9wOiAwO1xyXG4gICAgcGFkZGluZzogMnJlbSAxcmVtO1xyXG4gICAgdGV4dC1hbGlnbjoganVzdGlmeTtcclxufVxyXG5cclxuLndpemFyZCAudGFiLXBhbmUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbn1cclxuLnRhYi1jb250ZW50ID4gLmFjdGl2ZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLnRhYi1jb250ZW50ID4gLnRhYi1wYW5lIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcblxyXG4uZm9ybS1ncm91cCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAxLjVyZW07XHJcbn1cclxuLmZvcm0tZ3JvdXAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMXJlbTtcclxufVxyXG4ucm93IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IC0xNXB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xNXB4O1xyXG59XHJcblxyXG4uY29ubmVjdGluZy1saW5lIHtcclxuICAgIGhlaWdodDogMnB4O1xyXG4gICAgYmFja2dyb3VuZDogI2UwZTBlMDtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHdpZHRoOiA2MyU7XHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGxlZnQ6IDI1cHg7XHJcbiAgICByaWdodDogMDtcclxuICAgIHRvcDogNDVweDtcclxuICAgIHotaW5kZXg6IDE7XHJcbn1cclxuXHJcbi53aXphcmQgLm5hdi10YWJzIHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIG1hcmdpbi1ib3R0b206IDA7XHJcbiAgICBib3JkZXItYm90dG9tLWNvbG9yOiB0cmFuc3BhcmVudDtcclxufVxyXG4ubmF2LXRhYnMge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYmVkZjI7XHJcbn1cclxuLm5hdiB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMDtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbn1cclxuXHJcbi53aXphcmQgLm5hdi10YWJzID4gbGkge1xyXG4gICAgd2lkdGg6IDMzJTtcclxufVxyXG5cclxuLnB1bGwtcmlnaHQge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG59XHJcbi5saXN0LWlubGluZSB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDA7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG59XHJcblxyXG5cclxuLndpemFyZCAubmF2LXRhYnMgPiBsaS5hY3RpdmUgPiBhLCAud2l6YXJkIC5uYXYtdGFicyA+IGxpLmFjdGl2ZSA+IGE6aG92ZXIsIC53aXphcmQgLm5hdi10YWJzID4gbGkuYWN0aXZlID4gYTpmb2N1cyB7XHJcbiAgICBjb2xvcjogIzU1NTU1NTtcclxuICAgIGN1cnNvcjogZGVmYXVsdDtcclxuICAgIGJvcmRlcjogMDtcclxuICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50O1xyXG59XHJcbi53aXphcmQgLm5hdi10YWJzID4gbGkgYTpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxufVxyXG4ud2l6YXJkIC5uYXYtdGFicyA+IGxpIGEge1xyXG4gICAgd2lkdGg6IDMwcHg7XHJcbiAgICBoZWlnaHQ6IDMwcHg7XHJcbiAgICBtYXJnaW46IDIwcHggYXV0bztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwMCU7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB0b3A6IDA7XHJcbn1cclxuYTpob3ZlciB7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbi53aXphcmQgbGkuYWN0aXZlIHNwYW4ucm91bmQtdGFiIHtcclxuICAgIGJhY2tncm91bmQ6ICMwZGIwMmI7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJvcmRlci1jb2xvcjogIzBkYjAyYjtcclxufVxyXG5zcGFuLnJvdW5kLXRhYiB7XHJcbiAgICB3aWR0aDogMzBweDtcclxuICAgIGhlaWdodDogMzBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAzMHB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIHotaW5kZXg6IDI7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgY29sb3I6ICMwZTIxNGI7XHJcbiAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcclxufVxyXG4ud2l6YXJkIC5uYXYtdGFicyA+IGxpLmFjdGl2ZSA+IGEgaSB7XHJcbiAgICBjb2xvcjogIzBkYjAyYjtcclxufVxyXG4ud2l6YXJkIC5uYXYtdGFicyA+IGxpIGEgaSB7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB0b3A6IC0xNXB4O1xyXG4gICAgZm9udC1zdHlsZTogbm9ybWFsO1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgICBjb2xvcjogIzAwMDtcclxufVxyXG5cclxuXHJcbi5mb3JtLWdyb3VwIGxhYmVsIHtcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIGxpbmUtaGVpZ2h0OiAxLjRyZW07XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogdG9wO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogLjVyZW07XHJcbn1cclxubGFiZWwge1xyXG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG59XHJcbmZvcm0ge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBtYXJnaW4tdG9wOiAwZW07XHJcbn1cclxuLndpemFyZCAudGFiLXBhbmUge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbn1cclxuLnRhYi1jb250ZW50ID4gLmFjdGl2ZSB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuXHJcbmJ1dHRvbjpub3QoOmRpc2FibGVkKSwgW3R5cGU9XCJidXR0b25cIl06bm90KDpkaXNhYmxlZCksIFt0eXBlPVwicmVzZXRcIl06bm90KDpkaXNhYmxlZCksIFt0eXBlPVwic3VibWl0XCJdOm5vdCg6ZGlzYWJsZWQpIHtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4ubmV4dC1zdGVwIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDViYWE7XHJcbiAgICBjb2xvcjogI2ZmZmY7XHJcbn1cclxuLnByZXYtc3RlcCwgLm5leHQtc3RlcCB7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICBwYWRkaW5nOiA4cHggMjRweDtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbn1cclxuXHJcbi5jYXJkLWhlYWRlcnMge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICNlYWVjZjQ7XHJcbn1cclxuXHJcbi5wdWxsLXJpZ2h0IHtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG4ubGlzdC1pbmxpbmUge1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwO1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxufVxyXG4qLCAqOjpiZWZvcmUsICo6OmFmdGVyIHtcclxuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcbn1cclxuXHJcbi5saXN0LWlubGluZSBsaSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn0iXX0= */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](CreateInvoiceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-create-invoice',
          templateUrl: './create-invoice.component.html',
          styleUrls: ['./create-invoice.component.css']
        }]
      }], function () {
        return [{
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__["PurchaseService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]
        }, {
          type: _auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]
        }, {
          type: _auth_service_email_service__WEBPACK_IMPORTED_MODULE_9__["EmailService"]
        }];
      }, {
        myInputVariable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['myFileInput', {
            "static": false
          }]
        }]
      });
    })();

    var Invoice = function Invoice() {
      _classCallCheck(this, Invoice);

      this.INVOICE_NUMBER = '';
      this.INVOICE_DATE = '';
      this.CURRENCY = '';
      this.DESCRIPTION = '';
      this.INVOICE_AMOUNT = '';
      this.INVOICE_ID = '';
      this.REASON_DESCRIPTION = '';
      this.ADDITIONAL_NOTES = '';
      this.USER_NAME = '';
      this.FILE_NAME = '';
    };
    /***/

  },

  /***/
  "./src/app/components/finance/file-view/file-view.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/components/finance/file-view/file-view.component.ts ***!
    \*********************************************************************/

  /*! exports provided: FileViewComponent */

  /***/
  function srcAppComponentsFinanceFileViewFileViewComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FileViewComponent", function () {
      return FileViewComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _purchase_model_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../purchase/model/upload */
    "./src/app/components/purchase/model/upload.ts");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! file-saver */
    "./node_modules/file-saver/dist/FileSaver.min.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_2__);
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _auth_service_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../auth/service/login.service */
    "./src/app/components/auth/service/login.service.ts");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");

    var _c0 = ["myFileInput"];

    function FileViewComponent_li_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r684 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_li_4_Template_a_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r684);

          var ctx_r683 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r683.getPrintDocument();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Print");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function FileViewComponent_li_5_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Upload");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r678 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("data-target", "#myFileModal1" + ctx_r678.headerId);
      }
    }

    function FileViewComponent_li_6_Template(rf, ctx) {
      if (rf & 1) {
        var _r686 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "a", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_li_6_Template_a_click_1_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r686);

          var ctx_r685 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r685.getDocumentList();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Download");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r679 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("data-target", "#myFileModal" + ctx_r679.headerId);
      }
    }

    function FileViewComponent_tr_28_Template(rf, ctx) {
      if (rf & 1) {
        var _r689 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_tr_28_Template_a_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r689);

          var item_r687 = ctx.$implicit;

          var ctx_r688 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r688.downloadfile(item_r687);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](3, "i", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r687 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\xA0", item_r687.FILE_NAME, "");
      }
    }

    function FileViewComponent_tr_29_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "No attachments Available");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    var FileViewComponent =
    /*#__PURE__*/
    function () {
      function FileViewComponent(purchaseService, spinner, router, loginService, rsaService, notifierService) {
        _classCallCheck(this, FileViewComponent);

        this.purchaseService = purchaseService;
        this.spinner = spinner;
        this.router = router;
        this.loginService = loginService;
        this.rsaService = rsaService;
        this.notifierService = notifierService;
        this.attachments = [];
        this.fileData = null;
        this.notifier = notifierService;
      }

      _createClass(FileViewComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          this.upload = new _purchase_model_upload__WEBPACK_IMPORTED_MODULE_1__["Upload"]();
        }
      }, {
        key: "getDocumentList",
        value: function getDocumentList() {
          var _this14 = this;

          this.spinner.show(); // 247380

          if (this.type === 'INVOICE_DOC_ATTACHMENTS') {
            var details = {
              purchaseType: 'INVOICE_DOC_ATTACHMENTS',
              headerId: this.headerId
            };
            this.purchaseService.getDocs(details).subscribe(function (data) {
              // console.log('kkk', data);
              // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
              //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
              _this14.attachments = data.ATTACHMENTS ? data.ATTACHMENTS.map(function (x) {
                return x.ATTACHMENT;
              }) : [];

              _this14.spinner.hide();
            }, function (error) {
              console.log(error);

              _this14.spinner.hide();
            });
          } else if (this.type === 'AP_INVOICE_ATTACHMENTS') {
            var _details = {
              email: this.email,
              purchaseType: 'ATTACHMENT_LIST',
              attachmentString: this.attachmentString
            }; // const details = { purchaseType: this.type, headerId: this.headerId };
            // console.log('detaiks', details);

            this.purchaseService.getDocs(_details).subscribe(function (data) {
              // console.log('invoice', data);
              _this14.attachments = data.ATTACHMENTS ? data.ATTACHMENTS.map(function (x) {
                return x.ATTACHMENT;
              }) : [];

              _this14.spinner.hide();
            }, function (error) {
              console.log(error);

              _this14.spinner.hide();
            });
          } else if (this.type !== 'ASN_DETAILS') {
            var _details2 = {
              purchaseType: 'PO_ATTACHMENTS',
              headerId: this.headerId
            };
            this.purchaseService.getDocs(_details2).subscribe(function (data) {
              // this.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ?
              //   data.ATTACHMENTS.ATTACHMENT : (data.ATTACHMENTS.ATTACHMENT) ? [data.ATTACHMENTS.ATTACHMENT] : [];
              _this14.attachments = data ? data.ATTACHMENTS.map(function (x) {
                return x.ATTACHMENT;
              }) : [];

              _this14.spinner.hide();
            }, function (error) {
              console.log(error);

              _this14.spinner.hide();
            });
          } else if (this.type === 'ASN_DETAILS') {
            var _details3 = {
              purchaseType: 'ASN_ATTACHMENTS',
              headerId: this.headerId
            };
            this.purchaseService.getDocs(_details3).subscribe(function (data) {
              _this14.attachments = Array.isArray(data.ATTACHMENTS.ATTACHMENT) ? data.ATTACHMENTS.ATTACHMENT : data.ATTACHMENTS.ATTACHMENT ? [data.ATTACHMENTS.ATTACHMENT] : [];

              _this14.spinner.hide();
            }, function (error) {
              console.log(error);

              _this14.spinner.hide();
            });
          }
        }
      }, {
        key: "downloadfile",
        value: function downloadfile(element) {
          var _this15 = this;

          this.spinner.show();
          var dat = {
            documentId: element.DOCUMENT_ID,
            fileName: element.FILE_NAME
          };
          this.purchaseService.downloadAttachment(dat).subscribe(function (data) {
            var file = new Blob([data]);
            Object(file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"])(file, element.FILE_NAME); // var fileURL = URL.createObjectURL(file);
            // window.open(fileURL);

            _this15.spinner.hide();
          }, function (error) {
            console.log(error);

            _this15.spinner.hide();
          });
        } // onFilesAdded(fileInput: any) {
        //   this.spinner.show();
        //   this.fileData = fileInput.target.files as File;
        //   const length = fileInput.target.files.length;
        //   const file = new FormData();
        //   for (let i = 0; i < length; i++) {
        //     file.append('file', this.fileData[0], this.fileData[0].name);
        //   }
        //   this.loginService.FileUpload(file).subscribe((data) => {
        //     this.spinner.hide();
        //     let i;
        //     this.upload.file = this.fileData[0].name;
        //   }, err => {
        //     this.spinner.hide();
        //     this.resetFile();
        //     if (err.error.message) {
        //     } else {
        //     }
        //   });
        // }
        // resetFile() {
        //   this.myInputVariable.nativeElement.value = '';
        // }
        // fileUpload() {
        //   this.spinner.show();
        //   const query = {
        //     DOCUMENT_ID: this.headerId, FILE_NAME: this.upload.file, TYPE: this.type,
        //     TITLE: this.upload.documentCategory, DESCRIPTION: this.upload.description
        //   };
        //   this.purchaseService.FileUpload(query).subscribe((data) => {
        //     if (data === 'S') {
        //       this.upload.documentCategory = '';
        //       this.upload.description = '';
        //       this.resetFile();
        //     }
        //     this.spinner.hide();
        //   }, (error) => {
        //     console.log(error);
        //     this.spinner.hide();
        //   });
        // }

      }, {
        key: "onFilesAdded",
        value: function onFilesAdded(fileInput) {
          var _this16 = this;

          this.spinner.show();
          this.loginService.type = '/temp';
          this.loginService.fileType = 'No';
          this.fileInput1 = fileInput;
          this.fileData = fileInput.target.files;
          var length = fileInput.target.files.length;
          var file = new FormData();

          for (var i = 0; i < length; i++) {
            file.append('file', this.fileData[0], this.fileData[0].name);
          }

          this.loginService.FileUpload(file).subscribe(function (data) {
            _this16.spinner.hide();

            var i;
            _this16.upload.file = _this16.fileData[0].name;
          }, function (err) {
            _this16.spinner.hide();

            _this16.resetFile();

            if (err) {
              console.log("error : ", err);
            } else {}
          });
        }
      }, {
        key: "resetFile",
        value: function resetFile() {
          this.myInputVariable.nativeElement.value = '';
        }
      }, {
        key: "fileUpload",
        value: function fileUpload() {
          var _this17 = this;

          var temp = '';
          this.spinner.show();
          var query = {
            DOCUMENT_ID: this.headerId,
            FILE_NAME: this.upload.file,
            TYPE: this.type,
            TITLE: this.upload.documentCategory,
            DESCRIPTION: this.upload.description,
            USER_ID: this.email
          };
          this.purchaseService.FileUpload(query).subscribe(function (data) {
            var file1 = data.filename;
            var data1 = data.path;
            _this17.loginService.type = data1;
            _this17.loginService.fileType = 'Yes';

            if (data.o === 'S') {
              _this17.fileData = _this17.fileInput1.target.files;
              var _length = _this17.fileInput1.target.files.length;
              var file3 = new FormData();

              for (var i = 0; i < _length; i++) {
                file3.append('file', _this17.fileData[0], file1);
              }

              _this17.loginService.FileUpload(file3).subscribe(function (data) {
                _this17.spinner.hide();

                var i; // this.upload.file = this.fileData[0].name;
              }, function (err) {
                _this17.spinner.hide();

                _this17.resetFile();

                if (err.error.message) {} else {}
              });

              _this17.upload.documentCategory = '';
              _this17.upload.description = '';

              _this17.resetFile();

              _this17.notifier.notify('success', 'File being uploaded!');
            }

            _this17.spinner.hide();
          }, function (error) {
            console.log(error);

            _this17.spinner.hide();
          });
        }
      }, {
        key: "cancel",
        value: function cancel() {
          this.upload.documentCategory = '';
          this.upload.description = '';
          this.resetFile();
        }
      }, {
        key: "getPrintDocument",
        value: function getPrintDocument() {
          var _this18 = this;

          // if(this.type === 'ASN_DETAILS'){
          //   this.generatePdf(this.headerId)
          // }else{
          this.spinner.show();
          var details = {
            printDocumentString: this.printDocString
          };
          this.purchaseService.getDocPrint(details).subscribe(function (data) {
            _this18.spinner.hide();

            var file = new Blob([data]);
            Object(file_saver__WEBPACK_IMPORTED_MODULE_2__["saveAs"])(file, "this.headerId" + '.pdf');
          }, function (error) {
            console.log(error);

            _this18.notifier.notify('warning', 'Print document request has been submitted, You will be notified to download the document shortly.!');

            _this18.spinner.hide();
          }); // }
        }
      }, {
        key: "getTransacationCommunication",
        value: function getTransacationCommunication() {
          this.router.navigateByUrl('/craftsmanautomation/administration/communication', {
            queryParams: {
              communicationString: this.communicationString
            },
            skipLocationChange: true
          });
        }
      }]);

      return FileViewComponent;
    }();

    FileViewComponent.ɵfac = function FileViewComponent_Factory(t) {
      return new (t || FileViewComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_3__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_7__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_8__["NotifierService"]));
    };

    FileViewComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: FileViewComponent,
      selectors: [["app-file-view"]],
      viewQuery: function FileViewComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.myInputVariable = _t.first);
        }
      },
      inputs: {
        headerId: "headerId",
        type: "type",
        attachmentString: "attachmentString",
        printDocString: "printDocString",
        communicationString: "communicationString"
      },
      decls: 106,
      vars: 9,
      consts: [[1, "dropdown", "fileview"], ["type", "button", "data-toggle", "dropdown", 1, "btn", "btn-pri", "dropdown-toggle"], [1, "fa", "fa-ellipsis-h"], [1, "dropdown-menu", "dropdown-menu-right", "dropmenu"], ["style", "padding: 4px;", 4, "ngIf"], [2, "padding", "4px"], ["href", "javascript:void(0)", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-users", 2, "padding-right", "4px"], [1, "modal", "fade", 3, "id"], [1, "modal-dialog", "modeldlg"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title"], ["type", "button", "data-dismiss", "modal", 1, "close"], [1, "modal-body", "reg-form"], ["id", "list-table", 1, "card", "w-100"], [1, "table-responsive", "p-3"], ["id", "listtest", 1, "table", "align-items-center", "table-flush"], [1, "thead-light"], [4, "ngFor", "ngForOf"], [4, "ngIf"], [1, "modal-footer"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-secondary"], [1, "modal-dialog"], [1, "form-group"], ["name", "category", 1, "form-control", 3, "ngModel", "ngModelChange"], ["value", ""], ["value", "Arrangements"], ["value", "System Diagrams and Data Sheets"], ["value", "Detail Drawings"], ["value", "Technical Specifications"], ["value", "Instrument and Electrical Drawings"], ["value", "Performance data"], ["value", "Manufacturing and Quality Procedures"], ["value", "Commissioning, Operating and Maintenance Information"], ["value", "Certification"], ["value", "Test and Inspection Reports"], ["value", "Shipping Documents"], ["value", "Packing Documents"], ["value", "Non-disclosure Agreement (NDA)"], ["value", "Supplier Submission Form"], ["value", "Supplier Quality Manual"], ["value", "Supply Agreement"], ["value", "Price Agreement"], ["value", "Supplier Change Request"], ["type", "text", "name", "description", "id", "exampleInputFirstName", "placeholder", " ", 1, "form-control", 3, "ngModel", "ngModelChange"], ["type", "file", "name", "file", "accept", "image/*,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document", 1, "files", 3, "change"], ["myFileInput", ""], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-primary", 3, "click"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-secondary", 3, "click"], ["href", "javascript:void(0);", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-print", 2, "padding-right", "4px"], ["href", "javascript:void(0)", "data-toggle", "modal", 1, "text-primary"], ["aria-hidden", "true", 1, "fa", "fa-upload", 2, "padding-right", "4px"], ["href", "javascript:void(0)", "data-toggle", "modal", 1, "text-primary", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-download", 2, "padding-right", "4px"], ["href", "javascript:void(0)", 1, "text-primary", 3, "click"], ["aria-hidden", "true", 1, "fa", "fa-paperclip"]],
      template: function FileViewComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "button", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "ul", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, FileViewComponent_li_4_Template, 4, 0, "li", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, FileViewComponent_li_5_Template, 4, 1, "li", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, FileViewComponent_li_6_Template, 4, 1, "li", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "a", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_Template_a_click_8_listener() {
            return ctx.getTransacationCommunication();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "i", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Discussion");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "h4", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Attachments");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "button", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "table", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "thead", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "tr");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "th");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Files");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "tbody");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](28, FileViewComponent_tr_28_Template, 5, 1, "tr", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](29, FileViewComponent_tr_29_Template, 3, 0, "tr", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32, "Close");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](33, " \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h4", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Upload File");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "button", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Document Category");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "select", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FileViewComponent_Template_select_ngModelChange_47_listener($event) {
            return ctx.upload.documentCategory = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "option", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49, "Select");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "option", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Control Documents");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "option", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "Arrangements");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "option", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "System Diagrams and Data Sheets");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "option", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](57, "Detail Drawings");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "option", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Technical Specifications");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "option", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Instrument and Electrical Drawings");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "option", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Performance data");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "option", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Manufacturing and Quality Procedures");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "option", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Commissioning, Operating and Maintenance Information");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "option", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Certification");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "option", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Test and Inspection Reports");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "option", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Shipping Documents");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "option", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](75, "Packing Documents");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "option", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](77, "Non-disclosure Agreement (NDA)");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "option", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Supplier Submission Form");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "option", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Supplier Quality Manual");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "option", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](83, "Supply Agreement");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "option", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "Price Agreement");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "option", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](87, "Supplier Change Request");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "Description");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "input", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function FileViewComponent_Template_input_ngModelChange_92_listener($event) {
            return ctx.upload.description = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "File");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "input", 46, 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function FileViewComponent_Template_input_change_97_listener($event) {
            return ctx.onFilesAdded($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "button", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_Template_button_click_100_listener() {
            return ctx.fileUpload();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "Upload");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](102, " \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "button", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FileViewComponent_Template_button_click_103_listener() {
            return ctx.cancel();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](104, "Cancel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](105, "notifier-container");
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.printDocString);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.type !== "INVOICE_DOC_ATTACHMENTS" && ctx.type !== "AP_INVOICE_ATTACHMENTS");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.attachmentString);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "myFileModal", ctx.headerId, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.attachments);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.attachments.length === 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "myFileModal1", ctx.headerId, "");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.upload.documentCategory);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.upload.description);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵangular_packages_forms_forms_x"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["DefaultValueAccessor"], angular_notifier__WEBPACK_IMPORTED_MODULE_8__["NotifierContainerComponent"]],
      styles: [".btn-pri[_ngcontent-%COMP%] {\r\n  color: #6777EF;\r\n  \r\n}\r\n\r\n.btn[_ngcontent-%COMP%]:hover {\r\n  color: #6777EF;\r\n  text-decoration: none;\r\n  box-shadow: none  !important;\r\n  transition: none !important;\r\n  -webkit-transition: none !important;\r\n}\r\n\r\n.dropdown.fileview[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:after {\r\n  display: none !important;\r\n}\r\n\r\n.dropdown.fileview[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]   i[_ngcontent-%COMP%]:before {\r\n  font-size: 18px;\r\n  color: #000;\r\n}\r\n\r\n.modeldlg[_ngcontent-%COMP%]{\r\n  max-width: 80% !important;\r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9maW5hbmNlL2ZpbGUtdmlldy9maWxlLXZpZXcuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQWM7RUFDZDs7OztxQkFJbUI7QUFDckI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QscUJBQXFCO0VBRXJCLDRCQUE0QjtFQUM1QiwyQkFBMkI7RUFDM0IsbUNBQW1DO0FBQ3JDOztBQUVBO0VBQ0Usd0JBQXdCO0FBQzFCOztBQUVBO0VBQ0UsZUFBZTtFQUNmLFdBQVc7QUFDYjs7QUFFQTtFQUNFLHlCQUF5QjtBQUMzQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9maWxlLXZpZXcvZmlsZS12aWV3LmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLXByaSB7XHJcbiAgY29sb3I6ICM2Nzc3RUY7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XHJcbiAgYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuICAtd2Via2l0LWJveC1zaGFkb3c6IDAgICFpbXBvcnRhbnQ7XHJcbiAgYm94LXNoYWRvdzogMCAgIWltcG9ydGFudDtcclxuICBib3JkZXItcmFkaXVzOiAwOyAqL1xyXG59XHJcblxyXG4uYnRuOmhvdmVyIHtcclxuICBjb2xvcjogIzY3NzdFRjtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgYm94LXNoYWRvdzogbm9uZSAgIWltcG9ydGFudDtcclxuICB0cmFuc2l0aW9uOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5kcm9wZG93bi5maWxldmlldyBidXR0b246YWZ0ZXIge1xyXG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLmRyb3Bkb3duLmZpbGV2aWV3IGJ1dHRvbiBpOmJlZm9yZSB7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG59XHJcblxyXG4ubW9kZWxkbGd7XHJcbiAgbWF4LXdpZHRoOiA4MCUgIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbiJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FileViewComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-file-view',
          templateUrl: './file-view.component.html',
          styleUrls: ['./file-view.component.css']
        }]
      }], function () {
        return [{
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_3__["PurchaseService"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: _auth_service_login_service__WEBPACK_IMPORTED_MODULE_6__["LoginService"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_7__["RsaService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_8__["NotifierService"]
        }];
      }, {
        headerId: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        type: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        attachmentString: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        printDocString: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        communicationString: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"]
        }],
        myInputVariable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['myFileInput', {
            "static": false
          }]
        }]
      });
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/finance-routing.module.ts":
  /*!**************************************************************!*\
    !*** ./src/app/components/finance/finance-routing.module.ts ***!
    \**************************************************************/

  /*! exports provided: FinanceRoutingModule */

  /***/
  function srcAppComponentsFinanceFinanceRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanceRoutingModule", function () {
      return FinanceRoutingModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var _invoice_register_invoice_register_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./invoice-register/invoice-register.component */
    "./src/app/components/finance/invoice-register/invoice-register.component.ts");
    /* harmony import */


    var _payment_register_payment_register_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./payment-register/payment-register.component */
    "./src/app/components/finance/payment-register/payment-register.component.ts");
    /* harmony import */


    var _advance_payment_advance_payment_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./advance-payment/advance-payment.component */
    "./src/app/components/finance/advance-payment/advance-payment.component.ts");
    /* harmony import */


    var _my_ledger_my_ledger_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./my-ledger/my-ledger.component */
    "./src/app/components/finance/my-ledger/my-ledger.component.ts");
    /* harmony import */


    var _upload_invoice_upload_invoice_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./upload-invoice/upload-invoice.component */
    "./src/app/components/finance/upload-invoice/upload-invoice.component.ts");
    /* harmony import */


    var _create_invoice_create_invoice_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./create-invoice/create-invoice.component */
    "./src/app/components/finance/create-invoice/create-invoice.component.ts");
    /* harmony import */


    var src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! src/app/shared/service/auth-guard.service */
    "./src/app/shared/service/auth-guard.service.ts");

    var routes = [{
      path: 'invoiceRegister',
      component: _invoice_register_invoice_register_component__WEBPACK_IMPORTED_MODULE_2__["InvoiceRegisterComponent"],
      data: {
        title: 'Invoice Register',
        breadcrumb: 'Invoice Register'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }, {
      path: 'paymentRegister',
      component: _payment_register_payment_register_component__WEBPACK_IMPORTED_MODULE_3__["PaymentRegisterComponent"],
      data: {
        title: 'Payment Register',
        breadcrumb: 'Payment Register'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }, {
      path: 'advancePayment',
      component: _advance_payment_advance_payment_component__WEBPACK_IMPORTED_MODULE_4__["AdvancePaymentComponent"],
      data: {
        title: 'View Advance Payment',
        breadcrumb: 'View Advance Payment'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }, {
      path: 'myLedger',
      component: _my_ledger_my_ledger_component__WEBPACK_IMPORTED_MODULE_5__["MyLedgerComponent"],
      data: {
        title: 'My Ledger',
        breadcrumb: 'My Ledger'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }, {
      path: 'uploadinvoice',
      component: _upload_invoice_upload_invoice_component__WEBPACK_IMPORTED_MODULE_6__["UploadInvoiceComponent"],
      data: {
        title: 'Upload Invoice',
        breadcrumb: 'Upload Invoice'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }, {
      path: 'createinvoice',
      component: _create_invoice_create_invoice_component__WEBPACK_IMPORTED_MODULE_7__["CreateInvoiceComponent"],
      data: {
        title: 'Create Invoice',
        breadcrumb: 'Create Invoice'
      },
      canActivate: [src_app_shared_service_auth_guard_service__WEBPACK_IMPORTED_MODULE_8__["AuthGuardService"]]
    }];

    var FinanceRoutingModule = function FinanceRoutingModule() {
      _classCallCheck(this, FinanceRoutingModule);
    };

    FinanceRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: FinanceRoutingModule
    });
    FinanceRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function FinanceRoutingModule_Factory(t) {
        return new (t || FinanceRoutingModule)();
      },
      imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](FinanceRoutingModule, {
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FinanceRoutingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
          exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/finance.module.ts":
  /*!******************************************************!*\
    !*** ./src/app/components/finance/finance.module.ts ***!
    \******************************************************/

  /*! exports provided: FinanceModule */

  /***/
  function srcAppComponentsFinanceFinanceModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanceModule", function () {
      return FinanceModule;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var _finance_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./finance-routing.module */
    "./src/app/components/finance/finance-routing.module.ts");
    /* harmony import */


    var _invoice_register_invoice_register_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./invoice-register/invoice-register.component */
    "./src/app/components/finance/invoice-register/invoice-register.component.ts");
    /* harmony import */


    var _payment_register_payment_register_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./payment-register/payment-register.component */
    "./src/app/components/finance/payment-register/payment-register.component.ts");
    /* harmony import */


    var _advance_payment_advance_payment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./advance-payment/advance-payment.component */
    "./src/app/components/finance/advance-payment/advance-payment.component.ts");
    /* harmony import */


    var _my_ledger_my_ledger_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./my-ledger/my-ledger.component */
    "./src/app/components/finance/my-ledger/my-ledger.component.ts");
    /* harmony import */


    var _common_payment_view_common_payment_view_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./common-payment-view/common-payment-view.component */
    "./src/app/components/finance/common-payment-view/common-payment-view.component.ts");
    /* harmony import */


    var _upload_invoice_upload_invoice_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./upload-invoice/upload-invoice.component */
    "./src/app/components/finance/upload-invoice/upload-invoice.component.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");
    /* harmony import */


    var _create_invoice_create_invoice_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./create-invoice/create-invoice.component */
    "./src/app/components/finance/create-invoice/create-invoice.component.ts");
    /* harmony import */


    var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! src/app/shared/shared.module */
    "./src/app/shared/shared.module.ts"); // import { NumberFormattingPipe} from '../../shared/service/number-formatting.pipe'


    var customNotifierOptions = {
      position: {
        horizontal: {
          position: 'middle',
          distance: 12
        },
        vertical: {
          position: 'top',
          distance: 99,
          gap: 15
        }
      },
      theme: 'material',
      behaviour: {
        autoHide: 1200,
        onClick: 'hide',
        onMouseover: 'pauseAutoHide',
        showDismissButton: true,
        stacking: 4
      },
      animations: {
        enabled: true,
        show: {
          preset: 'slide',
          speed: 800,
          easing: 'ease'
        },
        hide: {
          preset: 'fade',
          speed: 1000,
          easing: 'ease',
          offset: 150
        },
        shift: {
          speed: 500,
          easing: 'ease'
        },
        overlap: 90
      }
    };

    var FinanceModule = function FinanceModule() {
      _classCallCheck(this, FinanceModule);
    };

    FinanceModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
      type: FinanceModule
    });
    FinanceModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
      factory: function FinanceModule_Factory(t) {
        return new (t || FinanceModule)();
      },
      imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _finance_routing_module__WEBPACK_IMPORTED_MODULE_5__["FinanceRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], angular_notifier__WEBPACK_IMPORTED_MODULE_12__["NotifierModule"].withConfig(customNotifierOptions), ngx_pagination__WEBPACK_IMPORTED_MODULE_3__["NgxPaginationModule"], ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_15__["SharedModule"]]]
    });

    (function () {
      (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](FinanceModule, {
        declarations: [_invoice_register_invoice_register_component__WEBPACK_IMPORTED_MODULE_6__["InvoiceRegisterComponent"], _payment_register_payment_register_component__WEBPACK_IMPORTED_MODULE_7__["PaymentRegisterComponent"], _advance_payment_advance_payment_component__WEBPACK_IMPORTED_MODULE_8__["AdvancePaymentComponent"], _my_ledger_my_ledger_component__WEBPACK_IMPORTED_MODULE_9__["MyLedgerComponent"], _common_payment_view_common_payment_view_component__WEBPACK_IMPORTED_MODULE_10__["CommonPaymentViewComponent"], _upload_invoice_upload_invoice_component__WEBPACK_IMPORTED_MODULE_11__["UploadInvoiceComponent"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_13__["FileViewComponent"], _create_invoice_create_invoice_component__WEBPACK_IMPORTED_MODULE_14__["CreateInvoiceComponent"]],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _finance_routing_module__WEBPACK_IMPORTED_MODULE_5__["FinanceRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], angular_notifier__WEBPACK_IMPORTED_MODULE_12__["NotifierModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_3__["NgxPaginationModule"], ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_15__["SharedModule"]]
      });
    })();
    /*@__PURE__*/


    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FinanceModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
          declarations: [_invoice_register_invoice_register_component__WEBPACK_IMPORTED_MODULE_6__["InvoiceRegisterComponent"], _payment_register_payment_register_component__WEBPACK_IMPORTED_MODULE_7__["PaymentRegisterComponent"], _advance_payment_advance_payment_component__WEBPACK_IMPORTED_MODULE_8__["AdvancePaymentComponent"], _my_ledger_my_ledger_component__WEBPACK_IMPORTED_MODULE_9__["MyLedgerComponent"], _common_payment_view_common_payment_view_component__WEBPACK_IMPORTED_MODULE_10__["CommonPaymentViewComponent"], _upload_invoice_upload_invoice_component__WEBPACK_IMPORTED_MODULE_11__["UploadInvoiceComponent"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_13__["FileViewComponent"], _create_invoice_create_invoice_component__WEBPACK_IMPORTED_MODULE_14__["CreateInvoiceComponent"]],
          imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"], _finance_routing_module__WEBPACK_IMPORTED_MODULE_5__["FinanceRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], angular_notifier__WEBPACK_IMPORTED_MODULE_12__["NotifierModule"].withConfig(customNotifierOptions), ngx_pagination__WEBPACK_IMPORTED_MODULE_3__["NgxPaginationModule"], ngx_spinner__WEBPACK_IMPORTED_MODULE_4__["NgxSpinnerModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_15__["SharedModule"]]
        }]
      }], null, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/invoice-register/invoice-register.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/components/finance/invoice-register/invoice-register.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: InvoiceRegisterComponent */

  /***/
  function srcAppComponentsFinanceInvoiceRegisterInvoiceRegisterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "InvoiceRegisterComponent", function () {
      return InvoiceRegisterComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_config_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/app/config/config */
    "./src/app/config/config.ts");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var _service_finance_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../service/finance.service */
    "./src/app/components/finance/service/finance.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");
    /* harmony import */


    var _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../../../shared/service/number-formatting.pipe */
    "./src/app/shared/service/number-formatting.pipe.ts");

    function InvoiceRegisterComponent_div_3_div_87_div_4_span_62_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r500 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r500.PAYMENT_STATUS, " ");
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_a_64_Template(rf, ctx) {
      if (rf & 1) {
        var _r508 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 89);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_div_87_div_4_a_64_Template_a_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r508);

          var element_r500 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

          var ctx_r506 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r506.clickedLineId1(element_r500.INVOICE_ID, "INVOICE_ID");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r500 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r500.PAYMENT_STATUS, " ");
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r517 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 102);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_3_Template_span_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r517);

          var ctx_r516 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var element1_r510 = ctx_r516.$implicit;
          var j_r511 = ctx_r516.index;

          var i_r501 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

          var ctx_r515 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r515.viewTaxButton1(element1_r510, i_r501, j_r511);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 103);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "[");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " \xA0");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "i", 104);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "\xA0 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "]");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var j_r511 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        var i_r501 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate2"]("data-target", "#demo", i_r501, "", j_r511, "");
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r523 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 105);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_4_Template_span_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r523);

          var ctx_r522 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          var element1_r510 = ctx_r522.$implicit;
          var j_r511 = ctx_r522.index;

          var i_r501 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

          var ctx_r521 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r521.viewTaxButton2(element1_r510, i_r501, j_r511);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 103);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "[");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, " \xA0");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "i", 106);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "\xA0 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "]");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var j_r511 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        var i_r501 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate2"]("data-target", "#demo", i_r501, "", j_r511, "");
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_tr_68_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](9, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element2_r527 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element2_r527.TAX_LINE_NUM, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element2_r527.TAX_TYPE_NAME, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element2_r527.TAX_RATE_PERCENTAGE, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](9, 7, element2_r527.TAXABLE_AMT), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 9, element2_r527.TAX_AMT), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element2_r527.TAX_CURRENCY_CODE, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element2_r527.SELF_ASSESSED_FLAG, " ");
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_3_Template, 9, 2, "span", 90);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_span_4_Template, 9, 2, "span", 91);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 92);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](16, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](22, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "td", 93);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 94);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 95);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 96);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "p", 97);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31, "Tax Lines");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 98);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Customer PAN No :");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 98);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](38, "Customer GSTIN : ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 98);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Supplier GSTIN :");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 99);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "strong");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "HSN/SAC :");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 100);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "table", 101);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "thead", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](54, "Line number ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Tax Rate Code ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "Tax Rate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60, "Taxable Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](62, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](64, "Currency ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, " Self Assessed");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](68, InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_tr_68_Template, 17, 11, "tr", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
      }

      if (rf & 2) {
        var element1_r510 = ctx.$implicit;
        var j_r511 = ctx.index;

        var i_r501 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().index;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element1_r510.action);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !element1_r510.action);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("\xA0 ", element1_r510.LINE_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("title", element1_r510.ITEM_DESCRIPTION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r510.DESCRIPTION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 18, element1_r510.QUANTITY_INVOICED));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r510.UOM, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](16, 20, element1_r510.UNIT_PRICE));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 22, element1_r510.BASE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](22, 24, element1_r510.TAX_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate2"]("id", "demo", i_r501, "", j_r511, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r510.taxPayerId, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r510.taxRegNo, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r510.myGst, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate"]("title", element1_r510.hsnsacCodeDesc);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r510.hsnsacCode, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", element1_r510.taxData);
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_4_Template(rf, ctx) {
      if (rf & 1) {
        var _r531 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Organization");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Invoice No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "a", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_div_87_div_4_Template_a_click_14_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r531);

          var element_r500 = ctx.$implicit;

          var ctx_r530 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r530.clickedLineId(element_r500.INVOICE_ID, "INVOICE_ID");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Unit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](24, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Type");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](35, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41, "Voucher No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](50, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "a", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_div_87_div_4_Template_a_click_51_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r531);

          var element_r500 = ctx.$implicit;

          var ctx_r532 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r532.getInvoiceAmountData(element_r500.INVOICE_ID);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "i", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "Withholding Tax");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](59, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div", 77);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 78);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](62, InvoiceRegisterComponent_div_3_div_87_div_4_span_62_Template, 2, 1, "span", 79);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](64, InvoiceRegisterComponent_div_3_div_87_div_4_a_64_Template, 2, 1, "a", 80);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 81);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](66, "app-file-view", 82);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "table", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "thead", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "Line No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](76, "Item");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78, "Quantity");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](79, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](80, "UOM");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "Unit Price");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Base Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](86, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](88, InvoiceRegisterComponent_div_3_div_87_div_4_ng_container_88_Template, 69, 26, "ng-container", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r500 = ctx.$implicit;
        var i_r501 = ctx.index;

        var ctx_r498 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "heading", i_r501, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r500.ORGANIZATION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r500.INVOICE_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r500.UNIT);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r500.INVOICE_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r500.TYPE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r500.INVOICE_CURRENCY, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r500.VOUCHER_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](50, 26, element_r500.INVOICE_AMOUNT), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](59, 28, element_r500.WHT_AMOUNT), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](element_r500.PAYMENT_STATUS);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element_r500.PAYMENT_STATUS === "Not Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element_r500.PAYMENT_STATUS !== "Not Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("headerId", element_r500.INVOICE_ID)("type", "AP_INVOICE_ATTACHMENTS")("attachmentString", element_r500.ATTACH_DOCUMENT_STRING)("communicationString", element_r500.COMMUNICATION_STRING);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "collapse ", i_r501, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate1"]("aria-labelledby", "heading", i_r501, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMapInterpolate1"]("card-body", i_r501, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "test", i_r501 + (ctx_r498.page - 1) * 10, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r498.invListHead[i_r501 + (ctx_r498.page - 1) * 10].filteredList);
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_6_tr_25_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](16, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](19, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r534 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r534.TYPE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r534.INVOICE_NUM, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r534.VOUCHER_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r534.INVOICE_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r534.INVOICE_CURRENCY);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](13, 12, element_r534.BASE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](16, 14, element_r534.TAX_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](19, 16, element_r534.INVOICE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](element_r534.PAYMENT_STATUS);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r534.PAYMENT_STATUS);
      }
    }

    function InvoiceRegisterComponent_div_3_div_87_div_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 107);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 108);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "table", 109);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "thead", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Invoice Type");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Invoice No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Voucher No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Base Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Invoice Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Status");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](25, InvoiceRegisterComponent_div_3_div_87_div_6_tr_25_Template, 23, 18, "tr", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r499 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r499.fullInvListHead);
      }
    }

    var _c0 = function _c0(a0, a1) {
      return {
        itemsPerPage: a0,
        currentPage: a1
      };
    };

    function InvoiceRegisterComponent_div_3_div_87_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, InvoiceRegisterComponent_div_3_div_87_div_4_Template, 89, 30, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "paginate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, InvoiceRegisterComponent_div_3_div_87_div_6_Template, 26, 1, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r496 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](5, 2, ctx_r496.invListHead, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](5, _c0, ctx_r496.viewCount, ctx_r496.page)));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r496.showHide && !ctx_r496.tableView && !ctx_r496.shipmentView);
      }
    }

    function InvoiceRegisterComponent_div_3_div_88_Template(rf, ctx) {
      if (rf & 1) {
        var _r536 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 110);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 111);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 112);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "select", 113);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function InvoiceRegisterComponent_div_3_div_88_Template_select_ngModelChange_4_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r536);

          var ctx_r535 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r535.viewCount = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "option", 114);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "10");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "option", 115);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "20");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "option", 116);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "30");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "option", 117);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "40");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "option", 118);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "50");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, " \xA0\xA0\xA0 ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "span", 119);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "pagination-controls", 120);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pageChange", function InvoiceRegisterComponent_div_3_div_88_Template_pagination_controls_pageChange_18_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r536);

          var ctx_r537 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

          return ctx_r537.onPageChanged($event);
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r497 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r497.viewCount);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", ctx_r497.viewCount, " ");
      }
    }

    function InvoiceRegisterComponent_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r539 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "label", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Invoice Date From");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 16);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 18);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Invoice Date To");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](15, "input", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_Template_a_click_17_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r538 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r538.getInvRegister();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "View");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "input", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function InvoiceRegisterComponent_div_3_Template_input_ngModelChange_21_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r540 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r540.searchText = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "i", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_Template_i_click_24_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r541 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r541.setFilteredData();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_Template_a_click_26_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r542 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r542.exportExcel();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "svg", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "g");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "path", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "path", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "path", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "a", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_Template_a_click_34_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r543 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r543.gridView();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "i", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "a", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function InvoiceRegisterComponent_div_3_Template_a_click_36_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r539);

          var ctx_r544 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r544.listView();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](37, "i", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "p", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](42, "date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](47, "i", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](51, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](53, "No.of Invoices Created");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](57, "i", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](61, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "No.of Invoices Approved");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](68, "i", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](72, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](74, "No.of Invoices Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](79, "i", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](83, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](85, "No.of Invoices Not Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](86, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](87, InvoiceRegisterComponent_div_3_div_87_Template, 7, 8, "div", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](88, InvoiceRegisterComponent_div_3_div_88_Template, 19, 2, "div", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r494 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r494.searchText);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("btn-primary", ctx_r494.tableView)("btn-light", !ctx_r494.tableView);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("btn-primary", !ctx_r494.tableView)("btn-light", ctx_r494.tableView);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Data available from: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](42, 17, ctx_r494.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START, "dd-MM-yyyy"), " and Maximum range: ", ctx_r494.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 30, " months");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](51, 20, ctx_r494.noOfInvoiceCreated));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](61, 22, ctx_r494.noOfInvoiceApproved));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](72, 24, ctx_r494.noOfInvoicePaid));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](83, 26, ctx_r494.noOfInvoiceNotPaid));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r494.tableView && ctx_r494.showHide);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r494.tableView);
      }
    }

    function InvoiceRegisterComponent_div_6_tr_32_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](5, "date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](8, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r546 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", item_r546.PAYMENT_NUM, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](5, 3, item_r546.DUE_DATE, "dd-MMM-yyyy"), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](8, 6, item_r546.GROSS_AMOUNT), "");
      }
    }

    function InvoiceRegisterComponent_div_6_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 121);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 122);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h4", 123);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " Payment Schedules");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "button", 124);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5, "\xD7");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 125);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 126);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 127);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Payment Term");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "table", 128);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "thead", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](28, " Due Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "th", 87);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, " Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](32, InvoiceRegisterComponent_div_6_tr_32_Template, 9, 8, "tr", 88);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r495 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r495.paymentSchedule[0].INVOICE_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r495.paymentSchedule[0].PAYMENT_TERM, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r495.paymentSchedule);
      }
    }

    var InvoiceRegisterComponent =
    /*#__PURE__*/
    function () {
      function InvoiceRegisterComponent(purchaseService, chRef, breadCrumbServices, router, notifierService, rsaService, financeService, spinner) {
        _classCallCheck(this, InvoiceRegisterComponent);

        this.purchaseService = purchaseService;
        this.chRef = chRef;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.notifierService = notifierService;
        this.rsaService = rsaService;
        this.financeService = financeService;
        this.spinner = spinner;
        this.fullInvListHead = [];
        this.fullInvListLine = [];
        this.fullSummary = [];
        this.summary = [];
        this.invListHead = [];
        this.invListLine = [];
        this.email = '';
        this.showHide = true;
        this.page = 1;
        this.viewCount = 10;
        this.searchText = '';
        this.lineId = '';
        this.shipmentView = false;
        this.noOfInvoiceCreated = 0;
        this.noOfInvoiceApproved = 0;
        this.noOfInvoicePaid = 0;
        this.noOfInvoiceNotPaid = 0;
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.today = new Date();
        this.action = false;
        this.taxData = [];
        this.fieldName = '';
        this.paymentSchedule = [];
        this.paymentScheduleFlag = false;
        this.selectedmenuFilter = [];
        this.oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
        this.fromDate = ('0' + this.oneMonthBefore.getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
        this.toDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
        this.notifier = notifierService;
      }

      _createClass(InvoiceRegisterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this19 = this;

          this.email = this.rsaService.decrypt(localStorage.getItem('3')); // this.rsaService.numberValueformatNumber()

          this.tableView = true;
          localStorage.setItem('INDF', this.fromDate);
          localStorage.setItem('INDT', this.toDate);
          this.selectedmenu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
          this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(function (_ref2) {
            var MENUACCESS = _ref2.MENUACCESS;
            return MENUACCESS.MENU_CODE === 'INVOICE_REGISTER';
          });
          this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START);
          this.noOfMonths = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 31);
          this.breadCrumbServices.selectValuesChanged.subscribe(function () {
            if (_this19.router.url === '/craftsmanautomation/finance/invoiceRegister') {
              _this19.setFilteredData();
            }
          });
          this.getInvRegister();

          (function ($, _this) {
            $(document).ready(function () {
              // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              // const today = new Date();
              // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
              // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
              //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
                value: _this.fromDate,
                minDate: _this.oneyearstartingDate,
                change: function change(e) {
                  localStorage.setItem('INDF', e.target.value);
                  _this.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
                value: _this.toDate,
                maxDate: new Date(),
                change: function change(e) {
                  localStorage.setItem('INDT', e.target.value);
                  _this.toDate = e.target.value;
                }
              });
            });
          })(jQuery, this);

          this.setAsDataTable();
        }
      }, {
        key: "getInvRegister",
        value: function getInvRegister() {
          var _this20 = this;

          // this.fromDate = localStorage.getItem('INDF');
          // this.toDate = localStorage.getItem('INDT');
          this.spinner.show(); // const details = { email: this.email, financeType: 'INVOICE_DETAILS', date1: this.fromDate, date2: this.toDate };
          // this.financeService.getFinance(details).subscribe((data: any) => {

          var time_difference = Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
          var months_difference = Math.floor(time_difference / (1000 * 60 * 60 * 24) / 31);

          if (-months_difference > this.noOfMonths) {
            // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
            this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
            this.spinner.hide();
          } else {
            var query = this.purchaseService.inputJSON('INVOICE_DETAILS', this.fromDate, this.toDate);
            this.purchaseService.getPurchaseOrder(query).subscribe(function (data) {
              // console.log("getInvRegister",data);
              _this20.spinner.hide();

              if (data) {
                // this.fullInvListHead = Array.isArray(data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER) ? data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER : (data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER) ? [data.INVOICES.INVOICE_HEADERS.INVOICE_HEADER] : [];
                // this.fullInvListLine = Array.isArray(data.INVOICES.INVOICE_LINES.INVOICE_LINE) ? data.INVOICES.INVOICE_LINES.INVOICE_LINE : (data.INVOICES.INVOICE_LINES.INVOICE_LINE) ? [data.INVOICES.INVOICE_LINES.INVOICE_LINE] : [];
                _this20.fullInvListHead = data.INVOICES.INVOICE_HEADERS ? data.INVOICES.INVOICE_HEADERS.map(function (x) {
                  return x.INVOICE_HEADER;
                }) : [];
                _this20.fullInvListLine = data.INVOICES.INVOICE_LINES ? data.INVOICES.INVOICE_LINES.map(function (x) {
                  return x.INVOICE_LINE;
                }) : [];
                _this20.fullSummary = data.INVOICES.INVOICE_SUMMARIES ? data.INVOICES.INVOICE_SUMMARIES.map(function (x) {
                  return x.INVOICE_SUMMARY;
                }) : [];
                _this20.invListHead = _this20.fullInvListHead;
                _this20.invListLine = _this20.fullInvListLine;
                _this20.summary = _this20.fullSummary; // console.log(" fullInvListHead : ", this.fullInvListHead)

                _this20.invListHead.forEach(function (x, i) {
                  // this.invListHead[i].INVOICE_AMOUNT =this.formatNumber.transform(this.invListHead[i].INVOICE_AMOUNT, null)
                  _this20.invListHead[i].filteredList = _this20.invListLine.filter(function (y) {
                    return y.INVOICE_ID === x.INVOICE_ID;
                  });
                });

                _this20.setPoSummary();

                _this20.setFilteredData();

                _this20.setAsDataTable();
              } else {
                _this20.spinner.hide();

                console.error('NULL VALUE');
              }
            }, function (error) {
              _this20.spinner.hide();

              console.log(error);
            });
          }
        }
      }, {
        key: "setPoSummary",
        value: function setPoSummary() {
          var _this21 = this;

          this.summary = this.fullSummary.filter(function (f) {
            var a = true;

            if (_this21.breadCrumbServices.select1 && _this21.breadCrumbServices.select1 !== 'All') {
              a = a && _this21.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this21.breadCrumbServices.select2 && _this21.breadCrumbServices.select2 !== 'All') {
              a = a && _this21.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this21.breadCrumbServices.select3 && _this21.breadCrumbServices.select3 !== 'All') {
              a = a && _this21.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            } // if (this.searchText) {
            //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
            // }


            return a;
          }); // if (this.searchText) {
          //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
          // }

          this.setSummaryData();
        }
      }, {
        key: "setSummaryData",
        value: function setSummaryData() {
          var InvoiceCreated = 0,
              InvoiceApproved = 0,
              InvoicePaid = 0,
              InvoiceNotPaid = 0;
          this.summary.forEach(function (f) {
            InvoiceCreated = InvoiceCreated + Number(f.NO_OF_INV_CREATED);
            InvoiceApproved = InvoiceApproved + Number(f.NO_OF_INV_APPROVED);
            InvoicePaid = InvoicePaid + Number(f.NO_OF_INV_PAID);
            InvoiceNotPaid = InvoiceNotPaid + Number(f.NO_OF_INV_NOT_PAID);
          }); // this.poCounts.forEach(f=>{
          //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
          // })

          this.noOfInvoiceCreated = InvoiceCreated;
          this.noOfInvoiceApproved = InvoiceApproved;
          this.noOfInvoicePaid = InvoicePaid;
          this.noOfInvoiceNotPaid = InvoiceNotPaid;
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this22 = this;

          this.invListHead = this.fullInvListHead.filter(function (f) {
            var a = true;

            if (_this22.breadCrumbServices.select1 && _this22.breadCrumbServices.select1 !== 'All') {
              a = a && _this22.breadCrumbServices.select1 === f.UNIT;
            }

            if (_this22.breadCrumbServices.select2 && _this22.breadCrumbServices.select2 !== 'All') {
              a = a && _this22.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this22.breadCrumbServices.select3 && _this22.breadCrumbServices.select3 !== 'All') {
              a = a && _this22.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            }

            if (_this22.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this22.searchText.toLowerCase());
            }

            return a;
          });
          this.invListHead.forEach(function (f, i) {
            _this22.invListHead[i].list = _this22.fullInvListLine.filter(function (x) {
              return x.INVOICE_ID == f.INVOICE_ID;
            });
          });
          this.setAsDataTable();
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();
          this.invListHead.forEach(function (f, i) {
            (function ($) {
              $('#test' + i).DataTable({
                responsive: true,
                bLengthChange: false,
                retrieve: true,
                pageLength: 5
              });
            })(jQuery);
          });
        }
      }, {
        key: "setAsDataTableList",
        value: function setAsDataTableList() {
          this.chRef.detectChanges();

          (function ($) {
            $('#Pending-table').DataTable({
              responsive: true,
              bLengthChange: false,
              retrieve: true,
              pageLength: 20
            });
          })(jQuery);
        }
      }, {
        key: "showHideTable",
        value: function showHideTable() {
          var _this23 = this;

          this.showHide = false;
          setTimeout(function () {
            _this23.showHide = true;

            _this23.setFilteredData();
          }, 10);
        }
      }, {
        key: "gridView",
        value: function gridView() {
          this.tableView = true;
          this.setAsDataTable();
        }
      }, {
        key: "listView",
        value: function listView() {
          this.tableView = false;
          this.setAsDataTableList();
        }
      }, {
        key: "onPageChanged",
        value: function onPageChanged(event) {
          this.page = event;
        }
      }, {
        key: "exportExcel",
        value: function exportExcel() {
          var details = {
            POLINES: this.fullInvListHead,
            title: 'Invoice Report'
          };
          this.financeService.getExcel(details);
        }
      }, {
        key: "clickedLineId",
        value: function clickedLineId(lineId, name) {
          this.lineId = lineId; // this.shipmentView = true;

          this.router.navigateByUrl('/craftsmanautomation/purchase/pastPurchaseOrders', {
            queryParams: {
              invoiceId: this.lineId,
              fieldName: name
            },
            skipLocationChange: src_app_config_config__WEBPACK_IMPORTED_MODULE_1__["profileData"].hideURL
          });
        }
      }, {
        key: "clickedLineId1",
        value: function clickedLineId1(lineId, name) {
          this.lineId = lineId; // this.shipmentView = true;

          this.router.navigateByUrl('/craftsmanautomation/finance/paymentRegister', {
            queryParams: {
              invoiceId: this.lineId,
              fieldName: name
            },
            skipLocationChange: src_app_config_config__WEBPACK_IMPORTED_MODULE_1__["profileData"].hideURL
          });
        }
      }, {
        key: "goBackShip",
        value: function goBackShip() {
          this.shipmentView = false;
          this.revive();
        }
      }, {
        key: "revive",
        value: function revive() {
          (function ($) {
            $(document).ready(function () {
              var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              var today = new Date();
              var oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
              var fromDate = '' + ('0' + oneMonthBefore.getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              var toDate = '' + ('0' + new Date().getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
                value: localStorage.getItem('INDF'),
                change: function change(e) {
                  localStorage.setItem('INDF', e.target.value);
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
                value: localStorage.getItem('INDT'),
                change: function change(e) {
                  localStorage.setItem('INDT', e.target.value);
                }
              });
            });
          })(jQuery);

          this.setAsDataTable();
        }
      }, {
        key: "viewTaxButton1",
        value: function viewTaxButton1(element, i, j) {
          this.action = false; // this.poHeader[i].filteredList[j].action = false;

          this.invListHead[i].filteredList[j].action = false;
        }
      }, {
        key: "viewTaxButton2",
        value: function viewTaxButton2(element, i, j) {
          var _this24 = this;

          // console.log("ele : ", element)
          this.action = true; // this.poHeader[i].filteredList[j].action = true

          this.invListHead[i].filteredList[j].action = true; // const details = { email: this.email, attachmentString: element.TAX_IDENTIFICATION_STRING, purchaseType: 'TAX_LINES'};
          // this.purchaseService.getDocs(details).subscribe((data: any) => {

          var details = this.purchaseService.inputJSON('TAX_LINES', undefined, undefined, 'TAX_LINES', element.TAX_IDENTIFICATION_STRING);
          this.purchaseService.getPurchaseOrder(details).subscribe(function (data) {
            // console.log("tax details : ", data)
            _this24.taxData = data ? data.TAX_DETAILS.map(function (x) {
              return x.TAX_DETAIL;
            }) : [];
            _this24.taxPayerId = _this24.taxData[0].CUSTOMER_TAX_PAYER_ID;
            _this24.taxRegNo = _this24.taxData[0].CUSTOMER_TAX_REG_NO;
            _this24.myGst = _this24.taxData[0].MY_TAX_REG_NO;
            _this24.hsnsacCode = _this24.taxData[0].HSN_SAC_CODE;
            _this24.invListHead[i].filteredList[j].taxData = data ? data.TAX_DETAILS.map(function (x) {
              return x.TAX_DETAIL;
            }) : [];
            _this24.invListHead[i].filteredList[j].taxPayerId = _this24.taxData[0].CUSTOMER_TAX_PAYER_ID;
            _this24.invListHead[i].filteredList[j].taxRegNo = _this24.taxData[0].CUSTOMER_TAX_REG_NO;
            _this24.invListHead[i].filteredList[j].myGst = _this24.taxData[0].MY_TAX_REG_NO;
            _this24.invListHead[i].filteredList[j].hsnsacCode = _this24.taxData[0].HSN_SAC_CODE;
            _this24.invListHead[i].filteredList[j].hsnsacCodeDesc = _this24.taxData[0].HSN_SAC_CODE_DESC;

            _this24.spinner.hide();
          }, function (err) {
            _this24.spinner.hide();

            console.log(err);
          });
        }
      }, {
        key: "getInvoiceAmountData",
        value: function getInvoiceAmountData(invoiceId) {
          var _this25 = this;

          console.log("inn");
          var query = this.purchaseService.inputJSON("PAYMENT_SCHEDULES", undefined, undefined, "INVOICE_ID", invoiceId);
          this.purchaseService.getPurchaseOrder(query).subscribe(function (data) {
            _this25.paymentSchedule = data.PAYMENT_SCHEDULES ? data.PAYMENT_SCHEDULES.map(function (x) {
              return x.PAYMENT_SCHEDULE;
            }) : [];
            _this25.paymentScheduleFlag = true;
            console.log("payment schedule : ", _this25.paymentSchedule);
          }, function (err) {
            _this25.spinner.hide();

            console.log(err);
          });
        }
      }]);

      return InvoiceRegisterComponent;
    }();

    InvoiceRegisterComponent.ɵfac = function InvoiceRegisterComponent_Factory(t) {
      return new (t || InvoiceRegisterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_5__["NotifierService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_finance_service__WEBPACK_IMPORTED_MODULE_7__["FinanceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_8__["NgxSpinnerService"]));
    };

    InvoiceRegisterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: InvoiceRegisterComponent,
      selectors: [["app-invoice-register"]],
      decls: 11,
      vars: 3,
      consts: [["id", "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["class", "container-fluid", "id", "container-wrapper", 4, "ngIf"], ["id", "amountModel", 1, "modal", "fade"], [1, "modal-dialog", "modeldlg"], ["class", "modal-content", 4, "ngIf"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["id", "container-wrapper", 1, "container-fluid"], [1, "row", "mb-3", "pl-3", "pr-3", 2, "margin-top", "30px"], [1, "col-sm-12", "col-xs-12"], [1, "row", "float", "pt-2", "pb-2", "d-flex", "align-items-center"], [1, "col-md-2"], [1, "form-group"], [1, "control-label"], ["id", "datetimepicker1", 1, "input-group", "date"], ["id", "datepicker", "width", "276", "readonly", ""], [1, "col-sm-2"], ["id", "datetimepicker2", 1, "input-group", "date"], ["id", "datepicker1", "width", "276", "readonly", ""], [1, "col-sm-1"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "mt-3", 3, "click"], [1, "col-sm-4", 2, "padding-top", "13px"], [1, "searchbox-section"], ["type", "text", "placeholder", "Enter Your Search Text", "aria-describedby", "basic-addon2", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "col-sm-1", 2, "padding-top", "13px"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "btn-sm"], [1, "fas", "fa-search", 3, "click"], ["href", "javascript:void(0)", 1, "", 3, "click"], ["xmlns", "http://www.w3.org/2000/svg", "version", "1.1", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", 0, "xmlns", "svgjs", "http://svgjs.com/svgjs", "width", "28", "height", "28", "x", "0", "y", "0", "viewBox", "0 0 512 512", 0, "xml", "space", "preserve", 1, "", 2, "enable-background", "new 0 0 512 512"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5v338c0 18.2-14.7 32.9-32.9 32.9h-304.2c-18.2 0-32.9-14.7-32.9-32.9v-445c0-18.2 14.7-32.9 32.9-32.9h197.2z", "fill", "#23a566", "data-original", "#23a566", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m336.9 254.9h-161.8c-5.8 0-10.5 4.7-10.5 10.5v140.9c0 5.8 4.7 10.5 10.5 10.5h161.9c5.8 0 10.5-4.7 10.5-10.5v-140.8c-.1-5.8-4.8-10.6-10.6-10.6zm-151.3 67.6h59.9v26.7h-59.9zm80.9 0h59.9v26.7h-59.9zm59.9-21h-59.9v-25.5h59.9zm-80.9-25.5v25.5h-59.9v-25.5zm-59.9 94.3h59.9v25.5h-59.9zm80.9 25.5v-25.5h59.9v25.5z", "fill", "#ffffff", "data-original", "#ffffff", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m319.9 135.4 121.1 98.1v-92.1l-68.7-39.9z", "opacity", ".19", "fill", "#000000", "data-original", "#000000"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5h-107c-18.2 0-32.9-14.7-32.9-32.9v-107z", "fill", "#8ed1b1", "data-original", "#8ed1b1"], [1, "col-sm-1", "float-right", 2, "padding-top", "13px"], ["href", "javascript:void(0)", "id", "grid", 1, "btn", "btn-primary", "btn-sm", 3, "click"], [1, "fas", "fa-th"], ["href", "javascript:void(0)", "id", "list", 1, "btn", "btn-light", "btn-sm", 3, "click"], [1, "fas", "fa-list"], [1, "row", "mb-3", "pl-3", "pr-3"], [2, "font-size", "10px", "margin-top", "-30px"], [1, "row", "mb-2", 2, "overflow", "hidden"], [1, "col-md-3"], [1, "design-card-2", "dashboard1"], [1, "icons"], [1, "fa", "fa-flag", "fa-2x"], [1, "data-section"], [1, "design-card", "dashboard1"], [1, "fas", "fa-receipt", "fa-2x"], [1, "design-card-3", "dashboard1"], [1, "fas", "fa-shipping-fast", "fa-2x"], [1, "design-card-1", "dashboard1"], [1, "fas", "fa-shopping-bag", "fa-2x"], ["class", "row mb-3 pl-3 pr-3", 4, "ngIf"], ["class", "row mb-3 pl-3 pr-3", "id", "filter", 4, "ngIf"], [1, "col-sm-12"], [1, "row"], ["id", "accordion", 1, "myaccordion", "w-100"], ["class", "card c1", 4, "ngFor", "ngForOf"], ["class", "row mb-3 pl-3 pr-3", "id", "grid-sections", 4, "ngIf"], [1, "card", "c1"], [1, "card-header", 3, "id"], [1, "mb-0"], [1, "col-sm-3"], [1, "form-group", "c1"], [1, "ctrl-label"], [1, "ctrl-value"], [1, "form-group", "c4"], ["href", "javascript:void(0)", 3, "click"], [1, "form-group", "c3"], [1, "form-group", "c5"], [1, "form-group", "c2"], [1, "form-group", "c8"], [1, "form-group", "c7"], ["href", "javascript:void(0)", "data-toggle", "modal", "data-target", "#amountModel", "title", "Click to view Payment Schedules", 1, "text-primary", 3, "click"], [1, "fa", "fa-th-list", 2, "color", "orange"], [1, "form-group", "c8", 2, "padding-top", "20px"], [1, "ctrl-label", 2, "color", "white"], ["style", "color: white;", 4, "ngIf"], ["style", "color: white;", "href", "javascript:void(0)", 3, "click", 4, "ngIf"], [1, "menubutton"], [3, "headerId", "type", "attachmentString", "communicationString"], ["data-parent", "#accordion", 1, "bodysection", "border-top", 3, "id"], [1, "table-responsive", "p-3"], [1, "table", "align-items-center", "table-flush", 3, "id"], [1, "thead-light"], [1, "text-right"], [4, "ngFor", "ngForOf"], ["href", "javascript:void(0)", 2, "color", "white", 3, "click"], ["style", "font-size:8px; color: blue; margin-left:0; cursor:pointer;", "data-toggle", "collapse", "class", "accordion-toggle", "title", "Hide Tax", 3, "click", 4, "ngIf"], ["style", "font-size:8px;  color: blue; margin-left:0; cursor:pointer;", "data-toggle", "collapse", "class", "accordion-toggle", "title", "Show Tax", 3, "click", 4, "ngIf"], ["data-toggle", "tooltip", 3, "title"], ["colspan", "11", 1, "hiddenRow", 2, "padding-left", "30px"], [1, "accordian-body", "collapse", "p-3"], [1, "dropTiles"], [1, "col-xl-1"], [1, "modal-title", "text-primary", "pl-3"], [1, "col-xl-3"], ["data-toggle", "tooltip", 1, "col-xl-2", 3, "title"], [1, "table", "table-bordered", "p-3"], ["id", "listtest", 1, "table"], ["data-toggle", "collapse", "title", "Hide Tax", 1, "accordion-toggle", 2, "font-size", "8px", "color", "blue", "margin-left", "0", "cursor", "pointer", 3, "click"], [2, "padding-top", "4px", "float", "left"], [1, "fas", "fa-minus"], ["data-toggle", "collapse", "title", "Show Tax", 1, "accordion-toggle", 2, "font-size", "8px", "color", "blue", "margin-left", "0", "cursor", "pointer", 3, "click"], [1, "fas", "fa-plus"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], ["id", "pending-details", 1, "card", "w-100"], ["id", "Pending-table", 1, "table", "align-items-center", "table-flush"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12", 2, "padding-bottom", "10px"], [1, "align-items-center", "justify-content-center", "d-flex"], [1, "form-control", 3, "ngModel", "ngModelChange"], ["value", "10"], ["value", "20"], ["value", "30"], ["value", "40"], ["value", "50"], [2, "margin-top", "10px"], ["maxSize", "5", 1, "product-pagination", 3, "pageChange"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title"], ["type", "button", "data-dismiss", "modal", 1, "close"], [1, "modal-body", "reg-form"], [1, "row", "w-100"], [1, "col-sm-6"], ["id", "listtest", 1, "table", "align-items-center", "table-flush"]],
      template: function InvoiceRegisterComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, InvoiceRegisterComponent_div_3_Template, 89, 28, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, InvoiceRegisterComponent_div_6_Template, 33, 3, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "ngx-spinner", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "p", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](10, "notifier-container");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.shipmentView);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.paymentScheduleFlag);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], ngx_spinner__WEBPACK_IMPORTED_MODULE_8__["NgxSpinnerComponent"], angular_notifier__WEBPACK_IMPORTED_MODULE_5__["NotifierContainerComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__["FileViewComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵangular_packages_forms_forms_x"], ngx_pagination__WEBPACK_IMPORTED_MODULE_12__["PaginationControlsComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["DatePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["DecimalPipe"], ngx_pagination__WEBPACK_IMPORTED_MODULE_12__["PaginatePipe"], _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_13__["NumberFormattingPipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9pbnZvaWNlLXJlZ2lzdGVyL2ludm9pY2UtcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](InvoiceRegisterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-invoice-register',
          templateUrl: './invoice-register.component.html',
          styleUrls: ['./invoice-register.component.css']
        }]
      }], function () {
        return [{
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_2__["PurchaseService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_5__["NotifierService"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]
        }, {
          type: _service_finance_service__WEBPACK_IMPORTED_MODULE_7__["FinanceService"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_8__["NgxSpinnerService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/my-ledger/my-ledger.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/components/finance/my-ledger/my-ledger.component.ts ***!
    \*********************************************************************/

  /*! exports provided: MyLedgerComponent */

  /***/
  function srcAppComponentsFinanceMyLedgerMyLedgerComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MyLedgerComponent", function () {
      return MyLedgerComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _service_finance_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../service/finance.service */
    "./src/app/components/finance/service/finance.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");
    /* harmony import */


    var _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../../../shared/service/number-formatting.pipe */
    "./src/app/shared/service/number-formatting.pipe.ts");

    function MyLedgerComponent_div_82_div_2_tr_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](11, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "td", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](14, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r581 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r581.TRANSACTION_TYPE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r581.TRANSACTION_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r581.TRANSACTION_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r581.CURRENCY, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](11, 6, element_r581.DEBIT), " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](14, 8, element_r581.CREDIT), " ");
      }
    }

    var _c0 = function _c0(a0, a1) {
      return {
        itemsPerPage: a0,
        currentPage: a1
      };
    };

    function MyLedgerComponent_div_82_div_2_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "table", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "thead", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "th", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "th", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](8, "app-file-view", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Document Type");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Document Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Document Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "th", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Debit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "th", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Credit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, MyLedgerComponent_div_82_div_2_tr_23_Template, 15, 10, "tr", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](24, "paginate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r579 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("Ledger statement from ", ctx_r579.fromDate, " to ", ctx_r579.toDate, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("headerId", ctx_r579.ledgerHeader[0].VENDOR_ID)("type", "LEDGER_DETAILS")("attachmentString", ctx_r579.ledgerHeader[0].ATTACH_DOCUMENT_STRING)("communicationString", ctx_r579.ledgerHeader[0].COMMUNICATION_STRING);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](24, 7, ctx_r579.ledgerLine, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](10, _c0, ctx_r579.viewCount, ctx_r579.page)));
      }
    }

    function MyLedgerComponent_div_82_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MyLedgerComponent_div_82_div_2_Template, 25, 13, "div", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r578 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r578.ledgerLength > 0);
      }
    }

    var MyLedgerComponent =
    /*#__PURE__*/
    function () {
      function MyLedgerComponent(financeService, chRef, spinner, breadCrumbServices, router, rsaService, purchaseService, notifierService) {
        _classCallCheck(this, MyLedgerComponent);

        this.financeService = financeService;
        this.chRef = chRef;
        this.spinner = spinner;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.rsaService = rsaService;
        this.purchaseService = purchaseService;
        this.notifierService = notifierService;
        this.ledgerHeaderAll = [];
        this.ledgerHeader = [];
        this.ledgerLineAll = [];
        this.ledgerLine = [];
        this.searchText = '';
        this.closingBal = 0;
        this.openingBal = 0;
        this.debit = 0;
        this.credit = 0;
        this.showHide = true;
        this.viewCount = 10;
        this.page = 1;
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.today = new Date();
        this.selectedmenuFilter = [];
        this.oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
        this.fromDate = ('0' + this.oneMonthBefore.getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear(); // public fromDate = ('0' + (this.oneMonthBefore).getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();

        this.toDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
        this.notifier = notifierService;
      }

      _createClass(MyLedgerComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this26 = this;

          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          localStorage.setItem('LDF', this.fromDate);
          localStorage.setItem('LDT', this.toDate);
          this.selectedmenu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
          this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(function (_ref3) {
            var MENUACCESS = _ref3.MENUACCESS;
            return MENUACCESS.MENU_CODE === 'SUPPLIER_LEDGER';
          });
          this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START);
          this.noOfMonths = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 31);
          this.breadCrumbServices.selectValuesChanged.subscribe(function () {
            if (_this26.router.url === '/craftsmanautomation/finance/myLedger') {
              _this26.showHideTable();

              _this26.setHeader();
            }
          });
          var i = 0;
          setTimeout(function () {
            _this26.getMyLedger();
          }, 100);

          (function ($, _this) {
            $(document).ready(function () {
              // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              // const today = new Date();
              // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
              // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
                value: _this.fromDate,
                minDate: _this.oneyearstartingDate,
                change: function change(e) {
                  localStorage.setItem('LDF', e.target.value);
                  _this.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
                value: _this.toDate,
                maxDate: new Date(),
                change: function change(e) {
                  localStorage.setItem('LDT', e.target.value);
                  _this.toDate = e.target.value;
                }
              });
            });
          })(jQuery, this);
        }
      }, {
        key: "getMyLedger",
        value: function getMyLedger() {
          var _this27 = this;

          this.fromDate = localStorage.getItem('LDF');
          this.toDate = localStorage.getItem('LDT');
          this.spinner.show(); // const details = { email: this.email, financeType: 'LEDGER_DETAILS', date1: this.fromDate, date2: this.toDate };
          // this.financeService.getFinance(details).subscribe((data: any) => {

          var time_difference = Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
          var months_difference = Math.floor(time_difference / (1000 * 60 * 60 * 24) / 31);

          if (-months_difference > this.noOfMonths) {
            // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
            this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
            this.spinner.hide();
          } else {
            var query = this.purchaseService.inputJSON('LEDGER_DETAILS', this.fromDate, this.toDate);
            this.purchaseService.getPurchaseOrder(query).subscribe(function (data) {
              _this27.spinner.hide();

              if (data) {
                // this.ledgerHeaderAll = Array.isArray(data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER) ?
                //   data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER : (data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER) ?
                //     [data.LEDGERS.LEDGER_HEADERS.LEDGER_HEADER] : [];
                // this.ledgerLineAll = Array.isArray(data.LEDGERS.LEDGER_LINES.LEDGER_LINE) ?
                //   data.LEDGERS.LEDGER_LINES.LEDGER_LINE : (data.LEDGERS.LEDGER_LINES.LEDGER_LINE) ? [data.LEDGERS.LEDGER_LINES.LEDGER_LINE] : [];
                _this27.ledgerHeaderAll = data.LEDGERS.LEDGER_HEADERS ? data.LEDGERS.LEDGER_HEADERS.map(function (x) {
                  return x.LEDGER_HEADER;
                }) : [];
                _this27.ledgerLineAll = data.LEDGERS.LEDGER_LINES ? data.LEDGERS.LEDGER_LINES.map(function (x) {
                  return x.LEDGER_LINE;
                }) : [];
                _this27.ledgerHeader = _this27.ledgerHeaderAll;
                _this27.ledgerLine = _this27.ledgerLineAll;
                _this27.ledgerLength = _this27.ledgerHeaderAll ? _this27.ledgerHeaderAll.length : 0; //  this.setHeader();
                //  this.setFilteredData();

                _this27.setHeaderData(); //  this.setAsDataTable();


                _this27.showHideTable();
              }
            }, function (err) {
              _this27.spinner.hide();

              console.log(err);
            });
          }
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this28 = this;

          this.ledgerLine = this.ledgerLineAll.filter(function (f) {
            var a = true;

            if (_this28.breadCrumbServices.select1 && _this28.breadCrumbServices.select1 !== 'All') {
              a = a && _this28.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this28.breadCrumbServices.select2 && _this28.breadCrumbServices.select2 !== 'All') {
              a = a && _this28.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this28.breadCrumbServices.select3 && _this28.breadCrumbServices.select3 !== 'All') {
              a = a && _this28.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            }

            if (_this28.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this28.searchText.toLowerCase());
            }

            return a;
          });
          this.setAsDataTable(); // this.showHideTable();
        }
      }, {
        key: "setHeader",
        value: function setHeader() {
          var _this29 = this;

          this.ledgerHeader = this.ledgerHeaderAll.filter(function (f) {
            var a = true;

            if (_this29.breadCrumbServices.select1 && _this29.breadCrumbServices.select1 !== 'All') {
              a = a && _this29.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this29.breadCrumbServices.select2 && _this29.breadCrumbServices.select2 !== 'All') {
              a = a && _this29.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this29.breadCrumbServices.select3 && _this29.breadCrumbServices.select3 !== 'All') {
              a = a && _this29.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            }

            if (_this29.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this29.searchText.toLowerCase());
            }

            return a;
          });
          this.setHeaderData();
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();

          (function ($) {
            $("#Pending-table").DataTable({
              responsive: true,
              bLengthChange: false,
              retrieve: true,
              sorting: false,
              paging: false,
              info: false
            });
          })(jQuery);
        }
      }, {
        key: "setHeaderData",
        value: function setHeaderData() {
          var closingBal = 0,
              openingBal = 0,
              credit = 0,
              debit = 0;
          this.ledgerHeader.forEach(function (f) {
            closingBal = closingBal + Number(f.CLOSING);
            openingBal = openingBal + Number(f.OPENING);
            credit = credit + Number(f.CREDIT);
            debit = debit + Number(f.DEBIT);
          });
          this.closingBal = closingBal;
          this.openingBal = openingBal;
          this.credit = credit;
          this.debit = debit;
        }
      }, {
        key: "showHideTable",
        value: function showHideTable() {
          var _this30 = this;

          this.showHide = false;
          setTimeout(function () {
            _this30.showHide = true;

            _this30.setFilteredData();
          }, 100);
        }
      }, {
        key: "exportExcel",
        value: function exportExcel() {
          var details = {
            POLINES: this.ledgerLineAll,
            title: 'Ledger Report'
          };
          this.financeService.getLedgerExcel(details);
        }
      }, {
        key: "onPageChanged",
        value: function onPageChanged(event) {
          this.page = event;
        }
      }]);

      return MyLedgerComponent;
    }();

    MyLedgerComponent.ɵfac = function MyLedgerComponent_Factory(t) {
      return new (t || MyLedgerComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_finance_service__WEBPACK_IMPORTED_MODULE_1__["FinanceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]));
    };

    MyLedgerComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: MyLedgerComponent,
      selectors: [["app-my-ledger"]],
      decls: 105,
      vars: 23,
      consts: [["id", "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["id", "container-wrapper", 1, "container-fluid"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12", "col-xs-12"], [1, "row", "float", "pt-2", "pb-2", "d-flex", "align-items-center"], [1, "col-md-2"], [1, "form-group"], [1, "control-label"], ["id", "datetimepicker1", 1, "input-group", "date"], ["id", "datepicker", "name", "fromDate", "width", "276", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-2"], ["id", "datetimepicker2", 1, "input-group", "date"], ["id", "datepicker1", "name", "toDate", "width", "276", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-1"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "mt-3", 2, "margin-top", "5px", 3, "click"], [1, "col-sm-4", 2, "padding-top", "13px"], [1, "searchbox-section"], ["type", "text", "placeholder", "Enter Your Search Text", "aria-describedby", "basic-addon2", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "col-sm-1", 2, "padding-top", "13px"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "btn-sm"], [1, "fas", "fa-search", 3, "click"], ["href", "javascript:void(0)", 1, "", 3, "click"], ["xmlns", "http://www.w3.org/2000/svg", "version", "1.1", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", 0, "xmlns", "svgjs", "http://svgjs.com/svgjs", "width", "28", "height", "28", "x", "0", "y", "0", "viewBox", "0 0 512 512", 0, "xml", "space", "preserve", 1, "", 2, "enable-background", "new 0 0 512 512"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5v338c0 18.2-14.7 32.9-32.9 32.9h-304.2c-18.2 0-32.9-14.7-32.9-32.9v-445c0-18.2 14.7-32.9 32.9-32.9h197.2z", "fill", "#23a566", "data-original", "#23a566", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m336.9 254.9h-161.8c-5.8 0-10.5 4.7-10.5 10.5v140.9c0 5.8 4.7 10.5 10.5 10.5h161.9c5.8 0 10.5-4.7 10.5-10.5v-140.8c-.1-5.8-4.8-10.6-10.6-10.6zm-151.3 67.6h59.9v26.7h-59.9zm80.9 0h59.9v26.7h-59.9zm59.9-21h-59.9v-25.5h59.9zm-80.9-25.5v25.5h-59.9v-25.5zm-59.9 94.3h59.9v25.5h-59.9zm80.9 25.5v-25.5h59.9v25.5z", "fill", "#ffffff", "data-original", "#ffffff", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m319.9 135.4 121.1 98.1v-92.1l-68.7-39.9z", "opacity", ".19", "fill", "#000000", "data-original", "#000000"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5h-107c-18.2 0-32.9-14.7-32.9-32.9v-107z", "fill", "#8ed1b1", "data-original", "#8ed1b1"], [1, "row", "mb-3", "pl-3", "pr-3"], [2, "font-size", "10px", "margin-top", "-30px"], [1, "row", "mb-2", 2, "overflow", "hidden"], [1, "col-md-3"], [1, "design-card-2", "dashboard1"], [1, "icons"], [1, "fas", "fa-balance-scale", "fa-2x", "text-red"], [1, "data-section"], [1, "design-card", "dashboard1"], [1, "fas", "fa-credit-card", "fa-2x", "text-success"], [1, "design-card-3", "dashboard1"], [1, "fas", "fa-credit-card", "fa-2x", "text-info"], [1, "design-card-1", "dashboard1"], [1, "fas", "fa-coins", "fa-2x", "text-warning"], ["class", "row mb-3 pl-3 pr-3", "id", "grid-sections", 4, "ngIf"], [1, "col-sm-12", 2, "padding-bottom", "10px"], [1, "align-items-center", "justify-content-center", "d-flex"], [1, "form-control", 3, "ngModel", "ngModelChange"], ["value", "10"], ["value", "20"], ["value", "30"], ["value", "40"], ["value", "50"], [2, "margin-top", "10px"], ["maxSize", "5", 1, "product-pagination", 3, "pageChange"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], ["id", "pending-details", 1, "card", "w-100"], ["class", "table-responsive  p-3", 4, "ngIf"], [1, "table-responsive", "p-3"], ["id", "Pending-table", 1, "table", "align-items-center", "table-flush"], [1, "thead-light"], ["colspan", "5", 2, "background-color", "white"], [2, "background-color", "white"], [1, "menubutton"], [3, "headerId", "type", "attachmentString", "communicationString"], [1, "text-right"], [4, "ngFor", "ngForOf"]],
      template: function MyLedgerComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Accounting Date From");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "input", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function MyLedgerComponent_Template_input_ngModelChange_12_listener($event) {
            return ctx.fromDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "label", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "Accounting Date To");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "input", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function MyLedgerComponent_Template_input_ngModelChange_18_listener($event) {
            return ctx.toDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "a", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MyLedgerComponent_Template_a_click_20_listener() {
            return ctx.getMyLedger();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "View");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "span", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "input", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function MyLedgerComponent_Template_input_ngModelChange_24_listener($event) {
            return ctx.searchText = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "i", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MyLedgerComponent_Template_i_click_27_listener() {
            return ctx.setFilteredData();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "a", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function MyLedgerComponent_Template_a_click_29_listener() {
            return ctx.exportExcel();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "svg", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "g");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](33, "path", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](34, "path", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](35, "path", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "p", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](40, "date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](42, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](45, "i", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](46, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](49, "numberFormatting");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Opening Balance");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](55, "i", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](59, "numberFormatting");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, " Debit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](65, "i", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](67, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](68);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](69, "numberFormatting");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, " Credit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "div", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](75, "i", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "div", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "h3");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](78);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](79, "numberFormatting");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "p");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](81, "Closing Balance");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](82, MyLedgerComponent_div_82_Template, 3, 1, "div", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](84, "div", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "select", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function MyLedgerComponent_Template_select_ngModelChange_87_listener($event) {
            return ctx.viewCount = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "option", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](89, "10");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](90, "option", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](91, "20");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](92, "option", 49);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](93, "30");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "option", 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](95, "40");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](96, "option", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](97, "50");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](98, " \xA0\xA0\xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "span", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "pagination-controls", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pageChange", function MyLedgerComponent_Template_pagination_controls_pageChange_100_listener($event) {
            return ctx.onPageChanged($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](101, "ngx-spinner", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "p", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](103, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](104, "notifier-container");
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.fromDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.toDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Data available from: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](40, 12, ctx.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START, "dd-MM-yyyy"), " and Maximum range: ", ctx.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 30, " months");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](49, 15, ctx.openingBal));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](59, 17, ctx.debit));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](69, 19, ctx.credit));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](79, 21, ctx.closingBal));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showHide);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.viewCount);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_8__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["ɵangular_packages_forms_forms_x"], ngx_pagination__WEBPACK_IMPORTED_MODULE_10__["PaginationControlsComponent"], ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerComponent"], angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierContainerComponent"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__["FileViewComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["DatePipe"], _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_12__["NumberFormattingPipe"], ngx_pagination__WEBPACK_IMPORTED_MODULE_10__["PaginatePipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9teS1sZWRnZXIvbXktbGVkZ2VyLmNvbXBvbmVudC5jc3MifQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](MyLedgerComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-my-ledger',
          templateUrl: './my-ledger.component.html',
          styleUrls: ['./my-ledger.component.css']
        }]
      }], function () {
        return [{
          type: _service_finance_service__WEBPACK_IMPORTED_MODULE_1__["FinanceService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_2__["NgxSpinnerService"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]
        }, {
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_6__["PurchaseService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/payment-register/payment-register.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/components/finance/payment-register/payment-register.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: PaymentRegisterComponent */

  /***/
  function srcAppComponentsFinancePaymentRegisterPaymentRegisterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PaymentRegisterComponent", function () {
      return PaymentRegisterComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_config_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/app/config/config */
    "./src/app/config/config.ts");
    /* harmony import */


    var _service_finance_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../service/finance.service */
    "./src/app/components/finance/service/finance.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var ngx_pagination__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ngx-pagination */
    "./node_modules/ngx-pagination/__ivy_ngcc__/dist/ngx-pagination.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ../file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");
    /* harmony import */


    var _common_payment_view_common_payment_view_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ../common-payment-view/common-payment-view.component */
    "./src/app/components/finance/common-payment-view/common-payment-view.component.ts");
    /* harmony import */


    var _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ../../../shared/service/number-formatting.pipe */
    "./src/app/shared/service/number-formatting.pipe.ts");

    function PaymentRegisterComponent_div_3_div_89_tr_75_Template(rf, ctx) {
      if (rf & 1) {
        var _r555 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "a", 85);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PaymentRegisterComponent_div_3_div_89_tr_75_Template_a_click_2_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r555);

          var element1_r553 = ctx.$implicit;

          var ctx_r554 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);

          return ctx_r554.clickedLineId(element1_r553.INVOICE_ID, "INVOICE_ID");
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "td", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](12, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "td", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](15, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "td", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](18, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element1_r553 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r553.INVOICE_NUM);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r553.VOUCHER_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r553.INVOICE_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element1_r553.INVOICE_CURRENCY);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](12, 11, element1_r553.LINE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](15, 13, element1_r553.TAX_AMOUNT), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](18, 15, element1_r553.INVOICE_AMOUNT), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMap"](element1_r553.PAYMENT_STATUS);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element1_r553.PAYMENT_STATUS, "");
      }
    }

    function PaymentRegisterComponent_div_3_div_89_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Organization");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 71);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, " Payment Reference No ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 72);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Unit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "div", 73);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "Payment Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "div", 74);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](29, "Payment Method");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](30, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "div", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 76);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40, "Amount Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](43, "numberFormatting");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "div", 75);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "div", 69);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](46, "UTR Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 70);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 77);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](51, "app-file-view", 78);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](52, "div", 79);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "div", 80);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](55, "table", 81);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](56, "thead", 82);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Invoice No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](61, "Voucher No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](65, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "th", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67, "Line Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "th", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](69, "Tax Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "th", 83);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Invoice Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](73, "Payment Status");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](75, PaymentRegisterComponent_div_3_div_89_tr_75_Template, 22, 17, "tr", 84);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r550 = ctx.$implicit;
        var i_r551 = ctx.index;

        var ctx_r549 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "heading", i_r551, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.ORGANIZATION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r550.PAYMENT_REF_NUMBER, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.UNIT);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.PAYMENT_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.PAYMENT_METHOD);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.CURRENCY);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](43, 21, element_r550.AMOUNT_PAID));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r550.UTR_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("headerId", element_r550.INVOICE_ID)("type", "AP_INVOICE_ATTACHMENTS")("attachmentString", element_r550.ATTACH_DOCUMENT_STRING)("printDocString", element_r550.PRINT_DOCUMENT_STRING)("communicationString", element_r550.COMMUNICATION_STRING);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "collapse ", i_r551, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattributeInterpolate1"]("aria-labelledby", "heading", i_r551, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassMapInterpolate1"]("card-body", i_r551, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("id", "test", i_r551 + (ctx_r549.page - 1) * 10, "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r549.payment[i_r551 + (ctx_r549.page - 1) * 10].filteredList);
      }
    }

    var _c0 = function _c0(a0, a1) {
      return {
        itemsPerPage: a0,
        currentPage: a1
      };
    };

    function PaymentRegisterComponent_div_3_Template(rf, ctx) {
      if (rf & 1) {
        var _r557 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 19);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 20);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 22);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "label", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Payment Date From ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 26);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PaymentRegisterComponent_div_3_Template_div_ngModelChange_8_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r556 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r556.fromDate = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](9, "input", 27);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "div", 28);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "div", 24);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "label", 25);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Payment Date To");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 29);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "input", 30);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PaymentRegisterComponent_div_3_Template_input_ngModelChange_15_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r558 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r558.toDate = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 31);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "a", 32);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PaymentRegisterComponent_div_3_Template_a_click_17_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r559 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r559.getPaymentRegister();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "View");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "div", 33);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 34);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "input", 35);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PaymentRegisterComponent_div_3_Template_input_ngModelChange_21_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r560 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r560.searchText = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](23, "a", 37);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "i", 38);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PaymentRegisterComponent_div_3_Template_i_click_24_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r561 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r561.setFilteredData();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "div", 36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "a", 39);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function PaymentRegisterComponent_div_3_Template_a_click_26_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r557);

          var ctx_r562 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r562.exportExcel();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "svg", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "g");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](29, "path", 41);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](30, "path", 42);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](31, "path", 43);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "path", 44);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceHTML"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](34, "div", 21);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](35, "p", 46);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](36);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](37, "date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 47);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 49);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](42, "i", 51);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](44, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](46, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](48, "No.Of Invoices Paid");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "div", 53);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](51, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "i", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](56, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](58, "No.Of Payment Approved");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](59, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](60, "i", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "div", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](63, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](64, "i", 54);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](66, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](68, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](70, "No.Of Payment Created");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](71, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](72, "i", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "div", 48);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "div", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](75, "div", 50);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](76, "i", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 52);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "h3");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](80, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "p");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](82, "No.Of Payment Void");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "small");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](84, "i", 55);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](89, PaymentRegisterComponent_div_3_div_89_Template, 76, 23, "div", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](90, "paginate");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r547 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r547.fromDate);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r547.toDate);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r547.searchText);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"](" Data available from: ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](37, 10, ctx_r547.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START, "dd-MM-yyyy"), " and Maximum range: ", ctx_r547.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 30, " months");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](9);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](46, 13, ctx_r547.noOfinvoicePaid));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](56, 15, ctx_r547.noOfpaymentApproved), "");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](68, 17, ctx_r547.noOfpaymentCreated));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](80, 19, ctx_r547.noOfpaymentVoid));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind2"](90, 21, ctx_r547.payment, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction2"](24, _c0, ctx_r547.viewCount, ctx_r547.page)));
      }
    }

    function PaymentRegisterComponent_app_common_payment_view_23_Template(rf, ctx) {
      if (rf & 1) {
        var _r564 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-common-payment-view", 86);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("goBackShip", function PaymentRegisterComponent_app_common_payment_view_23_Template_app_common_payment_view_goBackShip_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r564);

          var ctx_r563 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r563.goBackShip();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r548 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("shipmentId", ctx_r548.lineId)("shipmentView", ctx_r548.shipmentView);
      }
    }

    var PaymentRegisterComponent =
    /*#__PURE__*/
    function () {
      function PaymentRegisterComponent(financeService, chRef, spinner, breadCrumbServices, router, rsaService, notifierService, purchaseService) {
        _classCallCheck(this, PaymentRegisterComponent);

        this.financeService = financeService;
        this.chRef = chRef;
        this.spinner = spinner;
        this.breadCrumbServices = breadCrumbServices;
        this.router = router;
        this.rsaService = rsaService;
        this.notifierService = notifierService;
        this.purchaseService = purchaseService;
        this.viewCount = 10;
        this.searchText = '';
        this.page = 1;
        this.payment = [];
        this.paymentLine = [];
        this.paymentAll = [];
        this.paymentLineAll = [];
        this.paymentSummaryALL = [];
        this.summary = [];
        this.lineId = '';
        this.shipmentView = false;
        this.noOfinvoicePaid = 0;
        this.noOfpaymentApproved = 0;
        this.noOfpaymentCreated = 0;
        this.noOfpaymentVoid = 0;
        this.selectedmenuFilter = [];
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.today = new Date();
        this.oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
        this.fromDate = ('0' + this.oneMonthBefore.getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
        this.toDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
        this.notifier = notifierService;
      }

      _createClass(PaymentRegisterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this31 = this;

          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          localStorage.setItem('PRDF', this.fromDate);
          localStorage.setItem('PRDT', this.toDate);
          this.view = true;
          this.selectedmenu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
          this.selectedmenuFilter = JSON.parse(this.selectedmenu).filter(function (_ref4) {
            var MENUACCESS = _ref4.MENUACCESS;
            return MENUACCESS.MENU_CODE === 'PAYMENT_REGISTER';
          });
          this.oneyearstartingDate = new Date(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE_START);
          this.noOfMonths = Math.round(this.selectedmenuFilter[0].MENUACCESS.DATE_RANGE / 31);
          this.breadCrumbServices.selectValuesChanged.subscribe(function () {
            if (_this31.router.url === '/craftsmanautomation/finance/paymentRegister') {
              _this31.setFilteredData();
            }
          });
          this.getPaymentRegister();

          (function ($, _this) {
            $(document).ready(function () {
              // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              // const today = new Date();
              // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
              // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
              //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: _this.fromDate,
                minDate: _this.oneyearstartingDate,
                change: function change(e) {
                  localStorage.setItem('PRDF', e.target.value);
                  _this.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: _this.toDate,
                maxDate: new Date(),
                change: function change(e) {
                  localStorage.setItem('PRDT', e.target.value);
                  _this.toDate = e.target.value;
                }
              });
            });
          })(jQuery, this);
        }
      }, {
        key: "getPaymentRegister",
        value: function getPaymentRegister() {
          var _this32 = this;

          this.spinner.show(); // this.fromDate = localStorage.getItem('PRDF');
          // this.toDate = localStorage.getItem('PRDT');
          // const details = { email: this.email, financeType: 'REGULAR_PAYMENT', date1: this.fromDate, date2: this.toDate };
          // this.financeService.getFinance(details).subscribe((data: any) => {

          var time_difference = Math.floor(new Date(this.fromDate).getTime() - new Date(this.toDate).getTime());
          var months_difference = Math.floor(time_difference / (1000 * 60 * 60 * 24) / 31);

          if (-months_difference > this.noOfMonths) {
            // if(new Date(this.fromDate).getTime() < new Date(this.toDate).getTime()){
            this.notifier.notify("error", "You can request only " + this.noOfMonths + " month(s) of data");
            this.spinner.hide();
          } else {
            var query = this.purchaseService.inputJSON('REGULAR_PAYMENT', this.fromDate, this.toDate);
            this.purchaseService.getPurchaseOrder(query).subscribe(function (data) {
              console.log("getPaymentRegister", data);

              _this32.spinner.hide(); // this.paymentLineAll = Array.isArray(data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE) ? data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE :
              //   (data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE) ? [data.REGPAYMENT.PAYMENTLINES.PAYMENTLINE] : [];
              // this.paymentAll = Array.isArray(data.REGPAYMENT.PAYMENTS.PAYMENT) ?
              //   data.REGPAYMENT.PAYMENTS.PAYMENT : (data.REGPAYMENT.PAYMENTS.PAYMENT) ? [(data.REGPAYMENT.PAYMENTS.PAYMENT)] : [];


              _this32.paymentLineAll = data.REGPAYMENT.PAYMENTLINES ? data.REGPAYMENT.PAYMENTLINES.map(function (x) {
                return x.PAYMENTLINE;
              }) : [];
              _this32.paymentAll = data.REGPAYMENT.PAYMENTS ? data.REGPAYMENT.PAYMENTS.map(function (x) {
                return x.PAYMENT;
              }) : [];
              _this32.paymentSummaryALL = data.REGPAYMENT.PAYMENT_SUMMARIES ? data.REGPAYMENT.PAYMENT_SUMMARIES.map(function (x) {
                return x.PAYMENT_SUMMARY;
              }) : [];
              _this32.payment = _this32.paymentAll;
              _this32.paymentLine = _this32.paymentLineAll;
              _this32.summary = _this32.paymentSummaryALL;

              _this32.payment.forEach(function (f, i) {
                _this32.payment[i].filteredList = _this32.paymentLineAll.filter(function (x) {
                  return x.CHECK_ID == f.CHECK_ID;
                });
              });

              console.log("payment reg : ", _this32.payment);

              _this32.setInvoiceSummary();

              _this32.setAsDataTable();

              _this32.setFilteredData();
            }, function (err) {
              _this32.spinner.hide();

              console.log(err);
            });
          }
        }
      }, {
        key: "setInvoiceSummary",
        value: function setInvoiceSummary() {
          var _this33 = this;

          this.summary = this.paymentSummaryALL.filter(function (f) {
            var a = true;

            if (_this33.breadCrumbServices.select1 && _this33.breadCrumbServices.select1 !== 'All') {
              a = a && _this33.breadCrumbServices.select1 === f.BUYER_UNIT;
            }

            if (_this33.breadCrumbServices.select2 && _this33.breadCrumbServices.select2 !== 'All') {
              a = a && _this33.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this33.breadCrumbServices.select3 && _this33.breadCrumbServices.select3 !== 'All') {
              a = a && _this33.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            } // if (this.searchText) {
            //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
            // }


            return a;
          }); // if (this.searchText) {
          //   a = a && (JSON.stringify(f).toLowerCase().includes(this.searchText.toLowerCase()));
          // }

          this.setSummaryData();
        }
      }, {
        key: "setSummaryData",
        value: function setSummaryData() {
          var invoicePaid = 0,
              paymentApproved = 0,
              paymentCreated = 0,
              paymentVoid = 0;
          this.summary.forEach(function (f) {
            invoicePaid = invoicePaid + Number(f.NO_OF_INV_PAID);
            paymentApproved = paymentApproved + Number(f.NO_OF_PAY_APPROVED);
            paymentCreated = paymentCreated + Number(f.NO_OF_PAY_CREATED);
            paymentVoid = paymentVoid + Number(f.NO_OF_PAY_VOID);
          }); // this.poCounts.forEach(f=>{
          //   ordersReceived = ordersReceived + Number(f.PAST_PO_COUNT);
          // })

          this.noOfinvoicePaid = invoicePaid;
          this.noOfpaymentApproved = paymentApproved;
          this.noOfpaymentCreated = paymentCreated;
          this.noOfpaymentVoid = paymentVoid;
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this34 = this;

          this.payment = this.paymentAll.filter(function (f) {
            var a = true;

            if (_this34.breadCrumbServices.select1 && _this34.breadCrumbServices.select1 !== 'All') {
              a = a && _this34.breadCrumbServices.select1 === f.UNIT;
            }

            if (_this34.breadCrumbServices.select2 && _this34.breadCrumbServices.select2 !== 'All') {
              a = a && _this34.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            }

            if (_this34.breadCrumbServices.select3 && _this34.breadCrumbServices.select3 !== 'All') {
              a = a && _this34.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            }

            if (_this34.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this34.searchText.toLowerCase());
            }

            return a;
          });
          this.payment.forEach(function (f, i) {
            _this34.payment[i].list = _this34.paymentLineAll.filter(function (x) {
              return x.CHECK_ID == f.CHECK_ID;
            });
          });
          this.setAsDataTable();
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();
          this.payment.forEach(function (f, i) {
            (function ($) {
              $('#test' + i).DataTable({
                responsive: true,
                bLengthChange: false,
                retrieve: true,
                pageLength: 5
              });
            })(jQuery);
          });
        }
      }, {
        key: "exportExcel",
        value: function exportExcel() {
          var details = {
            POLINES: this.paymentLineAll,
            title: 'Payment Report'
          };
          this.financeService.getPaymentExcel(details);
        } // getTableDetails(paymentId) {
        //   this.view = false;
        //   this.paymentLine = this.paymentLineAll.filter(x => x.CHECK_ID == paymentId);
        //   this.setAsDataTable();
        // }
        // getBack() {
        //   this.view = true;
        // }

      }, {
        key: "onPageChanged",
        value: function onPageChanged(event) {
          this.page = event;
        }
      }, {
        key: "clickedLineId",
        value: function clickedLineId(lineId, name) {
          this.lineId = lineId; // this.shipmentView = true;

          this.router.navigateByUrl('/craftsmanautomation/purchase/pastPurchaseOrders', {
            queryParams: {
              invoiceId: this.lineId,
              fieldName: name
            },
            skipLocationChange: src_app_config_config__WEBPACK_IMPORTED_MODULE_1__["profileData"].hideURL
          });
        }
      }, {
        key: "goBackShip",
        value: function goBackShip() {
          this.shipmentView = false;
          this.revive();
        }
      }, {
        key: "revive",
        value: function revive() {
          (function ($) {
            $(document).ready(function () {
              // const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              // const today = new Date();
              // const oneMonthBefore = new Date(new Date().setDate((new Date()).getDate() - 30));
              // const fromDate = '' + ('0' + (oneMonthBefore).getDate()).slice(-2) + '-' +
              //   months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              // const toDate = '' + ('0' + (new Date()).getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: localStorage.getItem('PRDF'),
                change: function change(e) {
                  localStorage.setItem('PRDF', e.target.value);
                  this.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                value: localStorage.getItem('PRDT'),
                change: function change(e) {
                  localStorage.setItem('PRDT', e.target.value);
                }
              });
            });
          })(jQuery);

          this.setAsDataTable();
        }
      }]);

      return PaymentRegisterComponent;
    }();

    PaymentRegisterComponent.ɵfac = function PaymentRegisterComponent_Factory(t) {
      return new (t || PaymentRegisterComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_finance_service__WEBPACK_IMPORTED_MODULE_2__["FinanceService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_8__["PurchaseService"]));
    };

    PaymentRegisterComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: PaymentRegisterComponent,
      selectors: [["app-payment-register"]],
      decls: 28,
      vars: 4,
      consts: [[1, "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["class", "container-fluid", "id", "container-wrapper", 4, "ngIf"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12", 2, "padding-bottom", "10px"], [1, "align-items-center", "justify-content-center", "d-flex"], [1, "form-control", 3, "ngModel", "ngModelChange"], ["value", "10"], ["value", "20"], ["value", "30"], ["value", "40"], ["value", "50"], [2, "margin-top", "10px"], ["maxSize", "5", 1, "product-pagination", 3, "pageChange"], [2, "margin-top", "75px"], [3, "shipmentId", "shipmentView", "goBackShip", 4, "ngIf"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["id", "container-wrapper", 1, "container-fluid"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3", 2, "margin-top", "80px"], [1, "col-sm-12", "col-xs-12"], [1, "row", "float", "pt-2", "pb-2", "d-flex", "align-items-center"], [1, "col-md-2"], [1, "form-group"], [1, "control-label"], ["id", "datetimepicker1", "ngDefaultControl", "", 1, "input-group", "date", 3, "ngModel", "ngModelChange"], ["id", "datepicker", "width", "276", "readonly", ""], [1, "col-sm-2"], ["id", "datetimepicker2", 1, "input-group", "date"], ["id", "datepicker1", "width", "276", "ngDefaultControl", "", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-1"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "mt-3", 3, "click"], [1, "col-sm-4", 2, "padding-top", "13px"], [1, "searchbox-section"], ["type", "text", "placeholder", "Enter Your Search Text", "aria-describedby", "basic-addon2", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "col-sm-1", 2, "padding-top", "13px"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "btn-sm"], [1, "fas", "fa-search", 3, "click"], ["href", "javascript:void(0)", 1, "", 3, "click"], ["xmlns", "http://www.w3.org/2000/svg", "version", "1.1", 0, "xmlns", "xlink", "http://www.w3.org/1999/xlink", 0, "xmlns", "svgjs", "http://svgjs.com/svgjs", "width", "28", "height", "28", "x", "0", "y", "0", "viewBox", "0 0 512 512", 0, "xml", "space", "preserve", 1, "", 2, "enable-background", "new 0 0 512 512"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5v338c0 18.2-14.7 32.9-32.9 32.9h-304.2c-18.2 0-32.9-14.7-32.9-32.9v-445c0-18.2 14.7-32.9 32.9-32.9h197.2z", "fill", "#23a566", "data-original", "#23a566", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m336.9 254.9h-161.8c-5.8 0-10.5 4.7-10.5 10.5v140.9c0 5.8 4.7 10.5 10.5 10.5h161.9c5.8 0 10.5-4.7 10.5-10.5v-140.8c-.1-5.8-4.8-10.6-10.6-10.6zm-151.3 67.6h59.9v26.7h-59.9zm80.9 0h59.9v26.7h-59.9zm59.9-21h-59.9v-25.5h59.9zm-80.9-25.5v25.5h-59.9v-25.5zm-59.9 94.3h59.9v25.5h-59.9zm80.9 25.5v-25.5h59.9v25.5z", "fill", "#ffffff", "data-original", "#ffffff", 1, ""], ["xmlns", "http://www.w3.org/2000/svg", "d", "m319.9 135.4 121.1 98.1v-92.1l-68.7-39.9z", "opacity", ".19", "fill", "#000000", "data-original", "#000000"], ["xmlns", "http://www.w3.org/2000/svg", "d", "m441 140.5h-107c-18.2 0-32.9-14.7-32.9-32.9v-107z", "fill", "#8ed1b1", "data-original", "#8ed1b1"], [1, "row", "mb-3", "pl-3", "pr-3"], [2, "font-size", "10px", "margin-top", "-30px"], [1, "row", "mb-2", 2, "overflow", "hidden"], [1, "col-md-3"], [1, "design-card-2", "dashboard1"], [1, "icons"], [1, "fa", "fa-object-group", "fa-2x"], [1, "data-section"], [1, "design-card", "dashboard1"], [1, "fa", "fa-retweet", "fa-2x"], [1, "fas", "fa-long-arrow-alt-right", "readmore"], [1, "design-card-3", "dashboard1"], [1, "design-card-1", "dashboard1"], [1, "fa", "fa-credit-card", "fa-2x"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "col-sm-12"], [1, "row"], ["id", "accordion", 1, "myaccordion", "w-100"], ["class", "card card-2", 4, "ngFor", "ngForOf"], [1, "card", "card-2"], [1, "card-header", 3, "id"], [1, "mb-0"], [1, "col-sm-3"], [1, "form-group", "c1"], [1, "ctrl-label"], [1, "ctrl-value"], [1, "form-group", "c4"], [1, "form-group", "c3"], [1, "form-group", "c5"], [1, "form-group", "c2"], [1, "form-group", "c7"], [1, "form-group", "c6"], [1, "menubutton"], [3, "headerId", "type", "attachmentString", "printDocString", "communicationString"], ["data-parent", "#accordion", 1, "bodysection", "border-top", 3, "id"], [1, "table-responsive", "p-3"], [1, "table", "align-items-center", "table-flush", 3, "id"], [1, "thead-light"], [1, "text-right"], [4, "ngFor", "ngForOf"], ["href", "javascript:void(0)", 3, "click"], [3, "shipmentId", "shipmentView", "goBackShip"]],
      template: function PaymentRegisterComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, PaymentRegisterComponent_div_3_Template, 91, 27, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "span");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "select", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function PaymentRegisterComponent_Template_select_ngModelChange_8_listener($event) {
            return ctx.viewCount = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "option", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "10");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "option", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "20");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "option", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "30");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "option", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](16, "40");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "option", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "50");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, " \xA0\xA0\xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "span", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "pagination-controls", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("pageChange", function PaymentRegisterComponent_Template_pagination_controls_pageChange_21_listener($event) {
            return ctx.onPageChanged($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "div", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, PaymentRegisterComponent_app_common_payment_view_23_Template, 1, 2, "app-common-payment-view", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "ngx-spinner", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "p", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](27, "notifier-container");
        }

        if (rf & 2) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.shipmentView);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.viewCount);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.shipmentView);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgModel"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ɵangular_packages_forms_forms_x"], ngx_pagination__WEBPACK_IMPORTED_MODULE_11__["PaginationControlsComponent"], ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerComponent"], angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierContainerComponent"], _angular_forms__WEBPACK_IMPORTED_MODULE_10__["DefaultValueAccessor"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["NgForOf"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_12__["FileViewComponent"], _common_payment_view_common_payment_view_component__WEBPACK_IMPORTED_MODULE_13__["CommonPaymentViewComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_9__["DatePipe"], _angular_common__WEBPACK_IMPORTED_MODULE_9__["DecimalPipe"], ngx_pagination__WEBPACK_IMPORTED_MODULE_11__["PaginatePipe"], _shared_service_number_formatting_pipe__WEBPACK_IMPORTED_MODULE_14__["NumberFormattingPipe"]],
      styles: ["\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZmluYW5jZS9wYXltZW50LXJlZ2lzdGVyL3BheW1lbnQtcmVnaXN0ZXIuY29tcG9uZW50LmNzcyJ9 */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](PaymentRegisterComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-payment-register',
          templateUrl: './payment-register.component.html',
          styleUrls: ['./payment-register.component.css']
        }]
      }], function () {
        return [{
          type: _service_finance_service__WEBPACK_IMPORTED_MODULE_2__["FinanceService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_3__["NgxSpinnerService"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_4__["BreadcrumbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]
        }, {
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_8__["PurchaseService"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/service/finance.service.ts":
  /*!***************************************************************!*\
    !*** ./src/app/components/finance/service/finance.service.ts ***!
    \***************************************************************/

  /*! exports provided: FinanceService */

  /***/
  function srcAppComponentsFinanceServiceFinanceServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FinanceService", function () {
      return FinanceService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! file-saver */
    "./node_modules/file-saver/dist/FileSaver.min.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_3__);

    var FinanceService =
    /*#__PURE__*/
    function () {
      function FinanceService(http) {
        _classCallCheck(this, FinanceService);

        this.http = http;
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiBaseUrl + '/finance';
      }

      _createClass(FinanceService, [{
        key: "getFinance",
        value: function getFinance(email) {
          return this.http.post("".concat(this.baseUrl), email);
        }
      }, {
        key: "getExcel",
        value: function getExcel(data) {
          // return this.http.post(`${this.baseUrl}/getExcel`, data);
          var title = data.title;
          return this.http.post("".concat(this.baseUrl, "/getExcel"), data, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().append('token', localStorage.getItem('1')),
            observe: 'response',
            responseType: 'text'
          }).subscribe(function (r) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(new Blob([r.body], {
              type: 'text/csv'
            }), title + '.csv');
          });
        }
      }, {
        key: "getLedgerExcel",
        value: function getLedgerExcel(data) {
          // return this.http.post(`${this.baseUrl}/getExcel`, data);
          var title = data.title;
          return this.http.post("".concat(this.baseUrl, "/getLedgerExcel"), data, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().append('token', localStorage.getItem('1')),
            observe: 'response',
            responseType: 'text'
          }).subscribe(function (r) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(new Blob([r.body], {
              type: 'text/csv'
            }), title + '.csv');
          });
        }
      }, {
        key: "getAdvancePaymentExcel",
        value: function getAdvancePaymentExcel(data) {
          // return this.http.post(`${this.baseUrl}/getExcel`, data);
          var title = data.title;
          return this.http.post("".concat(this.baseUrl, "/getAdvancePaymentExcel"), data, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().append('token', localStorage.getItem('1')),
            observe: 'response',
            responseType: 'text'
          }).subscribe(function (r) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(new Blob([r.body], {
              type: 'text/csv'
            }), title + '.csv');
          });
        }
      }, {
        key: "getPaymentExcel",
        value: function getPaymentExcel(data) {
          // return this.http.post(`${this.baseUrl}/getExcel`, data);
          var title = data.title;
          return this.http.post("".concat(this.baseUrl, "/getPaymentExcel"), data, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().append('token', localStorage.getItem('1')),
            observe: 'response',
            responseType: 'text'
          }).subscribe(function (r) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(new Blob([r.body], {
              type: 'text/csv'
            }), title + '.csv');
          });
        }
      }]);

      return FinanceService;
    }();

    FinanceService.ɵfac = function FinanceService_Factory(t) {
      return new (t || FinanceService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    FinanceService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: FinanceService,
      factory: FinanceService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](FinanceService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/components/finance/upload-invoice/upload-invoice.component.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/components/finance/upload-invoice/upload-invoice.component.ts ***!
    \*******************************************************************************/

  /*! exports provided: UploadInvoiceComponent */

  /***/
  function srcAppComponentsFinanceUploadInvoiceUploadInvoiceComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UploadInvoiceComponent", function () {
      return UploadInvoiceComponent;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var _auth_service_email_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ../../auth/service/email.service */
    "./src/app/components/auth/service/email.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/shared/service/breadcrumb.service */
    "./src/app/shared/service/breadcrumb.service.ts");
    /* harmony import */


    var _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var ngx_spinner__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-spinner */
    "./node_modules/ngx-spinner/__ivy_ngcc__/fesm2015/ngx-spinner.js");
    /* harmony import */


    var src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/app/shared/service/rsa.service */
    "./src/app/shared/service/rsa.service.ts");
    /* harmony import */


    var angular_notifier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! angular-notifier */
    "./node_modules/angular-notifier/__ivy_ngcc__/fesm2015/angular-notifier.js");
    /* harmony import */


    var _auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../../auth/service/login.service */
    "./src/app/components/auth/service/login.service.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
    /* harmony import */


    var _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ../file-view/file-view.component */
    "./src/app/components/finance/file-view/file-view.component.ts");

    var _c0 = ["myFileInput"];

    function UploadInvoiceComponent_a_34_Template(rf, ctx) {
      if (rf & 1) {
        var _r593 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 56);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UploadInvoiceComponent_a_34_Template_a_click_0_listener() {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r593);

          var ctx_r592 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r592.onOpenModel();
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Upload Invoice ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UploadInvoiceComponent_a_35_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 58);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "Upload Invoice ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "i", 57);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }
    }

    function UploadInvoiceComponent_div_36_tr_23_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](13, "number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "td");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "diV");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](18, "app-file-view", 65);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var element_r595 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r595.BUYER_UNIT);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", element_r595.PO_NUMBER, " ");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r595.INVOICE_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r595.INVOICE_DATE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r595.CURRENCY_CODE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](13, 9, element_r595.INVOICE_AMOUNT));

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r595.INVOICE_DESCRIPTION);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("headerId", element_r595.INVOICE_ID)("type", "INVOICE_DOC_ATTACHMENTS");
      }
    }

    function UploadInvoiceComponent_div_36_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 59);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 60);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 61);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "table", 62);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "thead", 63);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "tr");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "Buyer Unit");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](9, "Purchase Order No");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "Invoice Number");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](13, "Invoice Date");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](15, "Currency");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "Invoice Amount");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](19, "Description");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "th");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "Files");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "tbody");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, UploadInvoiceComponent_div_36_tr_23_Template, 19, 11, "tr", 64);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r585 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](23);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r585.showDoc);
      }
    }

    function UploadInvoiceComponent_option_67_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r596 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", item_r596);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r596);
      }
    }

    function UploadInvoiceComponent_option_75_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r597 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", item_r597.PO_NUMBER);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r597.PO_NUMBER);
      }
    }

    function UploadInvoiceComponent_input_91_Template(rf, ctx) {
      if (rf & 1) {
        var _r599 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "input", 67);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_input_91_Template_input_ngModelChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r599);

          var ctx_r598 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r598.uploadInv.CURRENCY_CODE = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r589 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r589.uploadInv.CURRENCY_CODE)("readonly", ctx_r589.uploadInv.INVOICE_MODE === "po");
      }
    }

    function UploadInvoiceComponent_select_92_option_3_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "option", 66);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var item_r601 = ctx.$implicit;

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", item_r601.CURRENCY_CODE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](item_r601.CURRENCY_CODE);
      }
    }

    function UploadInvoiceComponent_select_92_Template(rf, ctx) {
      if (rf & 1) {
        var _r603 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "select", 68);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_select_92_Template_select_ngModelChange_0_listener($event) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r603);

          var ctx_r602 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

          return ctx_r602.uploadInv.CURRENCY_CODE = $event;
        });

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "option");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "Select");

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, UploadInvoiceComponent_select_92_option_3_Template, 2, 2, "option", 40);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      }

      if (rf & 2) {
        var ctx_r590 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx_r590.uploadInv.CURRENCY_CODE);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r590.currencyList);
      }
    }

    var UploadInvoiceComponent =
    /*#__PURE__*/
    function () {
      function UploadInvoiceComponent(emailService, router, breadCrumbServices, purchaseService, chRef, spinner, rsaService, notifierService, loginService) {
        _classCallCheck(this, UploadInvoiceComponent);

        this.emailService = emailService;
        this.router = router;
        this.breadCrumbServices = breadCrumbServices;
        this.purchaseService = purchaseService;
        this.chRef = chRef;
        this.spinner = spinner;
        this.rsaService = rsaService;
        this.notifierService = notifierService;
        this.loginService = loginService;
        this.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        this.today = new Date();
        this.oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
        this.fromDate = ('0' + this.oneMonthBefore.getDate()).slice(-2) + '-' + this.months[this.oneMonthBefore.getMonth()] + '-' + this.oneMonthBefore.getFullYear();
        this.toDate = ('0' + new Date().getDate()).slice(-2) + '-' + this.months[this.today.getMonth()] + '-' + this.today.getFullYear();
        this.fileData = null;
        this.uniquePos = [];
        this.units = [];
        this.showDoc = [];
        this.showDocAll = [];
        this.currencyList = [];
        this.showHide = true;
        this.notifier = notifierService;
        ;
      }

      _createClass(UploadInvoiceComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this35 = this;

          this.email = this.rsaService.decrypt(localStorage.getItem('3'));
          this.accountType = this.rsaService.decrypt(localStorage.getItem('6')); // this.accountType = 'SUPPLIER';

          this.uploadInv = new UploadInvoice();

          (function ($, thi) {
            $(document).ready(function () {
              var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
              var today = new Date();
              var oneMonthBefore = new Date(new Date().setDate(new Date().getDate() - 30));
              var fromDate = '' + ('0' + oneMonthBefore.getDate()).slice(-2) + '-' + months[oneMonthBefore.getMonth()] + '-' + oneMonthBefore.getFullYear();
              var toDate = '' + ('0' + new Date().getDate()).slice(-2) + '-' + months[today.getMonth()] + '-' + today.getFullYear();
              $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: ''+(new Date()).getFullYear()+'-01-'+('0'+(new Date()).getMonth()).slice(-2),
                value: thi.fromDate,
                change: function change(e) {
                  localStorage.setItem('LDF', e.target.value);
                  thi.fromDate = e.target.value;
                }
              });
              $('#datepicker1').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'dd-mmm-yyyy',
                // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
                value: thi.toDate,
                change: function change(e) {
                  thi.toDate = e.target.value;
                }
              });
            });
          })(jQuery, this);

          this.breadCrumbServices.selectValuesChanged.subscribe(function () {
            if (_this35.router.url === '/craftsmanautomation/finance/uploadinvoice') {
              _this35.showHideTable();
            }
          });
          this.getSelectValues();
          this.getPastPurchases();
          this.getCurrencyList();
        }
      }, {
        key: "setAsDataTable",
        value: function setAsDataTable() {
          this.chRef.detectChanges();

          (function ($) {
            $('#Pending-table').DataTable({
              responsive: true,
              bLengthChange: false,
              retrieve: true,
              pageLength: 5
            });
          })(jQuery);
        }
      }, {
        key: "getCurrencyList",
        value: function getCurrencyList() {
          var _this36 = this;

          // const details = { email: this.email, purchaseType: 'ALL_CURRENCIES' };
          var details = this.purchaseService.inputJSON('ALL_CURRENCIES', undefined, undefined);
          this.purchaseService.getPurchaseOrder(details).subscribe(function (data) {
            // console.log(
            //   'data',data
            // );
            _this36.currencyList = Array.isArray(data.FREIGHT_TERMS.FREIGHT_TERM) ? data.FREIGHT_TERMS.FREIGHT_TERM : data.FREIGHT_TERMS.FREIGHT_TERM ? [data.FREIGHT_TERMS.FREIGHT_TERM] : [];
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "getPastPurchases",
        value: function getPastPurchases() {
          var _this37 = this;

          this.spinner.show(); // this.fromDate = localStorage.getItem('PODF');
          // this.toDate = localStorage.getItem('PODT');
          //console.log(this.fromDate,this.toDate);
          // const details = { email: this.email, purchaseType: 'INVOICE_DOCUMENTS', date1: this.fromDate, date2: this.toDate };

          var details = this.purchaseService.inputJSON('INVOICE_DOCUMENTS', undefined, undefined);
          this.purchaseService.getPurchaseOrder(details).subscribe(function (data) {
            //console.log('ppp', data);
            _this37.spinner.hide();

            if (data) {
              _this37.showDocAll = Array.isArray(data.UPLOADED_INVOICES.UPLOADED_INVOICE) ? data.UPLOADED_INVOICES.UPLOADED_INVOICE : data.UPLOADED_INVOICES.UPLOADED_INVOICE ? [data.UPLOADED_INVOICES.UPLOADED_INVOICE] : [];

              _this37.showHideTable(); //this.setAsDataTable();
              // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
              // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
              // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
              // this.summary = this.fullSummary;
              // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
              // this.poCounts = this.poCountsAll;
              // this.setPoSummary();
              // this.setFilteredData();
              // this.setSummaryData();

            } else {}
          }, function (error) {
            _this37.spinner.hide();

            console.log(error);
          });
        }
      }, {
        key: "getPOList",
        value: function getPOList() {
          var _this38 = this;

          this.spinner.show(); // const details = { email: this.email, purchaseType: this.uploadInv.BUYER_UNIT };

          var details = this.purchaseService.inputJSON(this.uploadInv.BUYER_UNIT, undefined, undefined);
          this.purchaseService.getPurchaseOrderList(details).subscribe(function (data) {
            _this38.spinner.hide();

            if (data) {
              _this38.uniquePos = Array.isArray(data.PO_LISTS.PO_LIST) ? data.PO_LISTS.PO_LIST : data.PO_LISTS.PO_LIST ? [data.PO_LISTS.PO_LIST] : []; // this.fullpoHeaderList = Array.isArray(data.PASTPO.POHEADERS.POHEADER) ? data.PASTPO.POHEADERS.POHEADER : (data.PASTPO.POHEADERS.POHEADER) ? [data.PASTPO.POHEADERS.POHEADER] : [];
              // this.fullpolines = Array.isArray(data.PASTPO.POLINES.POLINE) ? data.PASTPO.POLINES.POLINE : (data.PASTPO.POHEADERS.POHEADER) ? [(data.PASTPO.POHEADERS.POHEADER)] : [];
              // this.fullSummary = Array.isArray(data.PASTPO.POSUMMARIES.POSUMMARY) ? data.PASTPO.POSUMMARIES.POSUMMARY : (data.PASTPO.POSUMMARIES.POSUMMARY) ? [data.PASTPO.POSUMMARIES.POSUMMARY] : [];
              // this.summary = this.fullSummary;
              // this.poCountsAll = Array.isArray(data.PASTPO.POCOUNTS.POCOUNT) ? data.PASTPO.POCOUNTS.POCOUNT : (data.PASTPO.POCOUNTS.POCOUNT) ? [data.PASTPO.POCOUNTS.POCOUNT] : [];
              // this.poCounts = this.poCountsAll;
              // this.setPoSummary();
              // this.setFilteredData();
              // this.setSummaryData();
            } else {}
          }, function (error) {
            _this38.spinner.hide();

            console.log(error);
          });
        }
      }, {
        key: "onFilesAdded",
        value: function onFilesAdded(fileInput) {
          var _this39 = this;

          // this.spinner.show();
          this.fileData = fileInput.target.files;
          var length = fileInput.target.files.length;
          this.length = fileInput.target.files.length;
          var file = new FormData();

          for (var i = 0; i < length; i++) {
            file.append('file', this.fileData[0], this.fileData[0].name);
          }

          var reader = new FileReader();
          reader.readAsDataURL(this.fileData[0]);

          reader.onload = function () {
            _this39.file64 = reader.result;
          };

          this.loginService.FileUpload(file).subscribe(function (data) {
            _this39.spinner.hide();

            var i;
            _this39.uploadInv.FILE_NAME = _this39.fileData[0].name;
          }, function (err) {
            _this39.spinner.hide();

            _this39.resetFile();

            if (err.error.message) {} else {}
          });
        }
      }, {
        key: "resetFile",
        value: function resetFile() {
          this.myInputVariable.nativeElement.value = '';
          this.length = 0;
        }
      }, {
        key: "cancel",
        value: function cancel() {
          this.resetFile();
        }
      }, {
        key: "setPo",
        value: function setPo(value) {
          var a = this.uniquePos.filter(function (f) {
            return f.PO_NUMBER === value;
          });

          if (a.length === 1) {
            this.uploadInv.PO_NUMBER = value;
            this.setCurrency(a);
          }
        }
      }, {
        key: "setCurrency",
        value: function setCurrency(a) {
          this.uploadInv.CURRENCY_CODE = a[0].CURRENCY_CODE;
          this.supplierName = a[0].VENDOR_NAME; //console.log('ccc', this.uploadInv.CURRENCY_CODE);
        }
      }, {
        key: "fileUpload",
        value: function fileUpload() {
          var _this40 = this;

          var toMail = '';

          if (['GP1 OU', 'GPL OU'].includes(this.uploadInv.BUYER_UNIT)) {
            toMail = 'hemavathi.raman@ytygroup.com.my';
          }

          if (['YTY OU', 'YTA OU'].includes(this.uploadInv.BUYER_UNIT)) {
            toMail = 'narjeet.singh@ytygroup.com.my';
          }

          this.spinner.show(); // &lt;Invoice Number&gt;
          // &lt;PO Number&gt;

          this.emailService.sendMailWithAttachments(toMail, "Craftsman Automation Supply: Invoice uploaded by ".concat(this.supplierName, " against PO ").concat(this.uploadInv.PO_NUMBER), "<p>Dear Invoice Process Team,</p>\n    <p>We have uploaded the invoice ".concat(this.uploadInv.INVOICE_NUMBER, "  against the Purchase Order ").concat(this.uploadInv.PO_NUMBER, " and the details are given below.</p>\n    <table border=\"1\">\n    <tbody>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Buyer Unit</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.BUYER_UNIT, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Purchase Order</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.PO_NUMBER, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Invoice Number</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.INVOICE_NUMBER, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Invoice Date</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.INVOICE_DATE, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Currency</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.CURRENCY_CODE, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Amount</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.INVOICE_AMOUNT, "</p>\n    </td>\n    </tr>\n    <tr style=\"height: 35px;\">\n    <td style=\"height: 35px; width: 160px;\">\n    <p><strong>Description</strong></p>\n    </td>\n    <td style=\"height: 35px; width: 441px;\">\n    <p>").concat(this.uploadInv.INVOICE_DESCRIPTION, "</p>\n    </td>\n    </tr>\n    </tbody>\n    </table>\n    <p>&nbsp;</p>\n    <p>Best Regards,</p>\n    <p>").concat(this.supplierName, "</p>\n    <p>This is an auto generated email. Please do not reply to this email.</p>"), this.fileData[0].name, this.file64).subscribe(function (message) {
            // this.spinner.hide();
            if (message === 'OK') {
              var id = 1; // this.router.navigate(['/auth/view'], { queryParams: { id } });
              // this.text = false;
              // this.email = '';
            } else {// this.notifier.notify('warning', 'Email Send Failed');
              }
          }, function (error) {
            console.log(error);
          });
          this.uploadInv.USER_NAME = this.email;
          this.purchaseService.saveInvoice({
            INVOICE: this.uploadInv
          }).subscribe(function (data) {
            _this40.spinner.hide();

            _this40.resetPage();

            _this40.getPastPurchases();
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "getSelectValues",
        value: function getSelectValues() {
          this.units = Array.from(new Set(this.breadCrumbServices.selectValues.map(function (x) {
            return x.ERP_ORGANIZATION_NAME;
          }))); // this.breadCrumService.selectValues
        }
      }, {
        key: "resetPage",
        value: function resetPage() {
          this.uploadInv = new UploadInvoice();
          this.resetFile();
        }
      }, {
        key: "onOpenModel",
        value: function onOpenModel() {
          this.uploadInv = new UploadInvoice();
          this.resetFile();

          (function ($, thi) {
            $('#datepicker2').datepicker({
              uiLibrary: 'bootstrap4',
              format: 'dd-mmm-yyyy',
              // value: +(new Date()).getFullYear()+'-'+('0' + (new Date()).getDate()).slice(-2)+'-'+('0'+(new Date()).getMonth()).slice(-2),
              value: thi.uploadInv.INVOICE_DATE,
              change: function change(e) {
                thi.uploadInv.INVOICE_DATE = e.target.value;
              }
            });
          })(jQuery, this);
        }
      }, {
        key: "getInvoice",
        value: function getInvoice() {
          if (this.uploadInv.INVOICE_MODE === 'direct') {
            this.uploadInv.PO_NUMBER = '';
            this.uploadInv.CURRENCY_CODE = '';
          } // console.log('fff', this.uploadInv.INVOICE_MODE, (!this.uploadInv.BUYER_UNIT && (this.uploadInv.INVOICE_MODE ==='direct')));

        }
      }, {
        key: "showHideTable",
        value: function showHideTable() {
          var _this41 = this;

          this.showHide = false;
          setTimeout(function () {
            _this41.showHide = true;

            _this41.setFilteredData();
          }, 10);
        }
      }, {
        key: "setFilteredData",
        value: function setFilteredData() {
          var _this42 = this;

          this.showDoc = this.showDocAll.filter(function (f) {
            var a = true;

            if (_this42.breadCrumbServices.select1 && _this42.breadCrumbServices.select1 !== 'All') {
              a = a && _this42.breadCrumbServices.select1 === f.BUYER_UNIT;
            } // if (this.breadCrumbServices.select2 && this.breadCrumbServices.select2 !== 'All') {
            //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_ID === f.VENDOR_ID;
            // }
            // if (this.breadCrumbServices.select3 && this.breadCrumbServices.select3 !== 'All') {
            //   a = a && this.breadCrumbServices.filteredSelection[0].VENDOR_SITE_ID === f.VENDOR_SITE_ID;
            // }


            if (_this42.searchText) {
              a = a && JSON.stringify(f).toLowerCase().includes(_this42.searchText.toLowerCase());
            }

            return a;
          });
          this.setAsDataTable(); // this.showHideTable();
        }
      }]);

      return UploadInvoiceComponent;
    }();

    UploadInvoiceComponent.ɵfac = function UploadInvoiceComponent_Factory(t) {
      return new (t || UploadInvoiceComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service_email_service__WEBPACK_IMPORTED_MODULE_1__["EmailService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]));
    };

    UploadInvoiceComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
      type: UploadInvoiceComponent,
      selectors: [["app-upload-invoice"]],
      viewQuery: function UploadInvoiceComponent_Query(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, true);
        }

        if (rf & 2) {
          var _t;

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.myInputVariable = _t.first);
        }
      },
      decls: 118,
      vars: 21,
      consts: [["id", "wrapper"], ["id", "content-wrapper", 1, "d-flex", "flex-column"], ["id", "content"], ["id", "container-wrapper", 1, "container-fluid"], ["id", "filter", 1, "row", "mb-3", "pl-3", "pr-3"], [1, "card", "w-100", "p-3"], [1, "col-sm-12"], [1, "row"], [1, "col-md-2"], [1, "form-group"], [1, "control-label"], ["id", "datetimepicker1", 1, "input-group", "date"], ["id", "datepicker", "name", "fromDate", "width", "276", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-2"], ["id", "datetimepicker2", 1, "input-group", "date"], ["id", "datepicker1", "name", "toDate", "width", "276", "readonly", "", 3, "ngModel", "ngModelChange"], [1, "col-sm-1"], ["href", "javascript:void(0)", 1, "btn", "btn-primary", "mt-2", 2, "margin-top", "5px", 3, "click"], [1, "col-sm-5"], [1, "input-group", "mt-2"], ["type", "text", "placeholder", "Enter Your Search Text", "aria-describedby", "basic-addon2", 1, "form-control", 3, "ngModel", "ngModelChange"], [1, "input-group-append"], ["id", "basic-addon2", "type", "submit", 1, "input-group-text", 3, "click"], [1, "float-right"], ["class", " float-right btn btn-primary btn-sm mt-2", "href", "javascript:void(0)", "data-toggle", "modal", "data-target", "#myModal", 3, "click", 4, "ngIf"], ["class", " float-right btn btn-primary btn-sm mt-2 isDisabled", "href", "javascript:void(0)", 4, "ngIf"], ["class", "row mb-3 pl-3 pr-3", "id", "grid-sections", 4, "ngIf"], ["id", "myModal", 1, "modal", "fade"], [1, "modal-dialog"], [1, "modal-content"], [1, "modal-header"], [1, "modal-title"], ["type", "button", "data-dismiss", "modal", 1, "close"], ["exampleForm1", "ngForm"], [1, "modal-body", "reg-form"], ["type", "radio", "name", "invoice", "value", "po", "required", "", 3, "ngModel", "ngModelChange"], ["for", "po"], ["type", "radio", "name", "invoice", "value", "direct", "required", "", 3, "ngModel", "ngModelChange"], ["for", "direct"], ["name", "category", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], [3, "value", 4, "ngFor", "ngForOf"], ["list", "selectPO", "id", "documentNo", "required", "", 1, "form-control", "ud", "js-example-basic-single", 3, "value", "disabled", "keyup"], ["id", "selectPO"], ["type", "text", "name", "number", "id", "exampleInputFirstName", "placeholder", " ", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["id", "datepicker2", "name", "date", "width", "276", "required", "", 3, "ngModel", "ngModelChange"], ["type", "text", "class", "form-control ud", "name", "currency", "id", "exampleInputFirstName", "placeholder", "Currency ", "required", "", 3, "ngModel", "readonly", "ngModelChange", 4, "ngIf"], ["name", "currency", "class", "form-control ud", "required", "", 3, "ngModel", "ngModelChange", 4, "ngIf"], ["type", "number", "name", "amount", "id", "exampleInputFirstName", "placeholder", "Enter Amount ", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "text", "id", "exampleInputFirstName", "placeholder", " ", "name", "description", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"], ["type", "file", "name", "file", "accept", "image/*,.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel,.pdf,.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document", 1, "files", 3, "change"], ["myFileInput", ""], [1, "modal-footer"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-primary", 3, "disabled", "click"], ["type", "button", "data-dismiss", "modal", 1, "btn", "btn-secondary", 3, "click"], ["bdColor", "rgba(0, 0, 0, 0.8)", "size", "medium", "color", "#1459cd", "type", "ball-spin-clockwise", 3, "fullScreen"], [2, "color", "white"], ["href", "javascript:void(0)", "data-toggle", "modal", "data-target", "#myModal", 1, "float-right", "btn", "btn-primary", "btn-sm", "mt-2", 3, "click"], [1, "ml-3", "fas", "fa-chevron-right"], ["href", "javascript:void(0)", 1, "float-right", "btn", "btn-primary", "btn-sm", "mt-2", "isDisabled"], ["id", "grid-sections", 1, "row", "mb-3", "pl-3", "pr-3"], ["id", "pending-details", 1, "card", "w-100"], [1, "table-responsive", "p-3"], ["id", "Pending-table", 1, "table", "align-items-center", "table-flush"], [1, "thead-light"], [4, "ngFor", "ngForOf"], [3, "headerId", "type"], [3, "value"], ["type", "text", "name", "currency", "id", "exampleInputFirstName", "placeholder", "Currency ", "required", "", 1, "form-control", "ud", 3, "ngModel", "readonly", "ngModelChange"], ["name", "currency", "required", "", 1, "form-control", "ud", 3, "ngModel", "ngModelChange"]],
      template: function UploadInvoiceComponent_Template(rf, ctx) {
        if (rf & 1) {
          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div", 3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "div", 5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "div", 7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](10, "label", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](11, "From Date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "input", 12);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_13_listener($event) {
            return ctx.fromDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](15, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "label", 10);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](17, "To Date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 14);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](19, "input", 15);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_19_listener($event) {
            return ctx.toDate = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "div", 16);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](21, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](22, "a", 17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UploadInvoiceComponent_Template_a_click_22_listener() {
            return ctx.getPastPurchases();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](23, "View");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "div", 18);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](25, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](26, "div", 19);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](27, "input", 20);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_27_listener($event) {
            return ctx.searchText = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "div", 21);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "Button", 22);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UploadInvoiceComponent_Template_Button_click_29_listener() {
            return ctx.showHideTable();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Search");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](31, "div", 13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](32, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "div", 23);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](34, UploadInvoiceComponent_a_34_Template, 3, 0, "a", 24);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, UploadInvoiceComponent_a_35_Template, 3, 0, "a", 25);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](36, UploadInvoiceComponent_div_36_Template, 24, 1, "div", 26);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](37, "div", 27);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "div", 28);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](39, "div", 29);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](40, "div", 30);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "h4", 31);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](42, "Upload Invoice");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "button", 32);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "\xD7");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](45, "form", null, 33);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](47, "div", 34);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](48, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](49, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](50, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](51, "Invoice Mode");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](52, "br");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](53, "input", 35);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_53_listener($event) {
            return ctx.uploadInv.INVOICE_MODE = $event;
          })("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_53_listener() {
            return ctx.getInvoice();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](54, "label", 36);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](55, "PO");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](56, "\xA0 \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](57, "input", 37);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_57_listener($event) {
            return ctx.uploadInv.INVOICE_MODE = $event;
          })("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_57_listener() {
            return ctx.getInvoice();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](58, "label", 38);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](59, "Direct");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](60, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](61, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](62, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](63, "Buyer Unit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](64, "select", 39);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_select_ngModelChange_64_listener($event) {
            return ctx.uploadInv.BUYER_UNIT = $event;
          })("ngModelChange", function UploadInvoiceComponent_Template_select_ngModelChange_64_listener() {
            return ctx.getPOList();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](65, "option");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](66, "Select");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](67, UploadInvoiceComponent_option_67_Template, 2, 2, "option", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](68, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](69, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](70, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](71, "Purchase Order No");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](72, "input", 41);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function UploadInvoiceComponent_Template_input_keyup_72_listener($event) {
            return ctx.setPo($event.target.value);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](73, "datalist", 42);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](74, "select");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](75, UploadInvoiceComponent_option_75_Template, 2, 2, "option", 40);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](76, "diV");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](77, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](78, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](79, "Invoice Number");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](80, "input", 43);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_80_listener($event) {
            return ctx.uploadInv.INVOICE_NUMBER = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](81, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](82, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](83, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](84, "Invoice Date");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](85, "div", 11);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](86, "input", 44);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_86_listener($event) {
            return ctx.uploadInv.INVOICE_DATE = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](87, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](88, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](89, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](90, "Currency");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](91, UploadInvoiceComponent_input_91_Template, 1, 2, "input", 45);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](92, UploadInvoiceComponent_select_92_Template, 4, 2, "select", 46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](93, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](94, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](95, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](96, "Invoice Amount");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](97, "input", 47);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_97_listener($event) {
            return ctx.uploadInv.INVOICE_AMOUNT = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](98, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](99, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](100, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](101, "Description");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](102, "input", 48);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("ngModelChange", function UploadInvoiceComponent_Template_input_ngModelChange_102_listener($event) {
            return ctx.uploadInv.INVOICE_DESCRIPTION = $event;
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](103, "div");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](104, "div", 9);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](105, "label");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](106, "File");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](107, "input", 49, 50);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function UploadInvoiceComponent_Template_input_change_107_listener($event) {
            return ctx.onFilesAdded($event);
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](109, "div", 51);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](110, "button", 52);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UploadInvoiceComponent_Template_button_click_110_listener() {
            return ctx.fileUpload();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](111, "Submit");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](112, " \xA0 ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](113, "button", 53);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function UploadInvoiceComponent_Template_button_click_113_listener() {
            return ctx.cancel();
          });

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](114, "Cancel");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](115, "ngx-spinner", 54);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](116, "p", 55);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](117, " Loading... ");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        }

        if (rf & 2) {
          var _r586 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](46);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](13);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.fromDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.toDate);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchText);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.accountType === "SUPPLIER");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.accountType !== "SUPPLIER");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.showHide);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](17);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_MODE);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_MODE);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.BUYER_UNIT);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.units);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", ctx.uploadInv.PO_NUMBER)("disabled", !(ctx.uploadInv.BUYER_UNIT && ctx.uploadInv.INVOICE_MODE === "po"));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.uniquePos);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_NUMBER);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_DATE);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.uploadInv.INVOICE_MODE === "po");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.uploadInv.INVOICE_MODE !== "po");

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_AMOUNT);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.uploadInv.INVOICE_DESCRIPTION);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](8);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", !(_r586.valid && ctx.length > 0));

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);

          _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("fullScreen", true);
        }
      },
      directives: [_angular_forms__WEBPACK_IMPORTED_MODULE_9__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgIf"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵangular_packages_forms_forms_y"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgControlStatusGroup"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgForm"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["RadioControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["RequiredValidator"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["SelectControlValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NgSelectOption"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ɵangular_packages_forms_forms_x"], _angular_common__WEBPACK_IMPORTED_MODULE_10__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_9__["NumberValueAccessor"], ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerComponent"], _file_view_file_view_component__WEBPACK_IMPORTED_MODULE_11__["FileViewComponent"]],
      pipes: [_angular_common__WEBPACK_IMPORTED_MODULE_10__["DecimalPipe"]],
      styles: [".ud[_ngcontent-%COMP%]{\r\n    width:80%;\r\n    padding-left: 5px;\r\n}\r\n\r\n.isDisabled[_ngcontent-%COMP%]{\r\n    \r\n  cursor: not-allowed;\r\n  opacity: 0.5;\r\n  \r\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9maW5hbmNlL3VwbG9hZC1pbnZvaWNlL3VwbG9hZC1pbnZvaWNlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxTQUFTO0lBQ1QsaUJBQWlCO0FBQ3JCOztBQUVBO0lBQ0kseUJBQXlCO0VBQzNCLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osMkJBQTJCO0FBQzdCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9maW5hbmNlL3VwbG9hZC1pbnZvaWNlL3VwbG9hZC1pbnZvaWNlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudWR7XHJcbiAgICB3aWR0aDo4MCU7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDVweDtcclxufVxyXG5cclxuLmlzRGlzYWJsZWR7XHJcbiAgICAvKiBjb2xvcjogY3VycmVudENvbG9yOyAqL1xyXG4gIGN1cnNvcjogbm90LWFsbG93ZWQ7XHJcbiAgb3BhY2l0eTogMC41O1xyXG4gIC8qIHRleHQtZGVjb3JhdGlvbjogbm9uZTsgKi9cclxufSJdfQ== */"]
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](UploadInvoiceComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"],
        args: [{
          selector: 'app-upload-invoice',
          templateUrl: './upload-invoice.component.html',
          styleUrls: ['./upload-invoice.component.css']
        }]
      }], function () {
        return [{
          type: _auth_service_email_service__WEBPACK_IMPORTED_MODULE_1__["EmailService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: src_app_shared_service_breadcrumb_service__WEBPACK_IMPORTED_MODULE_3__["BreadcrumbService"]
        }, {
          type: _purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__["PurchaseService"]
        }, {
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"]
        }, {
          type: ngx_spinner__WEBPACK_IMPORTED_MODULE_5__["NgxSpinnerService"]
        }, {
          type: src_app_shared_service_rsa_service__WEBPACK_IMPORTED_MODULE_6__["RsaService"]
        }, {
          type: angular_notifier__WEBPACK_IMPORTED_MODULE_7__["NotifierService"]
        }, {
          type: _auth_service_login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]
        }];
      }, {
        myInputVariable: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"],
          args: ['myFileInput', {
            "static": false
          }]
        }]
      });
    })();

    var UploadInvoice = function UploadInvoice() {
      _classCallCheck(this, UploadInvoice);

      this.BUYER_UNIT = '';
      this.PO_NUMBER = '';
      this.INVOICE_NUMBER = '';
      this.INVOICE_DATE = '';
      this.CURRENCY_CODE = '';
      this.INVOICE_AMOUNT = '';
      this.INVOICE_DESCRIPTION = '';
      this.FILE_NAME = '';
      this.USER_NAME = '';
      this.SUBJECT = '';
      this.BODY = '';
      this.TO = '';
      this.INVOICE_MODE = '';
    };
    /***/

  },

  /***/
  "./src/app/components/purchase/model/upload.ts":
  /*!*****************************************************!*\
    !*** ./src/app/components/purchase/model/upload.ts ***!
    \*****************************************************/

  /*! exports provided: Upload */

  /***/
  function srcAppComponentsPurchaseModelUploadTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "Upload", function () {
      return Upload;
    });

    var Upload = function Upload() {
      _classCallCheck(this, Upload);
    };
    /***/

  },

  /***/
  "./src/app/components/shipment/service/shipment.service.ts":
  /*!*****************************************************************!*\
    !*** ./src/app/components/shipment/service/shipment.service.ts ***!
    \*****************************************************************/

  /*! exports provided: ShipmentService */

  /***/
  function srcAppComponentsShipmentServiceShipmentServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ShipmentService", function () {
      return ShipmentService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! file-saver */
    "./node_modules/file-saver/dist/FileSaver.min.js");
    /* harmony import */


    var file_saver__WEBPACK_IMPORTED_MODULE_3___default =
    /*#__PURE__*/
    __webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_3__);

    var ShipmentService =
    /*#__PURE__*/
    function () {
      function ShipmentService(http) {
        _classCallCheck(this, ShipmentService);

        this.http = http;
        this.baseUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].apiBaseUrl + '/shipment';
      }

      _createClass(ShipmentService, [{
        key: "getShipments",
        value: function getShipments(email) {
          return this.http.post("".concat(this.baseUrl), email);
        }
      }, {
        key: "getExcel",
        value: function getExcel(data) {
          // return this.http.post(`${this.baseUrl}/getExcel`, data);
          var title = 'Pending Report';
          return this.http.post("".concat(this.baseUrl, "/getExcel"), data, {
            params: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().append('token', localStorage.getItem('1')),
            observe: 'response',
            responseType: 'text'
          }).subscribe(function (r) {
            Object(file_saver__WEBPACK_IMPORTED_MODULE_3__["saveAs"])(new Blob([r.body], {
              type: 'text/csv'
            }), title + '.csv');
          });
        }
      }, {
        key: "getShipmentsById",
        value: function getShipmentsById(email) {
          return this.http.put("".concat(this.baseUrl), email);
        }
      }]);

      return ShipmentService;
    }();

    ShipmentService.ɵfac = function ShipmentService_Factory(t) {
      return new (t || ShipmentService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]));
    };

    ShipmentService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: ShipmentService,
      factory: ShipmentService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](ShipmentService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      }, null);
    })();
    /***/

  },

  /***/
  "./src/app/shared/service/auth-guard.service.ts":
  /*!******************************************************!*\
    !*** ./src/app/shared/service/auth-guard.service.ts ***!
    \******************************************************/

  /*! exports provided: AuthGuardService */

  /***/
  function srcAppSharedServiceAuthGuardServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AuthGuardService", function () {
      return AuthGuardService;
    });
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
    /* harmony import */


    var src_app_config_config__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! src/app/config/config */
    "./src/app/config/config.ts");
    /* harmony import */


    var src_app_components_auth_service_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/components/auth/service/login.service */
    "./src/app/components/auth/service/login.service.ts");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
    /* harmony import */


    var src_app_components_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/components/purchase/service/purchase.service */
    "./src/app/components/purchase/service/purchase.service.ts");
    /* harmony import */


    var _rsa_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./rsa.service */
    "./src/app/shared/service/rsa.service.ts");

    var AuthGuardService =
    /*#__PURE__*/
    function () {
      function AuthGuardService(loginService, router, purchaseService, rsaService) {
        _classCallCheck(this, AuthGuardService);

        this.loginService = loginService;
        this.router = router;
        this.purchaseService = purchaseService;
        this.rsaService = rsaService;
      }

      _createClass(AuthGuardService, [{
        key: "canActivate",
        value: function canActivate(next, state) {
          var url = state.url;
          return this.checkLogin(url);
        }
      }, {
        key: "checkLogin",
        value: function checkLogin(url) {
          var _this43 = this;

          var menu;
          var val = localStorage.getItem('2');
          menu = this.rsaService.decrypt(localStorage.getItem('MENUACCESS'));
          var menuSelection = menu ? menu.MENUACCESS : [];
          var temp = menu.includes(url);

          if (!temp) {
            alert("This menu is not available for you. Please contact Administrator !");
            this.router.navigateByUrl('/auth/login');
          }

          var details = this.purchaseService.inputJSON('SESSION_CHECK', undefined, undefined);
          this.purchaseService.saveASN(details).subscribe(function (data) {
            if (data == 'L') {
              alert("session timeout");

              _this43.router.navigateByUrl('/auth/login');
            }
          }, function (error) {
            console.log(error);
          });

          if (val != null && val) {
            if (url == "auth/login") {
              this.router.navigateByUrl('/craftsmanautomation/dashboard', {
                skipLocationChange: src_app_config_config__WEBPACK_IMPORTED_MODULE_1__["profileData"].hideURL
              });
            } else return true;
          } else {
            return this.router.parseUrl('/auth/login');
          }
        }
      }]);

      return AuthGuardService;
    }();

    AuthGuardService.ɵfac = function AuthGuardService_Factory(t) {
      return new (t || AuthGuardService)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_components_auth_service_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](src_app_components_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__["PurchaseService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵinject"](_rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]));
    };

    AuthGuardService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({
      token: AuthGuardService,
      factory: AuthGuardService.ɵfac,
      providedIn: 'root'
    });
    /*@__PURE__*/

    (function () {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AuthGuardService, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"],
        args: [{
          providedIn: 'root'
        }]
      }], function () {
        return [{
          type: src_app_components_auth_service_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_components_purchase_service_purchase_service__WEBPACK_IMPORTED_MODULE_4__["PurchaseService"]
        }, {
          type: _rsa_service__WEBPACK_IMPORTED_MODULE_5__["RsaService"]
        }];
      }, null);
    })();
    /***/

  }
}]);
//# sourceMappingURL=components-finance-finance-module-es5.js.map